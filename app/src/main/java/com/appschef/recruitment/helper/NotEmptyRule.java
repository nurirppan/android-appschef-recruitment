package com.appschef.recruitment.helper;

import android.content.Context;
import android.support.design.widget.TextInputEditText;

import com.mobsandgeeks.saripaar.QuickRule;

public class NotEmptyRule extends QuickRule<TextInputEditText> {

    @Override
    public boolean isValid(TextInputEditText view) {
        String tmpString = view.getText().toString();
        return !"".equalsIgnoreCase(tmpString);
    }

    @Override
    public String getMessage(Context context) {
        return "This field is required";
    }
}
