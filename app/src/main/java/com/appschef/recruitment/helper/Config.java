package com.appschef.recruitment.helper;

public class Config {
    public static final String TAG = "MyFirebaseMsgingService";
    public static final String TITLE = "title";
    public static final String EMPTY = "";
    public static final String MESSAGE = "message";
    public static final String IMAGE = "image";
    public static final String ACTION = "action";
    public static final String DATA = "data";
    public static final String ACTION_DESTINATION = "action_destination";
    public static final String NAME = "name";
    public static final String TYPE_JOB = "typeJob";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String TIME_DATE = "timeDate";
    public static final String RECRUITMEN_ID = "recruitment_id";
    public static final String TYPE = "type";
}
