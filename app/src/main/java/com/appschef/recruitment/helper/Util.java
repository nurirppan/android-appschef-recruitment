package com.appschef.recruitment.helper;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.appschef.recruitment.activity.SignInActivity;
import com.appschef.recruitment.model.BaseResponse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Callback;

public class Util {

    public static RequestBody toTextRequestBody(String value) {
        if (value != null) {
            if (!"".equalsIgnoreCase(value)) {
                RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
                return body;
            }
        }
        return RequestBody.create(MediaType.parse("text/plain"), "");
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromURI(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static void checkSizeImage(Uri imageUri) {
        File fileImg = new File(imageUri.getPath());
        long fileLength = fileImg.length() / 1024;
        Log.i("size_img_file",String.valueOf(fileLength));

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(imageUri.getPath()).getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        Log.i("size_img_imageHeight",String.valueOf(imageHeight));
        Log.i("size_img_imageWidth",String.valueOf(imageWidth));
    }

    public static RequestBody toImageRequestBody(File file) {
        if (file != null) {
            RequestBody body = RequestBody.create(MediaType.parse("image/*"), file);
            return body;
        }
        return null;
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
