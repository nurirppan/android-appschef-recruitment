package com.appschef.recruitment.helper;

import android.content.Context;
import android.support.design.widget.TextInputEditText;

import com.mobsandgeeks.saripaar.QuickRule;

public class NotZeroRule extends QuickRule<TextInputEditText> {

    private String message;

    @Override
    public boolean isValid(TextInputEditText view) {

        if (!view.getText().toString().isEmpty()) {
            String numberStr = view.getText().toString().replaceAll(",", "");
            if ("0".equalsIgnoreCase(numberStr)) {
                message = "Inputan Tidak Boleh 0";
                return false;
            } else {
                int number = Integer.parseInt(numberStr);
                if (number < 1000) {
                    message = "Inputan Tidak Boleh Kurang Dari 1000";
                    return false;
                }
            }
        } else {
            message = "Inputan Tidak Boleh Kosong";
            return false;
        }

        return true;
    }

    @Override
    public String getMessage(Context context) {
        return message;
    }
}
