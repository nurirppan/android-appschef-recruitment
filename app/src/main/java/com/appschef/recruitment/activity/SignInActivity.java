package com.appschef.recruitment.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.model.response.LoginResponse;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.et_email_login)
    TextInputLayout etEmailLogin;
    @BindView(R.id.et_password_login)
    TextInputLayout etPasswordLogin;
    @BindView(R.id.btn_forget_password)
    TextView btnForgetPassword;
    @BindView(R.id.btn_sign_up)
    TextView btnSignUp;
    @BindView(R.id.btn_login)
    Button btnLogin;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    ApiService mApiService;

    ProgressDialog progressDialog;

    private String email;
    private String password;
    private String tokenFcm;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        mApiService = UtilsApi.getApiService();

        sharedPref = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setTitle("");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @OnClick({R.id.btn_forget_password, R.id.btn_sign_up, R.id.btn_login})
    public void onViewClicked(View view) {
        email = etEmailLogin.getEditText().getText().toString().trim();
        password = etPasswordLogin.getEditText().getText().toString().trim();
        switch (view.getId()) {
            case R.id.btn_forget_password:
                break;
            case R.id.btn_sign_up:
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
                finish();
                break;
            case R.id.btn_login:
                if (checkEmpty(email, password)) {
                    progressDialog.show();
                    requestLogin(email, password);
                }
                Log.i("edtxEmail", email);
                Log.i("edtxPasword", password);
                break;
        }
    }

    private void requestLogin(String email, String password) {
        mApiService.loginRequest(email, password)
                .enqueue(new Callback<BaseResponse<LoginResponse>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<LoginResponse>> call, Response<BaseResponse<LoginResponse>> response) {
                        if (response.isSuccessful()) {
                            processData(response);
                            progressDialog.dismiss();
                            startActivity(new Intent(SignInActivity.this, HomeActivity.class));
                            finish();
                        } else {
                            progressDialog.dismiss();
                            Util.showToast(SignInActivity.this, response.message());
                        }
                    }

                    private void processData(Response<BaseResponse<LoginResponse>> response) {
//                        if (response.body().getMeta().getCode() == 200) Toast.makeText(SignInActivity.this, response.body().getMeta().getMessage(), Toast.LENGTH_SHORT).show();
//                        else {
                        editor = sharedPref.edit();
                        String strToken = (response.body().getData().getToken().isEmpty()) ? "" : response.body().getData().getToken();
                        String strEmail = (response.body().getData().getUser().getEmail().isEmpty()) ? "" : response.body().getData().getUser().getEmail();
                        String strAvatar = (response.body().getData().getUser().getAvatar().isEmpty()) ? "" : response.body().getData().getUser().getAvatar();
                        String strFull_name = (response.body().getData().getUser().getFull_name().isEmpty()) ? "" : response.body().getData().getUser().getFull_name();

                        editor.putString(getString(R.string.sharedpref_token), strToken);
                        editor.putInt(getString(R.string.sharedpref_id), response.body().getData().getUser().getId());
                        editor.putString(getString(R.string.sharedpref_email), strEmail);
                        editor.putString(getString(R.string.sharedpref_avatar), strAvatar);
                        editor.putString(getString(R.string.sharedpref_fullname), strFull_name);
                        editor.apply();
//                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<LoginResponse>> call, Throwable t) {
                        progressDialog.dismiss();
                        Util.showToast(SignInActivity.this, "onFailure");
                    }
                });
    }

    private boolean checkEmpty(String email, String password) {
        if (email.isEmpty()) etEmailLogin.setError("email must be filled");
        else etEmailLogin.setError(null);
        if (password.isEmpty()) etPasswordLogin.setError("password must be filled");
        else etPasswordLogin.setError(null);
        return !email.isEmpty() && !password.isEmpty();
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Notice");
        alertDialog.setMessage("are you sure you want to close aplication ?");
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
//                System.exit(0);
            }
        });
        alertDialog.show();
    }
}
