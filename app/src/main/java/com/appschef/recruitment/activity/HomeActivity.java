package com.appschef.recruitment.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.appschef.recruitment.MainActivity;
import com.appschef.recruitment.R;
import com.appschef.recruitment.SecondActivity;
import com.appschef.recruitment.fragment.AboutFragment;
import com.appschef.recruitment.fragment.ApplyJobFragment;
import com.appschef.recruitment.fragment.ChangePasswordFragment;
import com.appschef.recruitment.fragment.NotificationsFragment;
import com.appschef.recruitment.fragment.RootFragment;
import com.appschef.recruitment.fragment.profile.DetailProfileFragment;
import com.appschef.recruitment.model.bus.RefreshActivity;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    Fragment fragment = null;
    private FragmentManager fragmentManager;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String token;
    private String username;
    private String email;
    private String avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");
        avatar = sharedPreferences.getString(getResources().getString(R.string.sharedpref_avatar), "");
        username = sharedPreferences.getString(getResources().getString(R.string.sharedpref_fullname), "");
        email = sharedPreferences.getString(getResources().getString(R.string.sharedpref_email), "");

        fragment = new RootFragment();
        actionFragment(fragment);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        View vNavHeader = navigationView.getHeaderView(0);
        CircleImageView ivPhotoProfile = (CircleImageView) vNavHeader.findViewById(R.id.iv_photo_profile);
        TextView tvUsername = vNavHeader.findViewById(R.id.tv_username);
        TextView tvEmail = vNavHeader.findViewById(R.id.tv_email);
        Glide.with(this).load(avatar).into(ivPhotoProfile);
        tvUsername.setText(username);
        tvEmail.setText(email);

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            fragmentManager = getSupportFragmentManager();
            Fragment fragmentHome = fragmentManager.findFragmentByTag("HomeFragment");
            if (fragmentManager.getBackStackEntryCount() >= 1) {
                if (fragmentManager != null) {
                    if (fragmentHome.isVisible()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Kembali ke menu utama?");
                        builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                HomeActivity.super.onBackPressed();
                            }
                        });
                        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                        return;
                    }
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Yakin keluar aplikasi?");
                builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor = sharedPreferences.edit();
                        editor.clear();
                        HomeActivity.this.finish();
                    }
                });
                builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;
        String tag = "";
        switch (item.getItemId()) {
            case R.id.nav_home:
                fragment = new RootFragment();
                tag = "DetailProfileFragment";
                break;
            case R.id.nav_profile:
                fragment = new DetailProfileFragment();
                tag = "DetailProfileFragment";
                break;
            case R.id.nav_change_password:
                fragment = new ChangePasswordFragment();
                tag = "ChangePasswordFragment";
                break;
            case R.id.nav_apply_job:
                fragment = new ApplyJobFragment();
                tag = "ApplyJobFragment";
                break;
            case R.id.nav_notifications:
                fragment = new NotificationsFragment();
                tag = "ApplyJobFragment";
                break;
            case R.id.nav_about:
                fragment = new AboutFragment();
                tag = "AboutFragment";
                break;
            case R.id.nav_main_activity:
                startActivity(new Intent(HomeActivity.this, MainActivity.class));
                break;
            case R.id.nav_second_activity:
                startActivity(new Intent(HomeActivity.this, SecondActivity.class));
                break;
            case R.id.nav_online_test:
                startActivity(new Intent(HomeActivity.this, OnlineTestActivity.class));
                break;
            case R.id.nav_interview_schedule:
                startActivity(new Intent(HomeActivity.this, InterviewScheduleActivity.class));
                break;
            case R.id.nav_requirement_online_test:
                startActivity(new Intent(HomeActivity.this, TestOnlineScheduleActivity.class));
                break;
            case R.id.nav_sign_out:
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_confirmation);
                TextView tvContentConfirmations = (TextView) dialog.findViewById(R.id.tv_content_confirmations);
                Button btnNo = (Button) dialog.findViewById(R.id.btn_no);
                Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
                tvContentConfirmations.setText(getString(R.string.txt_are_you_sure_want_to) + " sign out");
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.clear();
                        editor.apply();
                        dialog.dismiss();
                        startActivity(new Intent(HomeActivity.this, SignInActivity.class));
                        finish();
                    }
                });
                dialog.show();
                break;
        }
        actionFragment(fragment);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void actionFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setRefreshActivityEvent(RefreshActivity e) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View vNavHeader = navigationView.getHeaderView(0);
        CircleImageView ivPhotoProfile = (CircleImageView) vNavHeader.findViewById(R.id.iv_photo_profile);
        Glide.with(this).load(e.getStrFile()).into(ivPhotoProfile);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
