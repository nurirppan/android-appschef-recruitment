package com.appschef.recruitment.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.model.response.parentresponse.onlinetes.OnlineTestParent;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlineTestActivity extends AppCompatActivity {

    ApiService mApiService;
    SharedPreferences sharedPreferences;
    @BindView(R.id.pb_list_online_test)
    ProgressBar pbListOnlineTest;
    @BindView(R.id.tv_count_down)
    TextView tvCountDown;
    @BindView(R.id.tv_question_one)
    TextView tvQuestionOne;
    @BindView(R.id.rb_question_one_a)
    RadioButton rbQuestionOneA;
    @BindView(R.id.rb_question_one_b)
    RadioButton rbQuestionOneB;
    @BindView(R.id.rb_question_one_c)
    RadioButton rbQuestionOneC;
    @BindView(R.id.rb_question_one_d)
    RadioButton rbQuestionOneD;
    @BindView(R.id.rg_question_one)
    RadioGroup rgQuestionOne;
    @BindView(R.id.tv_question_two)
    TextView tvQuestionTwo;
    @BindView(R.id.rb_question_two_a)
    RadioButton rbQuestionTwoA;
    @BindView(R.id.rb_question_two_b)
    RadioButton rbQuestionTwoB;
    @BindView(R.id.rb_question_two_c)
    RadioButton rbQuestionTwoC;
    @BindView(R.id.rb_question_two_d)
    RadioButton rbQuestionTwoD;
    @BindView(R.id.rg_question_two)
    RadioGroup rgQuestionTwo;
    @BindView(R.id.tv_question_three)
    TextView tvQuestionThree;
    @BindView(R.id.rb_question_three_a)
    RadioButton rbQuestionThreeA;
    @BindView(R.id.rb_question_three_b)
    RadioButton rbQuestionThreeB;
    @BindView(R.id.rb_question_three_c)
    RadioButton rbQuestionThreeC;
    @BindView(R.id.rb_question_three_d)
    RadioButton rbQuestionThreeD;
    @BindView(R.id.rg_question_three)
    RadioGroup rgQuestionThree;
    @BindView(R.id.tv_question_four)
    TextView tvQuestionFour;
    @BindView(R.id.rb_question_four_a)
    RadioButton rbQuestionFourA;
    @BindView(R.id.rb_question_four_b)
    RadioButton rbQuestionFourB;
    @BindView(R.id.rb_question_four_c)
    RadioButton rbQuestionFourC;
    @BindView(R.id.rb_question_four_d)
    RadioButton rbQuestionFourD;
    @BindView(R.id.rg_question_four)
    RadioGroup rgQuestionFour;
    @BindView(R.id.tv_question_five)
    TextView tvQuestionFive;
    @BindView(R.id.rb_question_five_a)
    RadioButton rbQuestionFiveA;
    @BindView(R.id.rb_question_five_b)
    RadioButton rbQuestionFiveB;
    @BindView(R.id.rb_question_five_c)
    RadioButton rbQuestionFiveC;
    @BindView(R.id.rb_question_five_d)
    RadioButton rbQuestionFiveD;
    @BindView(R.id.rg_question_five)
    RadioGroup rgQuestionFive;
    @BindView(R.id.tv_question_six)
    TextView tvQuestionSix;
    @BindView(R.id.rb_question_six_a)
    RadioButton rbQuestionSixA;
    @BindView(R.id.rb_question_six_b)
    RadioButton rbQuestionSixB;
    @BindView(R.id.rb_question_six_c)
    RadioButton rbQuestionSixC;
    @BindView(R.id.rb_question_six_d)
    RadioButton rbQuestionSixD;
    @BindView(R.id.rg_question_six)
    RadioGroup rgQuestionSix;
    @BindView(R.id.tv_question_seven)
    TextView tvQuestionSeven;
    @BindView(R.id.rb_question_seven_a)
    RadioButton rbQuestionSevenA;
    @BindView(R.id.rb_question_seven_b)
    RadioButton rbQuestionSevenB;
    @BindView(R.id.rb_question_seven_c)
    RadioButton rbQuestionSevenC;
    @BindView(R.id.rb_question_seven_d)
    RadioButton rbQuestionSevenD;
    @BindView(R.id.rg_question_seven)
    RadioGroup rgQuestionSeven;
    @BindView(R.id.tv_question_eight)
    TextView tvQuestionEight;
    @BindView(R.id.rb_question_eight_a)
    RadioButton rbQuestionEightA;
    @BindView(R.id.rb_question_eight_b)
    RadioButton rbQuestionEightB;
    @BindView(R.id.rb_question_eight_c)
    RadioButton rbQuestionEightC;
    @BindView(R.id.rb_question_eight_d)
    RadioButton rbQuestionEightD;
    @BindView(R.id.rg_question_eight)
    RadioGroup rgQuestionEight;
    @BindView(R.id.tv_question_nine)
    TextView tvQuestionNine;
    @BindView(R.id.rb_question_nine_a)
    RadioButton rbQuestionNineA;
    @BindView(R.id.rb_question_nine_b)
    RadioButton rbQuestionNineB;
    @BindView(R.id.rb_question_nine_c)
    RadioButton rbQuestionNineC;
    @BindView(R.id.rb_question_nine_d)
    RadioButton rbQuestionNineD;
    @BindView(R.id.rg_question_nine)
    RadioGroup rgQuestionNine;
    @BindView(R.id.tv_question_ten)
    TextView tvQuestionTen;
    @BindView(R.id.rb_question_ten_a)
    RadioButton rbQuestionTenA;
    @BindView(R.id.rb_question_ten_b)
    RadioButton rbQuestionTenB;
    @BindView(R.id.rb_question_ten_c)
    RadioButton rbQuestionTenC;
    @BindView(R.id.rb_question_ten_d)
    RadioButton rbQuestionTenD;
    @BindView(R.id.rg_question_ten)
    RadioGroup rgQuestionTen;
    @BindView(R.id.tv_question_eleven)
    TextView tvQuestionEleven;
    @BindView(R.id.rb_question_eleven_a)
    RadioButton rbQuestionElevenA;
    @BindView(R.id.rb_question_eleven_b)
    RadioButton rbQuestionElevenB;
    @BindView(R.id.rb_question_eleven_c)
    RadioButton rbQuestionElevenC;
    @BindView(R.id.rb_question_eleven_d)
    RadioButton rbQuestionElevenD;
    @BindView(R.id.rg_question_eleven)
    RadioGroup rgQuestionEleven;
    @BindView(R.id.tv_question_twelve)
    TextView tvQuestionTwelve;
    @BindView(R.id.rb_question_twelve_a)
    RadioButton rbQuestionTwelveA;
    @BindView(R.id.rb_question_twelve_b)
    RadioButton rbQuestionTwelveB;
    @BindView(R.id.rb_question_twelve_c)
    RadioButton rbQuestionTwelveC;
    @BindView(R.id.rb_question_twelve_d)
    RadioButton rbQuestionTwelveD;
    @BindView(R.id.rg_question_twelve)
    RadioGroup rgQuestionTwelve;
    @BindView(R.id.tv_question_thirteen)
    TextView tvQuestionThirteen;
    @BindView(R.id.rb_question_thirteen_a)
    RadioButton rbQuestionThirteenA;
    @BindView(R.id.rb_question_thirteen_b)
    RadioButton rbQuestionThirteenB;
    @BindView(R.id.rb_question_thirteen_c)
    RadioButton rbQuestionThirteenC;
    @BindView(R.id.rb_question_thirteen_d)
    RadioButton rbQuestionThirteenD;
    @BindView(R.id.rg_question_thirteen)
    RadioGroup rgQuestionThirteen;
    @BindView(R.id.tv_question_fourteen)
    TextView tvQuestionFourteen;
    @BindView(R.id.rb_question_fourteen_a)
    RadioButton rbQuestionFourteenA;
    @BindView(R.id.rb_question_fourteen_b)
    RadioButton rbQuestionFourteenB;
    @BindView(R.id.rb_question_fourteen_c)
    RadioButton rbQuestionFourteenC;
    @BindView(R.id.rb_question_fourteen_d)
    RadioButton rbQuestionFourteenD;
    @BindView(R.id.rg_question_fourteen)
    RadioGroup rgQuestionFourteen;
    @BindView(R.id.tv_question_fiveteen)
    TextView tvQuestionFiveteen;
    @BindView(R.id.rb_question_fiveteen_a)
    RadioButton rbQuestionFiveteenA;
    @BindView(R.id.rb_question_fiveteen_b)
    RadioButton rbQuestionFiveteenB;
    @BindView(R.id.rb_question_fiveteen_c)
    RadioButton rbQuestionFiveteenC;
    @BindView(R.id.rb_question_fiveteen_d)
    RadioButton rbQuestionFiveteenD;
    @BindView(R.id.rg_question_fiveteen)
    RadioGroup rgQuestionFiveteen;
    @BindView(R.id.tv_question_sixteen)
    TextView tvQuestionSixteen;
    @BindView(R.id.et_answer_sixteen)
    EditText etAnswerSixteen;
    @BindView(R.id.tv_question_seventeen)
    TextView tvQuestionSeventeen;
    @BindView(R.id.et_answer_seventeen)
    EditText etAnswerSeventeen;
    @BindView(R.id.tv_question_eighteen)
    TextView tvQuestionEighteen;
    @BindView(R.id.et_answer_eighteen)
    EditText etAnswerEighteen;
    @BindView(R.id.tv_question_nineteen)
    TextView tvQuestionNineteen;
    @BindView(R.id.et_answer_nineteen)
    EditText etAnswerNineteen;
    @BindView(R.id.tv_question_twenty)
    TextView tvQuestionTwenty;
    @BindView(R.id.et_answer_twenty)
    EditText etAnswerTwenty;
    @BindView(R.id.no_one)
    Button noOne;
    @BindView(R.id.no_two)
    Button noTwo;
    @BindView(R.id.no_three)
    Button noThree;
    @BindView(R.id.no_four)
    Button noFour;
    @BindView(R.id.no_five)
    Button noFive;
    @BindView(R.id.no_six)
    Button noSix;
    @BindView(R.id.no_seven)
    Button noSeven;
    @BindView(R.id.no_eight)
    Button noEight;
    @BindView(R.id.no_nine)
    Button noNine;
    @BindView(R.id.no_ten)
    Button noTen;
    @BindView(R.id.no_eleven)
    Button noEleven;
    @BindView(R.id.no_twelve)
    Button noTwelve;
    @BindView(R.id.no_thirteen)
    Button noThirteen;
    @BindView(R.id.no_fourteen)
    Button noFourteen;
    @BindView(R.id.no_fiveteen)
    Button noFiveteen;
    @BindView(R.id.no_sixteen)
    Button noSixteen;
    @BindView(R.id.no_seventeen)
    Button noSeventeen;
    @BindView(R.id.no_eighteen)
    Button noEighteen;
    @BindView(R.id.no_nineteen)
    Button noNineteen;
    @BindView(R.id.no_twenty)
    Button noTwenty;
    @BindView(R.id.nsv_number)
    NestedScrollView nsvNumber;
    @BindView(R.id.ll_online_test)
    LinearLayout llOnlineTest;
    @BindView(R.id.btn_submit_online_test)
    Button btnSubmitOnlineTest;

    @BindView(R.id.content_question_one)
    LinearLayout contentQuestionOne;
    @BindView(R.id.content_question_two)
    LinearLayout contentQuestionTwo;
    @BindView(R.id.content_question_three)
    LinearLayout contentQuestionThree;
    @BindView(R.id.content_question_four)
    LinearLayout contentQuestionFour;
    @BindView(R.id.content_question_five)
    LinearLayout contentQuestionFive;
    @BindView(R.id.content_question_six)
    LinearLayout contentQuestionSix;
    @BindView(R.id.content_question_seven)
    LinearLayout contentQuestionSeven;
    @BindView(R.id.content_question_eight)
    LinearLayout contentQuestionEight;
    @BindView(R.id.content_question_nine)
    LinearLayout contentQuestionNine;
    @BindView(R.id.content_question_ten)
    LinearLayout contentQuestionTen;
    @BindView(R.id.content_question_eleven)
    LinearLayout contentQuestionEleven;
    @BindView(R.id.content_question_twelve)
    LinearLayout contentQuestionTwelve;
    @BindView(R.id.content_question_thirteen)
    LinearLayout contentQuestionThirteen;
    @BindView(R.id.content_question_fourteen)
    LinearLayout contentQuestionFourteen;
    @BindView(R.id.content_question_fiveteen)
    LinearLayout contentQuestionFiveteen;
    @BindView(R.id.content_question_sixteen)
    LinearLayout contentQuestionSixteen;
    @BindView(R.id.content_question_seventeen)
    LinearLayout contentQuestionSeventeen;
    @BindView(R.id.content_question_eighteen)
    LinearLayout contentQuestionEighteen;
    @BindView(R.id.content_question_nineteen)
    LinearLayout contentQuestionNineteen;
    @BindView(R.id.content_question_twenty)
    LinearLayout contentQuestionTwenty;

    @BindViews({R.id.content_question_one, R.id.content_question_two, R.id.content_question_three, R.id.content_question_four, R.id.content_question_five, R.id.content_question_six, R.id.content_question_seven, R.id.content_question_eight, R.id.content_question_nine, R.id.content_question_ten, R.id.content_question_eleven, R.id.content_question_twelve, R.id.content_question_thirteen, R.id.content_question_fourteen, R.id.content_question_fiveteen, R.id.content_question_sixteen, R.id.content_question_seventeen, R.id.content_question_eighteen, R.id.content_question_nineteen, R.id.content_question_twenty})
    List<LinearLayout> contentQuestionList;

    private String token;
    private ProgressDialog progressDialog;
    private int idQOne, idQTwo, idQThree, idQFour, idQFive, idQSix, idQSeven, idQEight, idQNine, idQTen, idQEleven, idQTwelve, idQThirteen, idQFourteen, idQFiveteen;
    private RadioButton answerOne, answerTwo, answerThree, answerFour, answerFive, answerSix, answerSeven, answerEight, answerNine, answerTen, answerEleven, answerTwelve, answerThirteen, answerFourteen, answerFiveteen;
    private String strAnswerOne, strAnswerTwo, strAnswerThree, strAnswerFour, strAnswerFive, strAnswerSix, strAnswerSeven, strAnswerEight, strAnswerNine, strAnswerTen, strAnswerEleven, strAnswerTwelve, strAnswerThirteen, strAnswerFourteen, strAnswerFiveteen, strAnswerSixteen, strAnswerSeventeen, strAnswerEighteen, strAnswerNineteen, strAnswerTwenty;
    private int intValueAnswerOne, intValueAnswerTwo, intValueAnswerThree, intValueAnswerFour, intValueAnswerFive, intValueAnswerSix, intValueAnswerSeven, intValueAnswerEight, intValueAnswerNine, intValueAnswerTen, intValueAnswerEleven, intValueAnswerTwelve, intValueAnswerThirteen, intValueAnswerFourteen, intValueAnswerFiveteen, intValueAnswerSixteen, intValueAnswerSeventeen, intValueAnswerEighteen, intValueAnswerNineteen, intValueAnswerTwenty;
    private double resultMultipleChoice;

    private Response<BaseResponse<OnlineTestParent>> response;
    private Response<BaseResponse<OnlineTestParent>> responseEssay;
    private String slug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_test);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();

        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");
        slug = getIntent().getStringExtra("slug");

        if (slug != null) valueOnlineTestGet(slug);
    }

    private void valueOnlineTestGet(String slugslug) {
        progressDialog.show();
        pbListOnlineTest.setVisibility(View.VISIBLE);
        hitMultipleChoice(slug);
    }

    private void hitEssay() {
        mApiService.essayGet(token, slug).enqueue(new Callback<BaseResponse<OnlineTestParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<OnlineTestParent>> call, Response<BaseResponse<OnlineTestParent>> response) {
                if (response.isSuccessful()) {
                    setValueEssayGet(response);
                    progressDialog.dismiss();
                    btnSubmitOnlineTest.setVisibility(View.VISIBLE);
                    pbListOnlineTest.setVisibility(View.GONE);
                    llOnlineTest.setVisibility(View.VISIBLE);
                    Toast.makeText(OnlineTestActivity.this, response.body().getMeta().getMessage(), Toast.LENGTH_SHORT).show();

                    new CountDownTimer(2700000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            String timerProgress = "" + String.format(Locale.getDefault(), "%02d : %02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                            tvCountDown.setText(timerProgress);
                        }

                        @Override
                        public void onFinish() {
                            tvCountDown.setText("Waktu Habis");
                            valueMultipleChoice();
                            postAnswerOnlineTest();
                        }
                    }.start();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(OnlineTestActivity.this, response.body().getMeta().getMessage(), Toast.LENGTH_SHORT).show();
                    pbListOnlineTest.setVisibility(View.GONE);
                    reHitApiOnlineTest(response);
                }
                Log.i("Log__OnlineTest", response.body().getMeta().getMessage());
            }

            @Override
            public void onFailure(Call<BaseResponse<OnlineTestParent>> call, Throwable t) {
                progressDialog.dismiss();
                pbListOnlineTest.setVisibility(View.GONE);
                Log.i("Log__OnlineTest", "onFailure");
            }
        });
    }

    private void hitMultipleChoice(String slug) {
        mApiService.multipleChoiceGet(token, slug).enqueue(new Callback<BaseResponse<OnlineTestParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<OnlineTestParent>> call, Response<BaseResponse<OnlineTestParent>> response) {
                if (response.isSuccessful()) {
                    setValueMultipleChoice(response);
                    Toast.makeText(OnlineTestActivity.this, response.body().getMeta().getMessage(), Toast.LENGTH_SHORT).show();
                    hitEssay();
                } else {
                    progressDialog.dismiss();
                    pbListOnlineTest.setVisibility(View.GONE);
                    Toast.makeText(OnlineTestActivity.this, response.body().getMeta().getMessage(), Toast.LENGTH_SHORT).show();
                    reHitApiOnlineTest(response);
                }
                Log.i("Log__OnlineTest", response.body().getMeta().getMessage());
            }

            @Override
            public void onFailure(Call<BaseResponse<OnlineTestParent>> call, Throwable t) {
                progressDialog.dismiss();
                pbListOnlineTest.setVisibility(View.GONE);
                Log.i("Log__OnlineTest", "onFailure");
            }
        });
    }

    private void reHitApiOnlineTest(Response<BaseResponse<OnlineTestParent>> response) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_confirmation);
        TextView tvContentConfirmations = (TextView) dialog.findViewById(R.id.tv_content_confirmations);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);
        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        tvContentConfirmations.setText("Retry");
        btnNo.setVisibility(View.GONE);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitMultipleChoice(slug);
                dialog.dismiss();
            }
        });
    }

    private void setValueEssayGet(Response<BaseResponse<OnlineTestParent>> response) {
        this.responseEssay = response;
        if (!response.body().getData().getPertanyaan().isEmpty()) {
            tvQuestionSixteen.setText(response.body().getData().getPertanyaan().get(0).getQuestion());
            tvQuestionSeventeen.setText(response.body().getData().getPertanyaan().get(1).getQuestion());
            tvQuestionEighteen.setText(response.body().getData().getPertanyaan().get(2).getQuestion());
            tvQuestionNineteen.setText(response.body().getData().getPertanyaan().get(3).getQuestion());
            tvQuestionTwenty.setText(response.body().getData().getPertanyaan().get(4).getQuestion());
        }
    }

    private void setValueMultipleChoice(Response<BaseResponse<OnlineTestParent>> response) {
        this.response = response;
        if (!response.body().getData().getPertanyaan().isEmpty()) {
            tvQuestionOne.setText(response.body().getData().getPertanyaan().get(0).getQuestion());
            tvQuestionTwo.setText(response.body().getData().getPertanyaan().get(1).getQuestion());
            tvQuestionThree.setText(response.body().getData().getPertanyaan().get(2).getQuestion());
            tvQuestionFour.setText(response.body().getData().getPertanyaan().get(3).getQuestion());
            tvQuestionFive.setText(response.body().getData().getPertanyaan().get(4).getQuestion());
            tvQuestionSix.setText(response.body().getData().getPertanyaan().get(5).getQuestion());
            tvQuestionSeven.setText(response.body().getData().getPertanyaan().get(6).getQuestion());
            tvQuestionEight.setText(response.body().getData().getPertanyaan().get(7).getQuestion());
            tvQuestionNine.setText(response.body().getData().getPertanyaan().get(8).getQuestion());
            tvQuestionTen.setText(response.body().getData().getPertanyaan().get(9).getQuestion());
            tvQuestionEleven.setText(response.body().getData().getPertanyaan().get(10).getQuestion());
            tvQuestionTwelve.setText(response.body().getData().getPertanyaan().get(11).getQuestion());
            tvQuestionThirteen.setText(response.body().getData().getPertanyaan().get(12).getQuestion());
            tvQuestionFourteen.setText(response.body().getData().getPertanyaan().get(13).getQuestion());
            tvQuestionFiveteen.setText(response.body().getData().getPertanyaan().get(14).getQuestion());
        }
        if (!response.body().getData().getPertanyaan().get(0).getAnswers().isEmpty()) {
            rbQuestionOneA.setText(response.body().getData().getPertanyaan().get(0).getAnswers().get(0).getAnswer());
            rbQuestionOneB.setText(response.body().getData().getPertanyaan().get(0).getAnswers().get(1).getAnswer());
            rbQuestionOneC.setText(response.body().getData().getPertanyaan().get(0).getAnswers().get(2).getAnswer());
            rbQuestionOneD.setText(response.body().getData().getPertanyaan().get(0).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(1).getAnswers().isEmpty()) {
            rbQuestionTwoA.setText(response.body().getData().getPertanyaan().get(1).getAnswers().get(0).getAnswer());
            rbQuestionTwoB.setText(response.body().getData().getPertanyaan().get(1).getAnswers().get(1).getAnswer());
            rbQuestionTwoC.setText(response.body().getData().getPertanyaan().get(1).getAnswers().get(2).getAnswer());
            rbQuestionTwoD.setText(response.body().getData().getPertanyaan().get(1).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(2).getAnswers().isEmpty()) {
            rbQuestionThreeA.setText(response.body().getData().getPertanyaan().get(2).getAnswers().get(0).getAnswer());
            rbQuestionThreeB.setText(response.body().getData().getPertanyaan().get(2).getAnswers().get(1).getAnswer());
            rbQuestionThreeC.setText(response.body().getData().getPertanyaan().get(2).getAnswers().get(2).getAnswer());
            rbQuestionThreeD.setText(response.body().getData().getPertanyaan().get(2).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(3).getAnswers().isEmpty()) {
            rbQuestionFourA.setText(response.body().getData().getPertanyaan().get(3).getAnswers().get(0).getAnswer());
            rbQuestionFourB.setText(response.body().getData().getPertanyaan().get(3).getAnswers().get(1).getAnswer());
            rbQuestionFourC.setText(response.body().getData().getPertanyaan().get(3).getAnswers().get(2).getAnswer());
            rbQuestionFourD.setText(response.body().getData().getPertanyaan().get(3).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(4).getAnswers().isEmpty()) {
            rbQuestionFiveA.setText(response.body().getData().getPertanyaan().get(4).getAnswers().get(0).getAnswer());
            rbQuestionFiveB.setText(response.body().getData().getPertanyaan().get(4).getAnswers().get(1).getAnswer());
            rbQuestionFiveC.setText(response.body().getData().getPertanyaan().get(4).getAnswers().get(2).getAnswer());
            rbQuestionFiveD.setText(response.body().getData().getPertanyaan().get(4).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(5).getAnswers().isEmpty()) {
            rbQuestionSixA.setText(response.body().getData().getPertanyaan().get(5).getAnswers().get(0).getAnswer());
            rbQuestionSixB.setText(response.body().getData().getPertanyaan().get(5).getAnswers().get(1).getAnswer());
            rbQuestionSixC.setText(response.body().getData().getPertanyaan().get(5).getAnswers().get(2).getAnswer());
            rbQuestionSixD.setText(response.body().getData().getPertanyaan().get(5).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(6).getAnswers().isEmpty()) {
            rbQuestionSevenA.setText(response.body().getData().getPertanyaan().get(6).getAnswers().get(0).getAnswer());
            rbQuestionSevenB.setText(response.body().getData().getPertanyaan().get(6).getAnswers().get(1).getAnswer());
            rbQuestionSevenC.setText(response.body().getData().getPertanyaan().get(6).getAnswers().get(2).getAnswer());
            rbQuestionSevenD.setText(response.body().getData().getPertanyaan().get(6).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(7).getAnswers().isEmpty()) {
            rbQuestionEightA.setText(response.body().getData().getPertanyaan().get(7).getAnswers().get(0).getAnswer());
            rbQuestionEightB.setText(response.body().getData().getPertanyaan().get(7).getAnswers().get(1).getAnswer());
            rbQuestionEightC.setText(response.body().getData().getPertanyaan().get(7).getAnswers().get(2).getAnswer());
            rbQuestionEightD.setText(response.body().getData().getPertanyaan().get(7).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(8).getAnswers().isEmpty()) {
            rbQuestionNineA.setText(response.body().getData().getPertanyaan().get(8).getAnswers().get(0).getAnswer());
            rbQuestionNineB.setText(response.body().getData().getPertanyaan().get(8).getAnswers().get(1).getAnswer());
            rbQuestionNineC.setText(response.body().getData().getPertanyaan().get(8).getAnswers().get(2).getAnswer());
            rbQuestionNineD.setText(response.body().getData().getPertanyaan().get(8).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(9).getAnswers().isEmpty()) {
            rbQuestionTenA.setText(response.body().getData().getPertanyaan().get(9).getAnswers().get(0).getAnswer());
            rbQuestionTenB.setText(response.body().getData().getPertanyaan().get(9).getAnswers().get(1).getAnswer());
            rbQuestionTenC.setText(response.body().getData().getPertanyaan().get(9).getAnswers().get(2).getAnswer());
            rbQuestionTenD.setText(response.body().getData().getPertanyaan().get(9).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(10).getAnswers().isEmpty()) {
            rbQuestionElevenA.setText(response.body().getData().getPertanyaan().get(10).getAnswers().get(0).getAnswer());
            rbQuestionElevenB.setText(response.body().getData().getPertanyaan().get(10).getAnswers().get(1).getAnswer());
            rbQuestionElevenC.setText(response.body().getData().getPertanyaan().get(10).getAnswers().get(2).getAnswer());
            rbQuestionElevenD.setText(response.body().getData().getPertanyaan().get(10).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(11).getAnswers().isEmpty()) {
            rbQuestionTwelveA.setText(response.body().getData().getPertanyaan().get(11).getAnswers().get(0).getAnswer());
            rbQuestionTwelveB.setText(response.body().getData().getPertanyaan().get(11).getAnswers().get(1).getAnswer());
            rbQuestionTwelveC.setText(response.body().getData().getPertanyaan().get(11).getAnswers().get(2).getAnswer());
            rbQuestionTwelveD.setText(response.body().getData().getPertanyaan().get(11).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(12).getAnswers().isEmpty()) {
            rbQuestionThirteenA.setText(response.body().getData().getPertanyaan().get(12).getAnswers().get(0).getAnswer());
            rbQuestionThirteenB.setText(response.body().getData().getPertanyaan().get(12).getAnswers().get(1).getAnswer());
            rbQuestionThirteenC.setText(response.body().getData().getPertanyaan().get(12).getAnswers().get(2).getAnswer());
            rbQuestionThirteenD.setText(response.body().getData().getPertanyaan().get(12).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(13).getAnswers().isEmpty()) {
            rbQuestionFourteenA.setText(response.body().getData().getPertanyaan().get(13).getAnswers().get(0).getAnswer());
            rbQuestionFourteenB.setText(response.body().getData().getPertanyaan().get(13).getAnswers().get(1).getAnswer());
            rbQuestionFourteenC.setText(response.body().getData().getPertanyaan().get(13).getAnswers().get(2).getAnswer());
            rbQuestionFourteenD.setText(response.body().getData().getPertanyaan().get(13).getAnswers().get(3).getAnswer());
        }
        if (!response.body().getData().getPertanyaan().get(14).getAnswers().isEmpty()) {
            rbQuestionFiveteenA.setText(response.body().getData().getPertanyaan().get(14).getAnswers().get(0).getAnswer());
            rbQuestionFiveteenB.setText(response.body().getData().getPertanyaan().get(14).getAnswers().get(1).getAnswer());
            rbQuestionFiveteenC.setText(response.body().getData().getPertanyaan().get(14).getAnswers().get(2).getAnswer());
            rbQuestionFiveteenD.setText(response.body().getData().getPertanyaan().get(14).getAnswers().get(3).getAnswer());
        }
    }

    @OnClick({R.id.no_one, R.id.no_two, R.id.no_three, R.id.no_four, R.id.no_five, R.id.no_six, R.id.no_seven, R.id.no_eight, R.id.no_nine, R.id.no_ten, R.id.no_eleven, R.id.no_twelve, R.id.no_thirteen, R.id.no_fourteen, R.id.no_fiveteen, R.id.no_sixteen, R.id.no_seventeen, R.id.no_eighteen, R.id.no_nineteen, R.id.no_twenty})
    public void onClickTestNumber(View view) {
        switch (view.getId()) {
            case R.id.no_one:
                showHideQuestion(contentQuestionOne);
                checkAnswer();
                break;
            case R.id.no_two:
                showHideQuestion(contentQuestionTwo);
                checkAnswer();
                break;
            case R.id.no_three:
                showHideQuestion(contentQuestionThree);
                checkAnswer();
                break;
            case R.id.no_four:
                showHideQuestion(contentQuestionFour);
                checkAnswer();
                break;
            case R.id.no_five:
                showHideQuestion(contentQuestionFive);
                checkAnswer();
                break;
            case R.id.no_six:
                showHideQuestion(contentQuestionSix);
                checkAnswer();
                break;
            case R.id.no_seven:
                showHideQuestion(contentQuestionSeven);
                checkAnswer();
                break;
            case R.id.no_eight:
                showHideQuestion(contentQuestionEight);
                checkAnswer();
                break;
            case R.id.no_nine:
                showHideQuestion(contentQuestionNine);
                checkAnswer();
                break;
            case R.id.no_ten:
                showHideQuestion(contentQuestionTen);
                checkAnswer();
                break;
            case R.id.no_eleven:
                showHideQuestion(contentQuestionEleven);
                checkAnswer();
                break;
            case R.id.no_twelve:
                showHideQuestion(contentQuestionTwelve);
                checkAnswer();
                break;
            case R.id.no_thirteen:
                showHideQuestion(contentQuestionThirteen);
                checkAnswer();
                break;
            case R.id.no_fourteen:
                showHideQuestion(contentQuestionFourteen);
                checkAnswer();
                break;
            case R.id.no_fiveteen:
                showHideQuestion(contentQuestionFiveteen);
                checkAnswer();
                break;
            case R.id.no_sixteen:
                showHideQuestion(contentQuestionSixteen);
                checkAnswer();
                break;
            case R.id.no_seventeen:
                showHideQuestion(contentQuestionSeventeen);
                checkAnswer();
                break;
            case R.id.no_eighteen:
                showHideQuestion(contentQuestionEighteen);
                checkAnswer();
                break;
            case R.id.no_nineteen:
                showHideQuestion(contentQuestionNineteen);
                checkAnswer();
                break;
            case R.id.no_twenty:
                showHideQuestion(contentQuestionTwenty);
                checkAnswer();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Please complete your online test", Toast.LENGTH_SHORT).show();
    }

    private void checkAnswer() {
        valueMultipleChoice();
        if (!strAnswerOne.isEmpty()) noOne.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerTwo.isEmpty()) noTwo.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerThree.isEmpty())
            noThree.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerFour.isEmpty())
            noFour.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerFive.isEmpty())
            noFive.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerSix.isEmpty()) noSix.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerSeven.isEmpty())
            noSeven.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerEight.isEmpty())
            noEight.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerNine.isEmpty())
            noNine.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerTen.isEmpty()) noTen.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerEleven.isEmpty())
            noEleven.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerTwelve.isEmpty())
            noTwelve.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerThirteen.isEmpty())
            noThirteen.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerFourteen.isEmpty())
            noFourteen.setBackgroundResource(R.drawable.bg_online_test_answer);
        if (!strAnswerFiveteen.isEmpty())
            noFiveteen.setBackgroundResource(R.drawable.bg_online_test_answer);

        if (!strAnswerSixteen.isEmpty())
            noSixteen.setBackgroundResource(R.drawable.bg_online_test_answer);
        else noSixteen.setBackgroundResource(R.drawable.bg_online_test_not_answer);

        if (!strAnswerSeventeen.isEmpty())
            noSeventeen.setBackgroundResource(R.drawable.bg_online_test_answer);
        else noSeventeen.setBackgroundResource(R.drawable.bg_online_test_not_answer);

        if (!strAnswerEighteen.isEmpty())
            noEighteen.setBackgroundResource(R.drawable.bg_online_test_answer);
        else noEighteen.setBackgroundResource(R.drawable.bg_online_test_not_answer);

        if (!strAnswerNineteen.isEmpty())
            noNineteen.setBackgroundResource(R.drawable.bg_online_test_answer);
        else noNineteen.setBackgroundResource(R.drawable.bg_online_test_not_answer);

        if (!strAnswerTwenty.isEmpty())
            noTwenty.setBackgroundResource(R.drawable.bg_online_test_answer);
        else noTwenty.setBackgroundResource(R.drawable.bg_online_test_not_answer);
    }

    private void showHideQuestion(LinearLayout v) {
        hideAllQuestion(v);
        if (v.getVisibility() == View.GONE) {
            v.setVisibility(View.VISIBLE);
        }
    }

    private void hideAllQuestion(LinearLayout lView) {
        for (int i = 0; i < contentQuestionList.size(); i++) {
            if (lView.getId() != contentQuestionList.get(i).getId()) {
                contentQuestionList.get(i).setVisibility(View.GONE);
            }
        }
    }

    private void valueMultipleChoice() {
        idQOne = rgQuestionOne.getCheckedRadioButtonId();
        answerOne = findViewById(idQOne);
        idQTwo = rgQuestionTwo.getCheckedRadioButtonId();
        answerTwo = findViewById(idQTwo);
        idQThree = rgQuestionThree.getCheckedRadioButtonId();
        answerThree = findViewById(idQThree);
        idQFour = rgQuestionFour.getCheckedRadioButtonId();
        answerFour = findViewById(idQFour);
        idQFive = rgQuestionFive.getCheckedRadioButtonId();
        answerFive = findViewById(idQFive);
        idQSix = rgQuestionSix.getCheckedRadioButtonId();
        answerSix = findViewById(idQSix);
        idQSeven = rgQuestionSeven.getCheckedRadioButtonId();
        answerSeven = findViewById(idQSeven);
        idQEight = rgQuestionEight.getCheckedRadioButtonId();
        answerEight = findViewById(idQEight);
        idQNine = rgQuestionNine.getCheckedRadioButtonId();
        answerNine = findViewById(idQNine);
        idQTen = rgQuestionTen.getCheckedRadioButtonId();
        answerTen = findViewById(idQTen);
        idQEleven = rgQuestionEleven.getCheckedRadioButtonId();
        answerEleven = findViewById(idQEleven);
        idQTwelve = rgQuestionTwelve.getCheckedRadioButtonId();
        answerTwelve = findViewById(idQTwelve);
        idQThirteen = rgQuestionThirteen.getCheckedRadioButtonId();
        answerThirteen = findViewById(idQThirteen);
        idQFourteen = rgQuestionFourteen.getCheckedRadioButtonId();
        answerFourteen = findViewById(idQFourteen);
        idQFiveteen = rgQuestionFiveteen.getCheckedRadioButtonId();
        answerFiveteen = findViewById(idQFiveteen);

        strAnswerOne = (answerOne == null) ? "" : answerOne.getText().toString();
        strAnswerTwo = (answerTwo == null) ? "" : answerTwo.getText().toString();
        strAnswerThree = (answerThree == null) ? "" : answerThree.getText().toString();
        strAnswerFour = (answerFour == null) ? "" : answerFour.getText().toString();
        strAnswerFive = (answerFive == null) ? "" : answerFive.getText().toString();
        strAnswerSix = (answerSix == null) ? "" : answerSix.getText().toString();
        strAnswerSeven = (answerSeven == null) ? "" : answerSeven.getText().toString();
        strAnswerEight = (answerEight == null) ? "" : answerEight.getText().toString();
        strAnswerNine = (answerNine == null) ? "" : answerNine.getText().toString();
        strAnswerTen = (answerTen == null) ? "" : answerTen.getText().toString();
        strAnswerEleven = (answerEleven == null) ? "" : answerEleven.getText().toString();
        strAnswerTwelve = (answerTwelve == null) ? "" : answerTwelve.getText().toString();
        strAnswerThirteen = (answerThirteen == null) ? "" : answerThirteen.getText().toString();
        strAnswerFourteen = (answerFourteen == null) ? "" : answerFourteen.getText().toString();
        strAnswerFiveteen = (answerFiveteen == null) ? "" : answerFiveteen.getText().toString();
        strAnswerSixteen = (etAnswerSixteen.getText().toString() == null) ? "" : etAnswerSixteen.getText().toString().trim();
        strAnswerSeventeen = (etAnswerSeventeen.getText().toString() == null) ? "" : etAnswerSeventeen.getText().toString().trim();
        strAnswerEighteen = (etAnswerEighteen.getText().toString() == null) ? "" : etAnswerEighteen.getText().toString().trim();
        strAnswerNineteen = (etAnswerNineteen.getText().toString() == null) ? "" : etAnswerNineteen.getText().toString().trim();
        strAnswerTwenty = (etAnswerTwenty.getText().toString() == null) ? "" : etAnswerTwenty.getText().toString().trim();
    }

    @OnClick(R.id.btn_submit_online_test)
    public void onClickedSubmit() {
        valueMultipleChoice();
        Log.i("answerOnlineTest", strAnswerOne + "\n" + strAnswerTwo + "\n" + strAnswerThree + "\n" + strAnswerFour + "\n" + strAnswerFive + "\n" + strAnswerSix + "\n" + strAnswerSeven + "\n" + strAnswerEight + "\n" + strAnswerNine + "\n" + strAnswerTen + "\n" + strAnswerEleven + "\n" + strAnswerTwelve + "\n" + strAnswerThirteen + "\n" + strAnswerFourteen + "\n" + strAnswerFiveteen + "\n" + strAnswerSixteen + "\n" + strAnswerSeventeen + "\n" + strAnswerEighteen + "\n" + strAnswerNineteen + "\n" + strAnswerTwenty + "\n");
        if (strAnswerOne.isEmpty() || strAnswerTwo.isEmpty() || strAnswerThree.isEmpty() || strAnswerFour.isEmpty() || strAnswerFive.isEmpty() || strAnswerSix.isEmpty() || strAnswerSeven.isEmpty() || strAnswerEight.isEmpty() || strAnswerNine.isEmpty() || strAnswerTen.isEmpty() || strAnswerEleven.isEmpty() || strAnswerTwelve.isEmpty() || strAnswerThirteen.isEmpty() || strAnswerFourteen.isEmpty() || strAnswerFiveteen.isEmpty() || strAnswerSixteen.isEmpty() || strAnswerSeventeen.isEmpty() || strAnswerEighteen.isEmpty() || strAnswerNineteen.isEmpty() || strAnswerTwenty.isEmpty()) {
            Toast.makeText(this, "Please complete your online test", Toast.LENGTH_SHORT).show();
        } else {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_confirmation);
            TextView tvContentConfirmations = (TextView) dialog.findViewById(R.id.tv_content_confirmations);
            Button btnNo = (Button) dialog.findViewById(R.id.btn_no);
            Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
            tvContentConfirmations.setText("Are you sure you have completed this test?");
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postAnswerOnlineTest();
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    private void postAnswerOnlineTest() {
        progressDialog.show();
        intValueAnswerOne = (strAnswerOne.equals(response.body().getData().getPertanyaan().get(0).getValue())) ? 1 : 0;
        intValueAnswerTwo = (strAnswerTwo.equals(response.body().getData().getPertanyaan().get(1).getValue())) ? 1 : 0;
        intValueAnswerThree = (strAnswerThree.equals(response.body().getData().getPertanyaan().get(2).getValue())) ? 1 : 0;
        intValueAnswerFour = (strAnswerFour.equals(response.body().getData().getPertanyaan().get(3).getValue())) ? 1 : 0;
        intValueAnswerFive = (strAnswerFive.equals(response.body().getData().getPertanyaan().get(4).getValue())) ? 1 : 0;
        intValueAnswerSix = (strAnswerSix.equals(response.body().getData().getPertanyaan().get(5).getValue())) ? 1 : 0;
        intValueAnswerSeven = (strAnswerSeven.equals(response.body().getData().getPertanyaan().get(6).getValue())) ? 1 : 0;
        intValueAnswerEight = (strAnswerEight.equals(response.body().getData().getPertanyaan().get(7).getValue())) ? 1 : 0;
        intValueAnswerNine = (strAnswerNine.equals(response.body().getData().getPertanyaan().get(8).getValue())) ? 1 : 0;
        intValueAnswerTen = (strAnswerTen.equals(response.body().getData().getPertanyaan().get(9).getValue())) ? 1 : 0;
        intValueAnswerEleven = (strAnswerEleven.equals(response.body().getData().getPertanyaan().get(10).getValue())) ? 1 : 0;
        intValueAnswerTwelve = (strAnswerTwelve.equals(response.body().getData().getPertanyaan().get(11).getValue())) ? 1 : 0;
        intValueAnswerThirteen = (strAnswerThirteen.equals(response.body().getData().getPertanyaan().get(12).getValue())) ? 1 : 0;
        intValueAnswerFourteen = (strAnswerFourteen.equals(response.body().getData().getPertanyaan().get(13).getValue())) ? 1 : 0;
        intValueAnswerFiveteen = (strAnswerFiveteen.equals(response.body().getData().getPertanyaan().get(14).getValue())) ? 1 : 0;
        resultMultipleChoice = ((intValueAnswerOne + intValueAnswerTwo + intValueAnswerThree + intValueAnswerFour + intValueAnswerFive + intValueAnswerSix + intValueAnswerSeven + intValueAnswerEight + intValueAnswerNine + intValueAnswerTen + intValueAnswerEleven + intValueAnswerTwelve + intValueAnswerThirteen + intValueAnswerFourteen + intValueAnswerFiveteen) * 100) / 15;
        Log.i("resultMultipleChoice", String.valueOf(resultMultipleChoice));

        Map<String, RequestBody> map = new HashMap<>();
        map.put("slug", Util.toTextRequestBody(slug));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(0).getId()), Util.toTextRequestBody(strAnswerOne));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(1).getId()), Util.toTextRequestBody(strAnswerTwo));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(2).getId()), Util.toTextRequestBody(strAnswerThree));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(3).getId()), Util.toTextRequestBody(strAnswerFour));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(4).getId()), Util.toTextRequestBody(strAnswerFive));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(5).getId()), Util.toTextRequestBody(strAnswerSix));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(6).getId()), Util.toTextRequestBody(strAnswerSeven));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(7).getId()), Util.toTextRequestBody(strAnswerEight));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(8).getId()), Util.toTextRequestBody(strAnswerNine));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(9).getId()), Util.toTextRequestBody(strAnswerTen));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(10).getId()), Util.toTextRequestBody(strAnswerEleven));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(11).getId()), Util.toTextRequestBody(strAnswerTwelve));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(12).getId()), Util.toTextRequestBody(strAnswerThirteen));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(13).getId()), Util.toTextRequestBody(strAnswerFourteen));
        map.put("id_" + String.valueOf(response.body().getData().getPertanyaan().get(14).getId()), Util.toTextRequestBody(strAnswerFiveteen));
        map.put("id_" + String.valueOf(responseEssay.body().getData().getPertanyaan().get(0).getId()), Util.toTextRequestBody(strAnswerSixteen));
        map.put("id_" + String.valueOf(responseEssay.body().getData().getPertanyaan().get(1).getId()), Util.toTextRequestBody(strAnswerSeventeen));
        map.put("id_" + String.valueOf(responseEssay.body().getData().getPertanyaan().get(2).getId()), Util.toTextRequestBody(strAnswerEighteen));
        map.put("id_" + String.valueOf(responseEssay.body().getData().getPertanyaan().get(3).getId()), Util.toTextRequestBody(strAnswerNineteen));
        map.put("id_" + String.valueOf(responseEssay.body().getData().getPertanyaan().get(4).getId()), Util.toTextRequestBody(strAnswerTwenty));
        map.put("result_multiple_choice", Util.toTextRequestBody(String.valueOf(resultMultipleChoice)));
        Log.d("mapmap", String.valueOf(map));
        mApiService.onlineTestPost(token, map).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    startActivity(new Intent(OnlineTestActivity.this, HomeActivity.class));
                    finish();
                } else {
                    progressDialog.dismiss();
                    Util.showToast(OnlineTestActivity.this, response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Util.showToast(OnlineTestActivity.this, "onFailure");
            }
        });
    }
}
