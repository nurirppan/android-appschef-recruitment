package com.appschef.recruitment.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.model.response.ScheduleResponse;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InterviewScheduleActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_name_invitation)
    TextView tvNameInvitation;
    @BindView(R.id.tv_day_date_schedule)
    TextView tvDayDateSchedule;
    @BindView(R.id.tv_until_finish)
    TextView tvUntilFinish;
    @BindView(R.id.company_address)
    TextView companyAddress;
    private String recruitment_id;
    private String type;
    private String title;
    private ApiService mApiService;
    private String token;
    private SharedPreferences sharedPreferences;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interview_schedule);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        recruitment_id = getIntent().getStringExtra("recruitment_id");
        type = getIntent().getStringExtra("type");
        title = getIntent().getStringExtra("title");

        if (recruitment_id != null && type != null && title != null) {
            mApiService.getSchedule(recruitment_id, token, type).enqueue(new Callback<BaseResponse<ScheduleResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<ScheduleResponse>> call, Response<BaseResponse<ScheduleResponse>> response) {
                    if (response.isSuccessful()) {
                        tvName.setText(response.body().getData().getName());
                        tvNameInvitation.setText("Undangan interview untuk " + response.body().getData().getName() + " di PT.Appschef");
                        tvDayDateSchedule.setText(response.body().getData().getDate());
                        tvUntilFinish.setText(response.body().getData().getStartTime() + " - " + response.body().getData().getEndTime());
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        Util.showToast(InterviewScheduleActivity.this, response.message());
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<ScheduleResponse>> call, Throwable t) {
                    progressDialog.dismiss();
                    Util.showToast(InterviewScheduleActivity.this, "onFailure");
                }
            });
        }
    }
}
