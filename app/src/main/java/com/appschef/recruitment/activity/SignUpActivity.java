package com.appschef.recruitment.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.et_fullname_register)
    TextInputLayout etFullnameRegister;
    @BindView(R.id.et_email_register)
    TextInputLayout etEmailRegister;
    @BindView(R.id.et_password_register)
    TextInputLayout etPasswordRegister;
    @BindView(R.id.et_re_password_register)
    TextInputLayout etRePasswordRegister;

    @BindView(R.id.btn_forget_password)
    TextView btnForgetPassword;
    @BindView(R.id.btn_login)
    TextView btnLogin;
    @BindView(R.id.btn_register)
    Button btnRegister;

    ApiService mApiService;

    ProgressDialog progressDialog;

    private String fullname;
    private String email;
    private String password;
    private String rePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setTitle("");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        mApiService = UtilsApi.getApiService();
    }


    @OnClick({R.id.btn_forget_password, R.id.btn_login, R.id.btn_register})
    public void onViewClicked(View view) {
        fullname = etFullnameRegister.getEditText().getText().toString().trim();
        email = etEmailRegister.getEditText().getText().toString().trim();
        password = etPasswordRegister.getEditText().getText().toString().trim();
        rePassword = etRePasswordRegister.getEditText().getText().toString().trim();

        switch (view.getId()) {
            case R.id.btn_forget_password:
                startActivity(new Intent(SignUpActivity.this, FogetPasswordActivity.class));
                finish();
                break;
            case R.id.btn_login:
                startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
                finish();
                break;
            case R.id.btn_register:
                if (checkEmpty(fullname, email, password, rePassword)) {
                    if (!password.equals(rePassword)) {
                        Toast.makeText(this, "incorrect password & re-password", Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.show();
                        requestRegister(fullname, email, password);
                    }
                }

                Log.i("edtxFullname", fullname);
                Log.i("edtxEmail", email);
                Log.i("edtxPassword", password);
                Log.i("edtxRePassword", rePassword);

                break;
        }
    }

    private void requestRegister(String fullname, String email, String password) {
        mApiService.registerRequest(fullname, email, password).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    Util.showToast(SignUpActivity.this, response.message());
                    startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
                    finish();
                } else {
                    progressDialog.dismiss();
                    Util.showToast(SignUpActivity.this, response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Util.showToast(SignUpActivity.this, "onFailure");
            }
        });
    }

    private boolean checkEmpty(String fullname, String email, String password, String rePassword) {
        if (this.fullname.isEmpty()) etFullnameRegister.setError("fullname must be filled");
        else etFullnameRegister.setError(null);
        if (this.email.isEmpty()) etEmailRegister.setError("email must be filled");
        else etEmailRegister.setError(null);
        if (this.password.isEmpty()) etPasswordRegister.setError("password must be filled");
        else etPasswordRegister.setError(null);
        if (this.rePassword.isEmpty()) etRePasswordRegister.setError("re-password must be filled");
        else etRePasswordRegister.setError(null);

        return !this.fullname.isEmpty() && !this.email.isEmpty() && !this.password.isEmpty() && !this.rePassword.isEmpty();
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Notice");
        alertDialog.setMessage("are you sure you want to close aplication ?");
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                System.exit(0);
            }
        });
        alertDialog.show();
    }
}
