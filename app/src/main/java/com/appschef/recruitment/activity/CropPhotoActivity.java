package com.appschef.recruitment.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.bus.CropPhotoEvent;
import com.appschef.recruitment.model.bus.RefreshActivity;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropPhotoActivity extends AppCompatActivity {

    @BindView(R.id.iv_crop_image_view)
    CropImageView ivCropImageView;
    @BindView(R.id.btn_crop_image)
    Button btnCropImage;

    ApiService mApiService;
    SharedPreferences.Editor editor;
    private Uri myUri;
    private int typePhoto;
    SharedPreferences sharedPreferences;
    private String token;
    private ProgressDialog progressDialog;
    private MultipartBody.Part attachmentPart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_photo);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");
        mApiService = UtilsApi.getApiService();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        typePhoto = getIntent().getExtras().getInt("requestCode");
        myUri = getIntent().getData();

        ivCropImageView.setImageUriAsync(myUri);
    }

    @OnClick(R.id.btn_crop_image)
    public void onViewClicked() {
        progressDialog.show();
        Bitmap cropped = ivCropImageView.getCroppedImage(2500, 4000);
        EventBus.getDefault().post(new CropPhotoEvent(typePhoto, cropped));

        Uri tempUri = Util.getImageUri(this, cropped);
        final File fileUri = new File(Util.getRealPathFromURI(this, tempUri));
        attachmentPart = prepareFilePart("profile_picture", fileUri);

        mApiService.imageProfilePost(token, attachmentPart).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    editor = sharedPreferences.edit();
                    editor.putString(getString(R.string.sharedpref_avatar), String.valueOf(fileUri));
                    editor.apply();
                    EventBus.getDefault().post(new RefreshActivity(String.valueOf(fileUri)));
                    progressDialog.dismiss();
                    finish();
                } else {
                    progressDialog.dismiss();
                    Util.showToast(CropPhotoActivity.this, response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Util.showToast(CropPhotoActivity.this,  "onFailure");
            }
        });
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, File file) {
        // create RequestBody instance from file
        RequestBody requestFile = Util.toImageRequestBody(file);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }
}
