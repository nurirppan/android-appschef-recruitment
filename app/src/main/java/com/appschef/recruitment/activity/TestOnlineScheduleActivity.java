package com.appschef.recruitment.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.model.response.ScheduleResponse;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestOnlineScheduleActivity extends AppCompatActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_type_job)
    TextView tvTypeJob;
    @BindView(R.id.tv_day_date_schedule)
    TextView tvDayDateSchedule;
    @BindView(R.id.tv_until_finish)
    TextView tvUntilFinish;
    @BindView(R.id.btn_tes_online)
    Button btnTesOnline;
    private String recruitment_id;
    private String type;
    private String title;

    private ApiService mApiService;
    private SharedPreferences sharedPreferences;
    private String token;
    private ProgressDialog progressDialog;
    private String slug;
    private Response<BaseResponse<ScheduleResponse>> response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_online_schedule);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        recruitment_id = getIntent().getStringExtra("recruitment_id");
        type = getIntent().getStringExtra("type");
        title = getIntent().getStringExtra("title");

        if (recruitment_id != null && type != null && title != null) {
            mApiService.getSchedule(recruitment_id, token, type).enqueue(new Callback<BaseResponse<ScheduleResponse>>() {
                @Override
                public void onResponse(Call<BaseResponse<ScheduleResponse>> call, Response<BaseResponse<ScheduleResponse>> response) {
                    if (response.isSuccessful()) {
                        setScheduleOnlineTest(response);
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        Util.showToast(TestOnlineScheduleActivity.this, response.message());
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<ScheduleResponse>> call, Throwable t) {
                    progressDialog.dismiss();
                    Util.showToast(TestOnlineScheduleActivity.this, "onFailure");
                }
            });
        }
    }

    private void setScheduleOnlineTest(Response<BaseResponse<ScheduleResponse>> response) {
        this.response = response;
        slug = response.body().getData().getSlug();
        tvName.setText(response.body().getData().getName());
        tvTypeJob.setText("Terima kasih telah mengirimkan lamaran lowongan pekerjaan sebagai " + slug + " Developer untuk PT Appschef melalui aplikasi kami yaitu Appschef Recruitment.");
        tvDayDateSchedule.setText(response.body().getData().getStartDate() + " - " + response.body().getData().getEndDate());

        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        int intStartDate = Integer.parseInt(response.body().getData().getStartDate().replace("-", ""));
        int intEndDate = Integer.parseInt(response.body().getData().getEndDate().replace("-", ""));
        int currentDate = Integer.parseInt(dateFormat.format(Calendar.getInstance().getTime()));
        if (currentDate < intStartDate && currentDate > intEndDate) {
            btnTesOnline.setEnabled(false);
            Util.showToast(TestOnlineScheduleActivity.this, "Mohon Maaf, Anda Melebihi Batas Waktu Pengerjaan Tes Online");
        }
    }

    @OnClick(R.id.btn_tes_online)
    public void onClickConfirmationOnlineTest(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_confirmation);
        TextView tvContentConfirmations = (TextView) dialog.findViewById(R.id.tv_content_confirmations);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);
        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        tvContentConfirmations.setText(getString(R.string.txt_are_you_sure_want_to) + " start online test ?");
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestOnlineScheduleActivity.this, OnlineTestActivity.class);
                intent.putExtra("slug", slug);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }
}
