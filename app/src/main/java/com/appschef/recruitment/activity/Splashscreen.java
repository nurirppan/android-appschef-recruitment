package com.appschef.recruitment.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.PermissionHelper;

public class Splashscreen extends AppCompatActivity {

    PermissionHelper permissionHelper;
    //    private IntentLauncher launcher = new IntentLauncher();
    private static long SLEEP_TIME = 1000;
    private SharedPreferences sharedPref;
    private String token;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splashscreen);
        sharedPref = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        token = sharedPref.getString(getResources().getString(R.string.sharedpref_token), "");
        email = sharedPref.getString(getResources().getString(R.string.sharedpref_email), "");

        permissionHelper = new PermissionHelper(this);

        checkAndRequestPermissions();

    }

    private void checkAndRequestPermissions() {
        permissionHelper.permissionListener(new PermissionHelper.PermissionListener() {
            @Override
            public void onPermissionCheckDone() {
                if (token.isEmpty()) {
                    startActivity(new Intent(Splashscreen.this, SignInActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(Splashscreen.this, HomeActivity.class));
                    finish();
                }
            }
        });
        permissionHelper.checkAndRequestPermissions();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHelper.onRequestCallBack(requestCode, permissions, grantResults);
    }

//    private class IntentLauncher extends Thread {
//        @Override
//        public void run() {
//            try {
//                Thread.sleep(SLEEP_TIME);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            if (ConstantTest.IS_LOGIN) {
//                startActivity(new Intent(Splashscreen.this, SignInActivity.class));
//            } else {
//                startActivity(new Intent(Splashscreen.this, HomeActivity.class));
//            }
//            finish();
//            super.run();
//        }
//    }
}
