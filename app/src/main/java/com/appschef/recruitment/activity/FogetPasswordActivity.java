package com.appschef.recruitment.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appschef.recruitment.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FogetPasswordActivity extends AppCompatActivity {

    @BindView(R.id.et_email_forget_password)
    TextInputLayout etEmailForgetPassword;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.btn_login)
    TextView btnLogin;
    @BindView(R.id.btn_register)
    TextView btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foget_password);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_submit, R.id.btn_login, R.id.btn_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                finish();
                break;
            case R.id.btn_login:
                startActivity(new Intent(FogetPasswordActivity.this, SignInActivity.class));
                finish();
                finish();
                break;
            case R.id.btn_register:
                startActivity(new Intent(FogetPasswordActivity.this, SignUpActivity.class));
                finish();
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FogetPasswordActivity.this, SignInActivity.class));
        finish();
    }
}
