package com.appschef.recruitment.model.response.childresponse.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SkillProgramGet {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("sp_nama_keahlian")
    @Expose
    public String spNamaKeahlian;
    @SerializedName("sp_deskripsi")
    @Expose
    public String spDeskripsi;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSpNamaKeahlian() {
        return spNamaKeahlian;
    }

    public void setSpNamaKeahlian(String spNamaKeahlian) {
        this.spNamaKeahlian = spNamaKeahlian;
    }

    public String getSpDeskripsi() {
        return spDeskripsi;
    }

    public void setSpDeskripsi(String spDeskripsi) {
        this.spDeskripsi = spDeskripsi;
    }
}
