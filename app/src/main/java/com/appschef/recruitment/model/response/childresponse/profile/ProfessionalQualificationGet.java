package com.appschef.recruitment.model.response.childresponse.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfessionalQualificationGet {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("pq_nama_kualifikasi_profesional")
    @Expose
    public String pqNamaKualifikasiProfesional;
    @SerializedName("pq_diselenggarakan_oleh")
    @Expose
    public String pqDiselenggarakanOleh;
    @SerializedName("pq_diterima_tahun")
    @Expose
    public String pqDiterimaTahun;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPqNamaKualifikasiProfesional() {
        return pqNamaKualifikasiProfesional;
    }

    public void setPqNamaKualifikasiProfesional(String pqNamaKualifikasiProfesional) {
        this.pqNamaKualifikasiProfesional = pqNamaKualifikasiProfesional;
    }

    public String getPqDiselenggarakanOleh() {
        return pqDiselenggarakanOleh;
    }

    public void setPqDiselenggarakanOleh(String pqDiselenggarakanOleh) {
        this.pqDiselenggarakanOleh = pqDiselenggarakanOleh;
    }

    public String getPqDiterimaTahun() {
        return pqDiterimaTahun;
    }

    public void setPqDiterimaTahun(String pqDiterimaTahun) {
        this.pqDiterimaTahun = pqDiterimaTahun;
    }
}
