package com.appschef.recruitment.model.response.parentresponse.profile;

import com.appschef.recruitment.model.response.childresponse.profile.PersonalDataGet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonalDataParent {

    @SerializedName("personaldata")
    @Expose
    public PersonalDataGet personalDataGets;

    public PersonalDataGet getPersonalDataGets() {
        return personalDataGets;
    }

    public void setPersonalDataGets(PersonalDataGet personalDataGets) {
        this.personalDataGets = personalDataGets;
    }
}
