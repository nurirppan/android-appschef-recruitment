package com.appschef.recruitment.model.response.parentresponse.profile;

import com.appschef.recruitment.model.response.childresponse.profile.EducationalBackgroundGet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EducationalBackgroundParent {

    @SerializedName("educational")
    @Expose
    public List<EducationalBackgroundGet> educationalBackgroundGetList;

    public List<EducationalBackgroundGet> getEducationalBackgroundGetList() {
        return educationalBackgroundGetList;
    }

    public void setEducationalBackgroundGetList(List<EducationalBackgroundGet> educationalBackgroundGetList) {
        this.educationalBackgroundGetList = educationalBackgroundGetList;
    }
}
