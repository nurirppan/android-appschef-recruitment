package com.appschef.recruitment.model;

import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("message")
    private String message;
    @SerializedName("succeded")
    private boolean succeded;
    @SerializedName("code")
    private int code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSucceded() {
        return succeded;
    }

    public void setSucceded(boolean succeded) {
        this.succeded = succeded;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
