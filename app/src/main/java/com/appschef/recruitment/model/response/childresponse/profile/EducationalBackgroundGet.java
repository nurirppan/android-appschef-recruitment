package com.appschef.recruitment.model.response.childresponse.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EducationalBackgroundGet {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("eb_tingkat_pendidikan")
    @Expose
    public String ebTingkatPendidikan;
    @SerializedName("eb_tahun_kelulusan")
    @Expose
    public String ebTahunKelulusan;
    @SerializedName("eb_program_studi")
    @Expose
    public String ebProgramStudi;
    @SerializedName("eb_ipk")
    @Expose
    public String ebIpk;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEbTingkatPendidikan() {
        return ebTingkatPendidikan;
    }

    public void setEbTingkatPendidikan(String ebTingkatPendidikan) {
        this.ebTingkatPendidikan = ebTingkatPendidikan;
    }

    public String getEbTahunKelulusan() {
        return ebTahunKelulusan;
    }

    public void setEbTahunKelulusan(String ebTahunKelulusan) {
        this.ebTahunKelulusan = ebTahunKelulusan;
    }

    public String getEbProgramStudi() {
        return ebProgramStudi;
    }

    public void setEbProgramStudi(String ebProgramStudi) {
        this.ebProgramStudi = ebProgramStudi;
    }

    public String getEbIpk() {
        return ebIpk;
    }

    public void setEbIpk(String ebIpk) {
        this.ebIpk = ebIpk;
    }
}
