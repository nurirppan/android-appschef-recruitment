package com.appschef.recruitment.model.response.childresponse.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkExperienceGet {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("we_bulan_mulai")
    @Expose
    public String weBulanMulai;
    @SerializedName("we_tahun_mulai")
    @Expose
    public String weTahunMulai;
    @SerializedName("we_bulan_selesai")
    @Expose
    public String weBulanSelesai;
    @SerializedName("we_tahun_selesai")
    @Expose
    public String weTahunSelesai;
    @SerializedName("we_nama_perusahaan")
    @Expose
    public String weNamaPerusahaan;
    @SerializedName("we_posisi_pekerjaan")
    @Expose
    public String wePosisiPekerjaan;
    @SerializedName("we_uraian_Tugas")
    @Expose
    public String weUraianTugas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWeBulanMulai() {
        return weBulanMulai;
    }

    public void setWeBulanMulai(String weBulanMulai) {
        this.weBulanMulai = weBulanMulai;
    }

    public String getWeTahunMulai() {
        return weTahunMulai;
    }

    public void setWeTahunMulai(String weTahunMulai) {
        this.weTahunMulai = weTahunMulai;
    }

    public String getWeBulanSelesai() {
        return weBulanSelesai;
    }

    public void setWeBulanSelesai(String weBulanSelesai) {
        this.weBulanSelesai = weBulanSelesai;
    }

    public String getWeTahunSelesai() {
        return weTahunSelesai;
    }

    public void setWeTahunSelesai(String weTahunSelesai) {
        this.weTahunSelesai = weTahunSelesai;
    }

    public String getWeNamaPerusahaan() {
        return weNamaPerusahaan;
    }

    public void setWeNamaPerusahaan(String weNamaPerusahaan) {
        this.weNamaPerusahaan = weNamaPerusahaan;
    }

    public String getWePosisiPekerjaan() {
        return wePosisiPekerjaan;
    }

    public void setWePosisiPekerjaan(String wePosisiPekerjaan) {
        this.wePosisiPekerjaan = wePosisiPekerjaan;
    }

    public String getWeUraianTugas() {
        return weUraianTugas;
    }

    public void setWeUraianTugas(String weUraianTugas) {
        this.weUraianTugas = weUraianTugas;
    }
}
