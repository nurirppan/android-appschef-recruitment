package com.appschef.recruitment.model.response.parentresponse.onlinetes;

import com.appschef.recruitment.model.response.childresponse.onlinetest.QuestionTestResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OnlineTestParent {

    @SerializedName("tipe_soal")
    @Expose
    public String tipeSoal;
    @SerializedName("slug")
    @Expose
    public String slug;
    @SerializedName("version")
    @Expose
    public Integer version;
    @SerializedName("pertanyaan")
    @Expose
    public List<QuestionTestResponse> pertanyaan;

    public String getTipeSoal() {
        return tipeSoal;
    }

    public void setTipeSoal(String tipeSoal) {
        this.tipeSoal = tipeSoal;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public List<QuestionTestResponse> getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(List<QuestionTestResponse> pertanyaan) {
        this.pertanyaan = pertanyaan;
    }
}
