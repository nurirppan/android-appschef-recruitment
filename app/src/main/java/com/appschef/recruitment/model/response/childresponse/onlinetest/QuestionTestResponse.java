package com.appschef.recruitment.model.response.childresponse.onlinetest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionTestResponse {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("question")
    @Expose
    public String question;
    @SerializedName("value")
    @Expose
    public String value;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("slug")
    @Expose
    public String slug;
    @SerializedName("answers")
    @Expose
    public List<AnswerTestResponse> answers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public List<AnswerTestResponse> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerTestResponse> answers) {
        this.answers = answers;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
