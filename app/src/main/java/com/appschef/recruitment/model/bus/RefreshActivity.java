package com.appschef.recruitment.model.bus;

public class RefreshActivity {
    private String strFile;

    public RefreshActivity(String strFile) {
        this.strFile = strFile;
    }

    public String getStrFile() {
        return strFile;
    }

    public void setStrFile(String strFile) {
        this.strFile = strFile;
    }
}
