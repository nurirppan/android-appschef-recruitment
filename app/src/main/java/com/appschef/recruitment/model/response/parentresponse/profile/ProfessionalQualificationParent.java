package com.appschef.recruitment.model.response.parentresponse.profile;

import com.appschef.recruitment.model.response.childresponse.profile.ProfessionalQualificationGet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfessionalQualificationParent {

    @SerializedName("professionalqualification")
    @Expose
    public List<ProfessionalQualificationGet> professionalQualificationGetList;

    public List<ProfessionalQualificationGet> getProfessionalQualificationGetList() {
        return professionalQualificationGetList;
    }

    public void setProfessionalQualificationGetList(List<ProfessionalQualificationGet> professionalQualificationGetList) {
        this.professionalQualificationGetList = professionalQualificationGetList;
    }
}
