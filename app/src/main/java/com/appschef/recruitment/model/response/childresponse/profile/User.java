package com.appschef.recruitment.model.response.childresponse.profile;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    public Integer id;
    @SerializedName("email")
    public String email;
    @SerializedName("avatar_url")
    public String avatar;
    @SerializedName("full_name")
    public String full_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}
