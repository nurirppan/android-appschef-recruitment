package com.appschef.recruitment.model.response.childresponse.onlinetest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnswerTestResponse {
    @SerializedName("questionId")
    @Expose
    public Integer questionId;
    @SerializedName("answer")
    @Expose
    public String answer;

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
