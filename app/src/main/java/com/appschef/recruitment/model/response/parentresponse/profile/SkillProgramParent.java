package com.appschef.recruitment.model.response.parentresponse.profile;

import com.appschef.recruitment.model.response.childresponse.profile.SkillProgramGet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SkillProgramParent {

    @SerializedName("expertiseprogram")
    @Expose
    public List<SkillProgramGet> skillProgramGetList;

    public List<SkillProgramGet> getSkillProgramGetList() {
        return skillProgramGetList;
    }

    public void setSkillProgramGetList(List<SkillProgramGet> skillProgramGetList) {
        this.skillProgramGetList = skillProgramGetList;
    }
}
