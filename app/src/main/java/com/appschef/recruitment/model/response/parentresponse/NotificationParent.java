package com.appschef.recruitment.model.response.parentresponse;

import com.appschef.recruitment.model.response.childresponse.NotificationResponse;
import com.appschef.recruitment.model.response.childresponse.onlinetest.QuestionTestResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationParent {
    @SerializedName("notifications")
    @Expose
    public List<NotificationResponse> notifications;

    public List<NotificationResponse> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationResponse> notifications) {
        this.notifications = notifications;
    }
}
