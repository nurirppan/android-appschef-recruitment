package com.appschef.recruitment.model.response.parentresponse.profile;

import com.appschef.recruitment.model.response.childresponse.profile.OrganizationalExperienceGet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrganizationalExperienceParent {

    @SerializedName("organizationexperience")
    @Expose
    public List<OrganizationalExperienceGet> organizationalExperienceGetList;

    public List<OrganizationalExperienceGet> getOrganizationalExperienceGetList() {
        return organizationalExperienceGetList;
    }

    public void setOrganizationalExperienceGetList(List<OrganizationalExperienceGet> organizationalExperienceGetList) {
        this.organizationalExperienceGetList = organizationalExperienceGetList;
    }
}
