package com.appschef.recruitment.model.response.parentresponse.profile;

import com.appschef.recruitment.model.response.childresponse.profile.AchievementGet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AchievementParent {

    @SerializedName("achievement")
    @Expose
    public List<AchievementGet> achievementGetList;

    public List<AchievementGet> getAchievementGetList() {
        return achievementGetList;
    }

    public void setAchievementGetList(List<AchievementGet> achievementGetList) {
        this.achievementGetList = achievementGetList;
    }
}
