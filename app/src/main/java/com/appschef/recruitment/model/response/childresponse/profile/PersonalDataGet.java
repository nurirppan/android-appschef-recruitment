package com.appschef.recruitment.model.response.childresponse.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonalDataGet {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("pd_ktp")
    @Expose
    public String pdKtp;
    @SerializedName("pd_username")
    @Expose
    public String pdUsername;
    @SerializedName("pd_nama_lengkap")
    @Expose
    public String pdNamaLengkap;
    @SerializedName("pd_jenis_kelamin")
    @Expose
    public String pdJenisKelamin;
    @SerializedName("pd_tempat_lahir")
    @Expose
    public String pdTempatLahir;
    @SerializedName("pd_tanggal_lahir")
    @Expose
    public String pdTanggalLahir;
    @SerializedName("pd_umur")
    @Expose
    public String pdUmur;
    @SerializedName("pd_kewarganegaraan")
    @Expose
    public String pdKewarganegaraan;
    @SerializedName("pd_status_pernikahan")
    @Expose
    public String pdStatusPernikahan;
    @SerializedName("pd_rt")
    @Expose
    public String pdRt;
    @SerializedName("pd_rw")
    @Expose
    public String pdRw;
    @SerializedName("pd_kelurahan")
    @Expose
    public String pdKelurahan;
    @SerializedName("pd_kecamatan")
    @Expose
    public String pdKecamatan;
    @SerializedName("pd_kota")
    @Expose
    public String pdKota;
    @SerializedName("pd_telepon_rumah")
    @Expose
    public String pdTeleponRumah;
    @SerializedName("pd_nomor_handphone_satu")
    @Expose
    public String pdNomorHandphoneSatu;
    @SerializedName("pd_nomor_handphone_dua")
    @Expose
    public String pdNomorHandphoneDua;
    @SerializedName("pd_kode_pos")
    @Expose
    public String pdKodePos;
    @SerializedName("pd_email")
    @Expose
    public String pdEmail;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("pd_alamat")
    @Expose
    public String pdAlamat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPdKtp() {
        return pdKtp;
    }

    public void setPdKtp(String pdKtp) {
        this.pdKtp = pdKtp;
    }

    public String getPdUsername() {
        return pdUsername;
    }

    public void setPdUsername(String pdUsername) {
        this.pdUsername = pdUsername;
    }

    public String getPdNamaLengkap() {
        return pdNamaLengkap;
    }

    public void setPdNamaLengkap(String pdNamaLengkap) {
        this.pdNamaLengkap = pdNamaLengkap;
    }

    public String getPdJenisKelamin() {
        return pdJenisKelamin;
    }

    public void setPdJenisKelamin(String pdJenisKelamin) {
        this.pdJenisKelamin = pdJenisKelamin;
    }

    public String getPdTempatLahir() {
        return pdTempatLahir;
    }

    public void setPdTempatLahir(String pdTempatLahir) {
        this.pdTempatLahir = pdTempatLahir;
    }

    public String getPdTanggalLahir() {
        return pdTanggalLahir;
    }

    public void setPdTanggalLahir(String pdTanggalLahir) {
        this.pdTanggalLahir = pdTanggalLahir;
    }

    public String getPdUmur() {
        return pdUmur;
    }

    public void setPdUmur(String pdUmur) {
        this.pdUmur = pdUmur;
    }

    public String getPdKewarganegaraan() {
        return pdKewarganegaraan;
    }

    public void setPdKewarganegaraan(String pdKewarganegaraan) {
        this.pdKewarganegaraan = pdKewarganegaraan;
    }

    public String getPdStatusPernikahan() {
        return pdStatusPernikahan;
    }

    public void setPdStatusPernikahan(String pdStatusPernikahan) {
        this.pdStatusPernikahan = pdStatusPernikahan;
    }

    public String getPdRt() {
        return pdRt;
    }

    public void setPdRt(String pdRt) {
        this.pdRt = pdRt;
    }

    public String getPdRw() {
        return pdRw;
    }

    public void setPdRw(String pdRw) {
        this.pdRw = pdRw;
    }

    public String getPdKelurahan() {
        return pdKelurahan;
    }

    public void setPdKelurahan(String pdKelurahan) {
        this.pdKelurahan = pdKelurahan;
    }

    public String getPdKecamatan() {
        return pdKecamatan;
    }

    public void setPdKecamatan(String pdKecamatan) {
        this.pdKecamatan = pdKecamatan;
    }

    public String getPdKota() {
        return pdKota;
    }

    public void setPdKota(String pdKota) {
        this.pdKota = pdKota;
    }

    public String getPdTeleponRumah() {
        return pdTeleponRumah;
    }

    public void setPdTeleponRumah(String pdTeleponRumah) {
        this.pdTeleponRumah = pdTeleponRumah;
    }

    public String getPdNomorHandphoneSatu() {
        return pdNomorHandphoneSatu;
    }

    public void setPdNomorHandphoneSatu(String pdNomorHandphoneSatu) {
        this.pdNomorHandphoneSatu = pdNomorHandphoneSatu;
    }

    public String getPdNomorHandphoneDua() {
        return pdNomorHandphoneDua;
    }

    public void setPdNomorHandphoneDua(String pdNomorHandphoneDua) {
        this.pdNomorHandphoneDua = pdNomorHandphoneDua;
    }

    public String getPdKodePos() {
        return pdKodePos;
    }

    public void setPdKodePos(String pdKodePos) {
        this.pdKodePos = pdKodePos;
    }

    public String getPdEmail() {
        return pdEmail;
    }

    public void setPdEmail(String pdEmail) {
        this.pdEmail = pdEmail;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPdAlamat() {
        return pdAlamat;
    }

    public void setPdAlamat(String pdAlamat) {
        this.pdAlamat = pdAlamat;
    }
}
