package com.appschef.recruitment.model.response.parentresponse.profile;

import com.appschef.recruitment.model.response.childresponse.profile.WorkExperienceGet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WorkExperienceParent {

    @SerializedName("workexperience")
    @Expose
    public List<WorkExperienceGet> workExperienceGetList;

    public List<WorkExperienceGet> getWorkExperienceGetList() {
        return workExperienceGetList;
    }

    public void setWorkExperienceGetList(List<WorkExperienceGet> workExperienceGetList) {
        this.workExperienceGetList = workExperienceGetList;
    }
}
