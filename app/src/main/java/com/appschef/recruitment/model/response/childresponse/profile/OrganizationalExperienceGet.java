package com.appschef.recruitment.model.response.childresponse.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrganizationalExperienceGet {


    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("oe_bulan_mulai")
    @Expose
    public String oeBulanMulai;
    @SerializedName("oe_tahun_mulai")
    @Expose
    public String oeTahunMulai;
    @SerializedName("oe_bulan_selesai")
    @Expose
    public String oeBulanSelesai;
    @SerializedName("oe_tahun_selesai")
    @Expose
    public String oeTahunSelesai;
    @SerializedName("oe_nama_organisasi")
    @Expose
    public String oeNamaOrganisasi;
    @SerializedName("oe_posisi_pekerjaan")
    @Expose
    public String oePosisiPekerjaan;
    @SerializedName("oe_uraian_Tugas")
    @Expose
    public String oeUraianTugas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOeBulanMulai() {
        return oeBulanMulai;
    }

    public void setOeBulanMulai(String oeBulanMulai) {
        this.oeBulanMulai = oeBulanMulai;
    }

    public String getOeTahunMulai() {
        return oeTahunMulai;
    }

    public void setOeTahunMulai(String oeTahunMulai) {
        this.oeTahunMulai = oeTahunMulai;
    }

    public String getOeBulanSelesai() {
        return oeBulanSelesai;
    }

    public void setOeBulanSelesai(String oeBulanSelesai) {
        this.oeBulanSelesai = oeBulanSelesai;
    }

    public String getOeTahunSelesai() {
        return oeTahunSelesai;
    }

    public void setOeTahunSelesai(String oeTahunSelesai) {
        this.oeTahunSelesai = oeTahunSelesai;
    }

    public String getOeNamaOrganisasi() {
        return oeNamaOrganisasi;
    }

    public void setOeNamaOrganisasi(String oeNamaOrganisasi) {
        this.oeNamaOrganisasi = oeNamaOrganisasi;
    }

    public String getOePosisiPekerjaan() {
        return oePosisiPekerjaan;
    }

    public void setOePosisiPekerjaan(String oePosisiPekerjaan) {
        this.oePosisiPekerjaan = oePosisiPekerjaan;
    }

    public String getOeUraianTugas() {
        return oeUraianTugas;
    }

    public void setOeUraianTugas(String oeUraianTugas) {
        this.oeUraianTugas = oeUraianTugas;
    }
}
