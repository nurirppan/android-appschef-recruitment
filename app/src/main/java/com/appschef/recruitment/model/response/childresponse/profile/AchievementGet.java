package com.appschef.recruitment.model.response.childresponse.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AchievementGet {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("ach_nama_prestasi")
    @Expose
    public String achNamaPrestasi;
    @SerializedName("ach_diselenggarakan_oleh")
    @Expose
    public String achDiselenggarakanOleh;
    @SerializedName("ach_diterima_tahun")
    @Expose
    public String achDiterimaTahun;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAchNamaPrestasi() {
        return achNamaPrestasi;
    }

    public void setAchNamaPrestasi(String achNamaPrestasi) {
        this.achNamaPrestasi = achNamaPrestasi;
    }

    public String getAchDiselenggarakanOleh() {
        return achDiselenggarakanOleh;
    }

    public void setAchDiselenggarakanOleh(String achDiselenggarakanOleh) {
        this.achDiselenggarakanOleh = achDiselenggarakanOleh;
    }

    public String getAchDiterimaTahun() {
        return achDiterimaTahun;
    }

    public void setAchDiterimaTahun(String achDiterimaTahun) {
        this.achDiterimaTahun = achDiterimaTahun;
    }
}
