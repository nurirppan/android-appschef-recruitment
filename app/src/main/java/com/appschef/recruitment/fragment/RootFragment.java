package com.appschef.recruitment.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appschef.recruitment.R;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RootFragment extends Fragment {

    SharedPreferences sharedPreferences;
    ApiService mApiService;
    private String token;
    private String tokenFcm;

    public RootFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_root, container, false);
        getActivity().setTitle("Appschef");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");
        tokenFcm = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token_firebase), "");

        if (!tokenFcm.isEmpty() && tokenFcm != null) {
            mApiService.postFCM(token, tokenFcm).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()) {
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                }
            });
        }
//            Intent intentStart = new Intent(getContext(), DeleteTokenService.class);
//            getContext().startService(intentStart);
//            startActivity(new Intent(getContext(), HomeActivity.class));
//            getActivity().finish();
//        } else {
    }
//    }
}
