package com.appschef.recruitment.fragment.profile;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appschef.recruitment.R;
import com.appschef.recruitment.activity.CropPhotoActivity;
import com.appschef.recruitment.adapter.AchievementAdapter;
import com.appschef.recruitment.adapter.EducationalBackgroundAdapter;
import com.appschef.recruitment.adapter.OrganizationExperienceAdapter;
import com.appschef.recruitment.adapter.ProfessionalQualificationAdapter;
import com.appschef.recruitment.adapter.SkillProgramAdapter;
import com.appschef.recruitment.adapter.WorkExperienceAdapter;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.model.bus.CropPhotoEvent;
import com.appschef.recruitment.model.bus.RefreshProfile;
import com.appschef.recruitment.model.response.childresponse.profile.AchievementGet;
import com.appschef.recruitment.model.response.childresponse.profile.EducationalBackgroundGet;
import com.appschef.recruitment.model.response.childresponse.profile.OrganizationalExperienceGet;
import com.appschef.recruitment.model.response.childresponse.profile.ProfessionalQualificationGet;
import com.appschef.recruitment.model.response.childresponse.profile.SkillProgramGet;
import com.appschef.recruitment.model.response.childresponse.profile.WorkExperienceGet;
import com.appschef.recruitment.model.response.parentresponse.profile.AchievementParent;
import com.appschef.recruitment.model.response.parentresponse.profile.EducationalBackgroundParent;
import com.appschef.recruitment.model.response.parentresponse.profile.OrganizationalExperienceParent;
import com.appschef.recruitment.model.response.parentresponse.profile.PersonalDataParent;
import com.appschef.recruitment.model.response.parentresponse.profile.ProfessionalQualificationParent;
import com.appschef.recruitment.model.response.parentresponse.profile.SkillProgramParent;
import com.appschef.recruitment.model.response.parentresponse.profile.WorkExperienceParent;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProfileFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.iv_photo_profile)
    CircleImageView ivPhotoProfile;
    @BindView(R.id.btn_update_personal_data)
    Button btnUpdatePersonalData;
    @BindView(R.id.pb_list_pd)
    ProgressBar pbListPd;
    @BindView(R.id.tv_detail_ktp)
    TextView tvDetailKtp;
    @BindView(R.id.tv_detail_fullname)
    TextView tvDetailFullname;
    @BindView(R.id.tv_detail_gender)
    TextView tvDetailGender;
    @BindView(R.id.tv_detail_date_of_birth)
    TextView tvDetailDateOfBirth;
    @BindView(R.id.tv_detail_religion)
    TextView tvDetailReligion;
    @BindView(R.id.tv_detail_citizenship)
    TextView tvDetailCitizenship;
    @BindView(R.id.tv_detail_marital_status)
    TextView tvDetailMaritalStatus;
    @BindView(R.id.tv_detail_address)
    TextView tvDetailAddress;
    @BindView(R.id.tv_detail_city)
    TextView tvDetailCity;
    @BindView(R.id.tv_detail_postal_code)
    TextView tvDetailPostalCode;
    @BindView(R.id.tv_detail_home_phone)
    TextView tvDetailHomePhone;
    @BindView(R.id.tv_detail_handphone_one)
    TextView tvDetailHandphoneOne;
    @BindView(R.id.tv_detail_handphone_two)
    TextView tvDetailHandphoneTwo;
    @BindView(R.id.tv_detail_email)
    TextView tvDetailEmail;
    @BindView(R.id.btn_update_eb)
    Button btnUpdateEb;
    @BindView(R.id.pb_list_eb)
    ProgressBar pbListEb;
    @BindView(R.id.rv_eb)
    RecyclerView rvEb;
    @BindView(R.id.btn_update_we)
    Button btnUpdateWe;
    @BindView(R.id.pb_list_we)
    ProgressBar pbListWe;
    @BindView(R.id.rv_we)
    RecyclerView rvWe;
    @BindView(R.id.ll_content_detail_work_experience)
    LinearLayout llContentDetailWorkExperience;
    @BindView(R.id.btn_update_oe)
    Button btnUpdateOe;
    @BindView(R.id.pb_list_oe)
    ProgressBar pbListOe;
    @BindView(R.id.rv_oe)
    RecyclerView rvOe;
    @BindView(R.id.btn_update_ach)
    Button btnUpdateAch;
    @BindView(R.id.pb_list_ach)
    ProgressBar pbListAch;
    @BindView(R.id.rv_ach)
    RecyclerView rvAch;
    @BindView(R.id.ll_content_detail_achievement)
    LinearLayout llContentDetailAchievement;
    @BindView(R.id.btn_update_pq)
    Button btnUpdatePq;
    @BindView(R.id.pb_list_pq)
    ProgressBar pbListPq;
    @BindView(R.id.rv_pq)
    RecyclerView rvPq;
    @BindView(R.id.btn_update_sp)
    Button btnUpdateSp;
    @BindView(R.id.pb_list_sp)
    ProgressBar pbListSp;
    @BindView(R.id.rv_sp)
    RecyclerView rvSp;
    @BindView(R.id.ll_detail_pd)
    LinearLayout llDetailPd;
    private Intent intent;
    private ProgressDialog progressDialog;
    private Uri fileUri;
    private Bitmap bitmap, decoded;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    private int bitmap_size = 40; // image quality 1 - 100;
    private int max_resolution_image = 800;

    private List<EducationalBackgroundGet> backgroundGetList = new ArrayList<>();
    private EducationalBackgroundAdapter backgroundAdapter;
    private List<WorkExperienceGet> workExperienceGetList = new ArrayList<>();
    private WorkExperienceAdapter workExperienceAdapter;
    private List<OrganizationalExperienceGet> organizationalExperienceGetList = new ArrayList<>();
    private OrganizationExperienceAdapter organizationExperienceAdapter;
    private List<AchievementGet> achievementGetList = new ArrayList<>();
    private AchievementAdapter achievementAdapter;
    private List<ProfessionalQualificationGet> professionalQualificationGetList = new ArrayList<>();
    private ProfessionalQualificationAdapter professionalQualificationAdapter;
    private List<SkillProgramGet> skillProgramGetList = new ArrayList<>();
    private SkillProgramAdapter skillProgramAdapter;

    private RecyclerView.LayoutManager mLlRvEb, mLlRvWe, mLlRvOe, mLlRvAch, mLlRvPq, mLlRvSp;

    SharedPreferences sharedPreferences;
    ApiService mApiService;
    private String token;
    private String avatar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        getActivity().setTitle("Profile");
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");
        avatar = sharedPreferences.getString(getResources().getString(R.string.sharedpref_avatar), "");
        Glide.with(this).load(avatar).into(ivPhotoProfile);
        Log.i("token_home", token);

        valuePersonalDataGet();
        valueEducationalBackgroundGet();
        valueWorkExperienceGet();
        valueOrganizationalExperienceGet();
        valueAchievementGet();
        valueProfessionalQualificationGet();
        valueSkillProgramGet();

        mLlRvEb = new LinearLayoutManager(getContext());
        rvEb.setLayoutManager(mLlRvEb);
        rvEb.setItemAnimator(new DefaultItemAnimator());

        mLlRvSp = new LinearLayoutManager(getContext());
        rvSp.setLayoutManager(mLlRvSp);
        rvSp.setItemAnimator(new DefaultItemAnimator());

        mLlRvPq = new LinearLayoutManager(getContext());
        rvPq.setLayoutManager(mLlRvPq);
        rvPq.setItemAnimator(new DefaultItemAnimator());

        mLlRvAch = new LinearLayoutManager(getContext());
        rvAch.setLayoutManager(mLlRvAch);
        rvAch.setItemAnimator(new DefaultItemAnimator());

        mLlRvOe = new LinearLayoutManager(getContext());
        rvOe.setLayoutManager(mLlRvOe);
        rvOe.setItemAnimator(new DefaultItemAnimator());

        mLlRvWe = new LinearLayoutManager(getContext());
        rvWe.setLayoutManager(mLlRvWe);
        rvWe.setItemAnimator(new DefaultItemAnimator());
    }

    private void valueSkillProgramGet() {
        rvSp.setVisibility(View.GONE);
        pbListSp.setVisibility(View.VISIBLE);
        mApiService.skillProgramGet(token).enqueue(new Callback<BaseResponse<SkillProgramParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<SkillProgramParent>> call, Response<BaseResponse<SkillProgramParent>> response) {
                if (response.isSuccessful()) {
                    skillProgramGetList = response.body().getData().getSkillProgramGetList();
                    skillProgramAdapter = new SkillProgramAdapter(getContext(), skillProgramGetList, mApiService, token, DetailProfileFragment.this);
                    rvSp.setAdapter(skillProgramAdapter);
                    pbListSp.setVisibility(View.GONE);
                    rvSp.setVisibility(View.VISIBLE);
                } else {
                    pbListSp.setVisibility(View.GONE);
                    rvSp.setVisibility(View.VISIBLE);
                    Util.showToast(getContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<SkillProgramParent>> call, Throwable t) {
                pbListSp.setVisibility(View.GONE);
                rvSp.setVisibility(View.VISIBLE);
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    private void valueProfessionalQualificationGet() {
        rvPq.setVisibility(View.GONE);
        pbListPq.setVisibility(View.VISIBLE);
        mApiService.professionalQualificationGet(token).enqueue(new Callback<BaseResponse<ProfessionalQualificationParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<ProfessionalQualificationParent>> call, Response<BaseResponse<ProfessionalQualificationParent>> response) {
                if (response.isSuccessful()) {
                    professionalQualificationGetList = response.body().getData().getProfessionalQualificationGetList();
                    professionalQualificationAdapter = new ProfessionalQualificationAdapter(getContext(), professionalQualificationGetList, mApiService, token, DetailProfileFragment.this);
                    rvPq.setAdapter(professionalQualificationAdapter);
                    pbListPq.setVisibility(View.GONE);
                    rvPq.setVisibility(View.VISIBLE);
                } else {
                    pbListPq.setVisibility(View.GONE);
                    rvPq.setVisibility(View.VISIBLE);
                    Util.showToast(getContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<ProfessionalQualificationParent>> call, Throwable t) {
                pbListPq.setVisibility(View.GONE);
                rvPq.setVisibility(View.VISIBLE);
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    private void valueAchievementGet() {
        rvAch.setVisibility(View.GONE);
        pbListAch.setVisibility(View.VISIBLE);
        mApiService.achievementGet(token).enqueue(new Callback<BaseResponse<AchievementParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<AchievementParent>> call, Response<BaseResponse<AchievementParent>> response) {
                if (response.isSuccessful()) {
                    achievementGetList = response.body().getData().getAchievementGetList();
                    achievementAdapter = new AchievementAdapter(getContext(), achievementGetList, mApiService, token, DetailProfileFragment.this);
                    rvAch.setAdapter(achievementAdapter);
                    pbListAch.setVisibility(View.GONE);
                    rvAch.setVisibility(View.VISIBLE);
                } else {
                    pbListAch.setVisibility(View.GONE);
                    rvAch.setVisibility(View.VISIBLE);
                    Util.showToast(getContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<AchievementParent>> call, Throwable t) {
                pbListAch.setVisibility(View.GONE);
                rvAch.setVisibility(View.VISIBLE);
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    private void valueOrganizationalExperienceGet() {
        rvOe.setVisibility(View.GONE);
        pbListOe.setVisibility(View.VISIBLE);
        mApiService.organizationalExperienceGet(token).enqueue(new Callback<BaseResponse<OrganizationalExperienceParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<OrganizationalExperienceParent>> call, Response<BaseResponse<OrganizationalExperienceParent>> response) {
                if (response.isSuccessful()) {
                    organizationalExperienceGetList = response.body().getData().getOrganizationalExperienceGetList();
                    organizationExperienceAdapter = new OrganizationExperienceAdapter(getContext(), organizationalExperienceGetList, mApiService, token, DetailProfileFragment.this);
                    rvOe.setAdapter(organizationExperienceAdapter);
                    pbListOe.setVisibility(View.GONE);
                    rvOe.setVisibility(View.VISIBLE);
                } else {
                    pbListOe.setVisibility(View.GONE);
                    rvOe.setVisibility(View.VISIBLE);
                    Util.showToast(getContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<OrganizationalExperienceParent>> call, Throwable t) {
                pbListOe.setVisibility(View.GONE);
                rvOe.setVisibility(View.VISIBLE);
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    private void valueWorkExperienceGet() {
        rvWe.setVisibility(View.GONE);
        pbListWe.setVisibility(View.VISIBLE);
        mApiService.workExperienceGet(token).enqueue(new Callback<BaseResponse<WorkExperienceParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<WorkExperienceParent>> call, Response<BaseResponse<WorkExperienceParent>> response) {
                if (response.isSuccessful()) {
                    workExperienceGetList = response.body().getData().getWorkExperienceGetList();
                    workExperienceAdapter = new WorkExperienceAdapter(getContext(), workExperienceGetList, mApiService, token, DetailProfileFragment.this);
                    rvWe.setAdapter(workExperienceAdapter);
                    pbListWe.setVisibility(View.GONE);
                    rvWe.setVisibility(View.VISIBLE);
                } else {
                    pbListWe.setVisibility(View.GONE);
                    rvWe.setVisibility(View.VISIBLE);
                    Util.showToast(getContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<WorkExperienceParent>> call, Throwable t) {
                pbListWe.setVisibility(View.GONE);
                rvWe.setVisibility(View.VISIBLE);
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    private void valueEducationalBackgroundGet() {
        rvEb.setVisibility(View.GONE);
        pbListEb.setVisibility(View.VISIBLE);
        mApiService.educationalBackgroundGet(token).enqueue(new Callback<BaseResponse<EducationalBackgroundParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<EducationalBackgroundParent>> call, Response<BaseResponse<EducationalBackgroundParent>> response) {
                if (response.isSuccessful()) {
                    backgroundGetList = response.body().getData().getEducationalBackgroundGetList();
                    backgroundAdapter = new EducationalBackgroundAdapter(getContext(), backgroundGetList, mApiService, token, DetailProfileFragment.this);
                    rvEb.setAdapter(backgroundAdapter);
                    pbListEb.setVisibility(View.GONE);
                    rvEb.setVisibility(View.VISIBLE);
                } else {
                    pbListEb.setVisibility(View.GONE);
                    rvEb.setVisibility(View.VISIBLE);
                    Util.showToast(getContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<EducationalBackgroundParent>> call, Throwable t) {
                pbListEb.setVisibility(View.GONE);
                rvEb.setVisibility(View.VISIBLE);
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    private void valuePersonalDataGet() {
        llDetailPd.setVisibility(View.GONE);
        pbListPd.setVisibility(View.VISIBLE);
        mApiService.personalDataGet(token).enqueue(new Callback<BaseResponse<PersonalDataParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<PersonalDataParent>> call, Response<BaseResponse<PersonalDataParent>> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) setDetailProfile(response);
                    pbListPd.setVisibility(View.GONE);
                    rvPq.setVisibility(View.VISIBLE);
                    llDetailPd.setVisibility(View.VISIBLE);
                } else {
                    pbListPd.setVisibility(View.GONE);
                    llDetailPd.setVisibility(View.VISIBLE);
                    Util.showToast(getContext(), response.message());
                }
            }

            private void setDetailProfile(Response<BaseResponse<PersonalDataParent>> response) {
                String strAlamat = (response.body().getData().getPersonalDataGets().getPdAlamat() == null) ? "" : response.body().getData().getPersonalDataGets().getPdAlamat();
                String strTempatLahir = (response.body().getData().getPersonalDataGets().getPdTempatLahir() == null) ? "" : response.body().getData().getPersonalDataGets().getPdTempatLahir();
                String strTanggalLahir = (response.body().getData().getPersonalDataGets().getPdTanggalLahir() == null) ? "" : response.body().getData().getPersonalDataGets().getPdTanggalLahir();
                String strRt = (response.body().getData().getPersonalDataGets().getPdRt() == null) ? "" : response.body().getData().getPersonalDataGets().getPdRt();
                String strRw = (response.body().getData().getPersonalDataGets().getPdRw() == null) ? "" : response.body().getData().getPersonalDataGets().getPdRw();
                String strKecamatan = (response.body().getData().getPersonalDataGets().getPdKecamatan() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKecamatan();
                String strKelurahan = (response.body().getData().getPersonalDataGets().getPdKelurahan() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKelurahan();

                tvDetailKtp.setText((response.body().getData().getPersonalDataGets().getPdKtp() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKtp());
                tvDetailFullname.setText((response.body().getData().getPersonalDataGets().getPdNamaLengkap() == null) ? "" : response.body().getData().getPersonalDataGets().getPdNamaLengkap());
                tvDetailGender.setText((response.body().getData().getPersonalDataGets().getPdJenisKelamin() == null) ? "" : response.body().getData().getPersonalDataGets().getPdJenisKelamin());
                tvDetailDateOfBirth.setText(strTempatLahir + ", " + strTanggalLahir);
                tvDetailCitizenship.setText((response.body().getData().getPersonalDataGets().getPdKewarganegaraan() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKewarganegaraan());
                tvDetailMaritalStatus.setText((response.body().getData().getPersonalDataGets().getPdStatusPernikahan() == null) ? "" : response.body().getData().getPersonalDataGets().getPdStatusPernikahan());
                tvDetailAddress.setText(strAlamat + " " + strRt + "/" + strRw + ", " + strKecamatan + ", " + strKelurahan);
                tvDetailCity.setText((response.body().getData().getPersonalDataGets().getPdKota() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKota());
                tvDetailPostalCode.setText((response.body().getData().getPersonalDataGets().getPdKodePos() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKodePos());
                tvDetailHomePhone.setText((response.body().getData().getPersonalDataGets().getPdTeleponRumah() == null) ? "" : response.body().getData().getPersonalDataGets().getPdTeleponRumah());
                tvDetailHandphoneOne.setText((response.body().getData().getPersonalDataGets().getPdNomorHandphoneSatu() == null) ? "" : response.body().getData().getPersonalDataGets().getPdNomorHandphoneSatu());
                tvDetailHandphoneTwo.setText((response.body().getData().getPersonalDataGets().getPdNomorHandphoneDua() == null) ? "" : response.body().getData().getPersonalDataGets().getPdNomorHandphoneDua());
                tvDetailEmail.setText((response.body().getData().getPersonalDataGets().getPdEmail() == null) ? "" : response.body().getData().getPersonalDataGets().getPdEmail());
            }

            @Override
            public void onFailure(Call<BaseResponse<PersonalDataParent>> call, Throwable t) {
                pbListPd.setVisibility(View.GONE);
                llDetailPd.setVisibility(View.VISIBLE);
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.iv_photo_profile)
    public void onClickTakeFoto() {
        final CharSequence[] items = {"Take Photo", "Chose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = getOutputMediaFileUri();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Chose from Library")) {
                    intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DeKa");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e("Monitoring", "Oops! Failed create Monitoring directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_DeKa_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("onActivityResult", "requestCode " + requestCode + ", resultCode " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Intent intent = new Intent(getContext(), CropPhotoActivity.class);
                intent.putExtra("requestCode", requestCode);
                intent.setData(fileUri);
                startActivity(intent);
            } else if (requestCode == SELECT_FILE && data != null && data.getData() != null) {
                Intent intent = new Intent(getContext(), CropPhotoActivity.class);
                intent.putExtra("requestCode", requestCode);
                intent.setData(data.getData());
                startActivity(intent);
            }
        }
    }

    // Untuk menampilkan bitmap pada ImageView
    private void setToImageView(Bitmap bmp) {
        //compress image
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
        ivPhotoProfile.setImageBitmap(decoded);
    }

    // Untuk resize bitmap
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @OnClick({R.id.btn_update_personal_data, R.id.btn_update_eb, R.id.btn_update_we, R.id.btn_update_oe, R.id.btn_update_ach, R.id.btn_update_pq, R.id.btn_update_sp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_update_personal_data:
                startActivity(new Intent(getContext(), PersonalDataActivity.class));
                break;
            case R.id.btn_update_eb:
                Intent a = new Intent(getContext(), EducationalBackgroundActivity.class);
                a.putExtra("page", "new");
                startActivity(a);
                break;
            case R.id.btn_update_we:
                Intent b = new Intent(getContext(), WorkExperienceActivity.class);
                b.putExtra("page", "new");
                startActivity(b);
                break;
            case R.id.btn_update_oe:
                Intent c = new Intent(getContext(), OrganizationExperienceActivity.class);
                c.putExtra("page", "new");
                startActivity(c);
                break;
            case R.id.btn_update_ach:
                Intent d = new Intent(getContext(), AchievementActivity.class);
                d.putExtra("page", "new");
                startActivity(d);
                break;
            case R.id.btn_update_pq:
                Intent e = new Intent(getContext(), ProfessionalQualificationActivity.class);
                e.putExtra("page", "new");
                startActivity(e);
                break;
            case R.id.btn_update_sp:
                Intent f = new Intent(getContext(), SkillProgramActivity.class);
                f.putExtra("page", "new");
                startActivity(f);
                break;
        }
    }

    public void delDataProfile(final String value, final Integer id, final String token) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_confirmation);
        TextView tvContentConfirmations = (TextView) dialog.findViewById(R.id.tv_content_confirmations);
        Button btnNo = (Button) dialog.findViewById(R.id.btn_no);
        Button btnYes = (Button) dialog.findViewById(R.id.btn_yes);
        tvContentConfirmations.setText(getString(R.string.txt_are_you_sure_want_to) + " delete this data ?");
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (value.equals("EB")) {
                    pbListEb.setVisibility(View.VISIBLE);
                    rvEb.setVisibility(View.GONE);
                    mApiService.educationalBackgroundDelete(id, token).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                rvEb.setVisibility(View.GONE);
                                valueEducationalBackgroundGet();
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                pbListEb.setVisibility(View.GONE);
                                rvEb.setVisibility(View.VISIBLE);
                            }
                            Log.i("Log__EB", response.message());
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                            pbListEb.setVisibility(View.GONE);
                            rvEb.setVisibility(View.VISIBLE);
                        }
                    });
                } else if (value.equals("WE")) {
                    pbListWe.setVisibility(View.VISIBLE);
                    rvWe.setVisibility(View.GONE);
                    mApiService.workExperienceDelete(id, token).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                rvWe.setVisibility(View.GONE);
                                valueWorkExperienceGet();
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                pbListWe.setVisibility(View.GONE);
                                rvWe.setVisibility(View.VISIBLE);
                            }
                            Log.i("Log__WE", response.message());
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                            pbListWe.setVisibility(View.GONE);
                            rvWe.setVisibility(View.VISIBLE);
                        }
                    });
                } else if (value.equals("OE")) {
                    pbListOe.setVisibility(View.VISIBLE);
                    rvOe.setVisibility(View.GONE);
                    mApiService.organizationExperienceDelete(id, token).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                rvOe.setVisibility(View.GONE);
                                valueOrganizationalExperienceGet();
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                pbListOe.setVisibility(View.GONE);
                                rvOe.setVisibility(View.VISIBLE);
                            }
                            Log.i("Log__OE", response.message());
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                            pbListOe.setVisibility(View.GONE);
                            rvOe.setVisibility(View.VISIBLE);
                        }
                    });
                } else if (value.equals("ACH")) {
                    pbListAch.setVisibility(View.VISIBLE);
                    rvAch.setVisibility(View.GONE);
                    mApiService.achievementDelete(id, token).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                rvAch.setVisibility(View.GONE);
                                valueAchievementGet();
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                pbListAch.setVisibility(View.GONE);
                                rvAch.setVisibility(View.VISIBLE);
                            }
                            Log.i("Log__ACH", response.message());
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                            pbListAch.setVisibility(View.GONE);
                            rvAch.setVisibility(View.VISIBLE);
                        }
                    });
                } else if (value.equals("PQ")) {
                    pbListPq.setVisibility(View.VISIBLE);
                    rvPq.setVisibility(View.GONE);
                    mApiService.professionalQualificationDelete(id, token).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                rvPq.setVisibility(View.GONE);
                                valueProfessionalQualificationGet();
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                pbListPq.setVisibility(View.GONE);
                                rvPq.setVisibility(View.VISIBLE);
                            }
                            Log.i("Log__PQ", response.message());
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                            pbListPq.setVisibility(View.GONE);
                            rvPq.setVisibility(View.VISIBLE);
                        }
                    });
                } else if (value.equals("SP")) {
                    pbListSp.setVisibility(View.VISIBLE);
                    rvSp.setVisibility(View.GONE);
                    mApiService.skillsProgramDelete(id, token).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                rvSp.setVisibility(View.GONE);
                                valueSkillProgramGet();
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                pbListSp.setVisibility(View.GONE);
                                rvSp.setVisibility(View.VISIBLE);
                            }
                            Log.i("Log__SP", response.message());
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                            pbListSp.setVisibility(View.GONE);
                            rvSp.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });
        dialog.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshProfile(RefreshProfile event) {
        valuePersonalDataGet();
        valueEducationalBackgroundGet();
        valueWorkExperienceGet();
        valueOrganizationalExperienceGet();
        valueAchievementGet();
        valueProfessionalQualificationGet();
        valueSkillProgramGet();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setCropPhotoEvent(CropPhotoEvent e) {
        Uri tempUri = Util.getImageUri(getContext(), e.getBitmap());
        File fileUri = new File(Util.getRealPathFromURI(getContext(), tempUri));
        long fileLength = fileUri.length() / 1024;
        Util.checkSizeImage(tempUri);
        Log.i("size_img_1", String.valueOf(fileLength));
        Glide.with(getContext()).load(fileUri).into(ivPhotoProfile);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
