package com.appschef.recruitment.fragment.profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.bus.RefreshProfile;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrganizationExperienceActivity extends AppCompatActivity {

    @BindView(R.id.et_oe_month_start)
    TextInputEditText etOeMonthStart;
    @BindView(R.id.til_oe_month_start)
    TextInputLayout tilOeMonthStart;
    @BindView(R.id.btn_rmv_oe_month_start)
    ImageView btnRmvOeMonthStart;
    @BindView(R.id.et_oe_year_start)
    TextInputEditText etOeYearStart;
    @BindView(R.id.til_oe_year_start)
    TextInputLayout tilOeYearStart;
    @BindView(R.id.btn_rmv_oe_year_start)
    ImageView btnRmvOeYearStart;
    @BindView(R.id.et_oe_month_finish)
    TextInputEditText etOeMonthFinish;
    @BindView(R.id.til_oe_month_finish)
    TextInputLayout tilOeMonthFinish;
    @BindView(R.id.btn_rmv_oe_month_finish)
    ImageView btnRmvOeMonthFinish;
    @BindView(R.id.et_oe_year_finish)
    TextInputEditText etOeYearFinish;
    @BindView(R.id.til_oe_year_finish)
    TextInputLayout tilOeYearFinish;
    @BindView(R.id.btn_rmv_oe_year_finish)
    ImageView btnRmvOeYearFinish;
    @BindView(R.id.et_oe_organization_name)
    TextInputLayout etOeOrganizationName;
    @BindView(R.id.btn_rmv_oe_organization_name)
    ImageView btnRmvOeOrganizationName;
    @BindView(R.id.et_oe_job_position)
    TextInputLayout etOeJobPosition;
    @BindView(R.id.btn_rmv_oe_job_position)
    ImageView btnRmvOeJobPosition;
    @BindView(R.id.et_oe_job_description)
    TextInputLayout etOeJobDescription;
    @BindView(R.id.btn_rmv_oe_job_description)
    ImageView btnRmvOeJobDescription;
    @BindView(R.id.btn_cancel_oe)
    Button btnCancelOe;
    @BindView(R.id.btn_save_oe)
    Button btnSaveOe;
    private ProgressDialog progressDialog;
    ApiService mApiService;
    SharedPreferences sharedPreferences;
    private String token;

    private String strOeMonthStart;
    private String strOeYearStart;
    private String strOeMonthFinish;
    private String strOeYearFinish;
    private String strOeOrganizationName;
    private String strOeJobPosition;
    private String strOeJobDescription;
    private Intent intent;
    private String page;
    private String initId;
    private String oe_bulan_mulai;
    private String oe_tahun_mulai;
    private String oe_bulan_selesai;
    private String oe_tahun_selesai;
    private String oe_nama_organisasi;
    private String oe_posisi_pekerjaan;
    private String oe_uraian_Tugas;

    private Dialog dialog;
    private DateFormat dateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_organization_experience);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();

        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        dateFormat = new SimpleDateFormat("yyyy");
        dialog = new Dialog(OrganizationExperienceActivity.this);

        intent = getIntent();
        page = intent.getStringExtra("page");

        if (page.equals("update")) {
            initId = intent.getStringExtra("id");
            oe_bulan_mulai = intent.getStringExtra("oe_bulan_mulai");
            oe_tahun_mulai = intent.getStringExtra("oe_tahun_mulai");
            oe_bulan_selesai = intent.getStringExtra("oe_bulan_selesai");
            oe_tahun_selesai = intent.getStringExtra("oe_tahun_selesai");
            oe_nama_organisasi = intent.getStringExtra("oe_nama_organisasi");
            oe_posisi_pekerjaan = intent.getStringExtra("oe_posisi_pekerjaan");
            oe_uraian_Tugas = intent.getStringExtra("oe_uraian_Tugas");

            etOeMonthStart.setText(oe_bulan_mulai);
            etOeYearStart.setText(oe_tahun_mulai);
            etOeMonthFinish.setText(oe_bulan_selesai);
            etOeYearFinish.setText(oe_tahun_selesai);
            etOeOrganizationName.getEditText().setText(oe_nama_organisasi);
            etOeJobPosition.getEditText().setText(oe_posisi_pekerjaan);
            etOeJobDescription.getEditText().setText(oe_uraian_Tugas);
        }
    }

    private boolean checkWorkExperience() {
        valueOrganizationExperience();
        if (strOeMonthStart.isEmpty()) tilOeMonthStart.setError("Wajib Di Isi");
        else tilOeMonthStart.setError(null);
        if (strOeYearStart.isEmpty()) tilOeYearStart.setError("Wajib Di Isi");
        else tilOeYearStart.setError(null);
        if (strOeMonthFinish.isEmpty()) tilOeMonthFinish.setError("Wajib Di Isi");
        else tilOeMonthFinish.setError(null);
        if (strOeYearFinish.isEmpty()) tilOeYearFinish.setError("Wajib Di Isi");
        else tilOeYearFinish.setError(null);
        if (strOeOrganizationName.isEmpty()) etOeOrganizationName.setError("Wajib Di Isi");
        else etOeOrganizationName.setError(null);
        if (strOeJobPosition.isEmpty()) etOeJobPosition.setError("Wajib Di Isi");
        else etOeJobPosition.setError(null);
        if (strOeJobDescription.isEmpty()) etOeJobDescription.setError("Wajib Di Isi");
        else etOeJobDescription.setError(null);

        return !strOeMonthStart.isEmpty() && !strOeYearStart.isEmpty() && !strOeMonthFinish.isEmpty() && !strOeYearFinish.isEmpty() && !strOeOrganizationName.isEmpty() && !strOeJobPosition.isEmpty() && !strOeJobDescription.isEmpty();
    }

    private void valueOrganizationExperience() {
        strOeMonthStart = etOeMonthStart.getText().toString().trim();
        strOeYearStart = etOeYearStart.getText().toString().trim();
        strOeMonthFinish = etOeMonthFinish.getText().toString().trim();
        strOeYearFinish = etOeYearFinish.getText().toString().trim();
        strOeOrganizationName = etOeOrganizationName.getEditText().getText().toString().trim();
        strOeJobPosition = etOeJobPosition.getEditText().getText().toString().trim();
        strOeJobDescription = etOeJobDescription.getEditText().getText().toString().trim();
    }

    @OnClick({R.id.btn_rmv_oe_month_start, R.id.btn_rmv_oe_year_start, R.id.btn_rmv_oe_month_finish, R.id.btn_rmv_oe_year_finish, R.id.btn_rmv_oe_organization_name, R.id.btn_rmv_oe_job_position, R.id.btn_rmv_oe_job_description})
    public void onClickRemoveOE(View view) {
        switch (view.getId()) {
            case R.id.btn_rmv_oe_month_start:
                etOeMonthStart.getText().clear();
                break;
            case R.id.btn_rmv_oe_year_start:
                etOeYearStart.getText().clear();
                break;
            case R.id.btn_rmv_oe_month_finish:
                etOeMonthFinish.getText().clear();
                break;
            case R.id.btn_rmv_oe_year_finish:
                etOeYearFinish.getText().clear();
                break;
            case R.id.btn_rmv_oe_organization_name:
                etOeOrganizationName.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_oe_job_position:
                etOeJobPosition.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_oe_job_description:
                etOeJobDescription.getEditText().getText().clear();
                break;
        }
    }

    @OnClick({R.id.btn_cancel_oe, R.id.btn_save_oe})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel_oe:
                finish();
                break;
            case R.id.btn_save_oe:
                if (checkWorkExperience()) {
                    progressDialog.show();
                    if (page.equals("new")) {
                        mApiService.organizationExperiencePost(token, strOeMonthStart, strOeYearStart, strOeMonthFinish, strOeYearFinish, strOeOrganizationName, strOeJobPosition, strOeJobDescription).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etOeMonthStart.getText().clear();
                                    etOeYearStart.getText().clear();
                                    etOeMonthFinish.getText().clear();
                                    etOeYearFinish.getText().clear();
                                    etOeOrganizationName.getEditText().getText().clear();
                                    etOeJobPosition.getEditText().getText().clear();
                                    etOeJobDescription.getEditText().getText().clear();
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                    Util.showToast(OrganizationExperienceActivity.this, response.message());
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(OrganizationExperienceActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(OrganizationExperienceActivity.this, "onFailure");
                            }
                        });
                    } else {
                        mApiService.organizationExperienceUpdate(token, Integer.valueOf(initId), strOeMonthStart, strOeYearStart, strOeMonthFinish, strOeYearFinish, strOeOrganizationName, strOeJobPosition, strOeJobDescription).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etOeMonthStart.getText().clear();
                                    etOeYearStart.getText().clear();
                                    etOeMonthFinish.getText().clear();
                                    etOeYearFinish.getText().clear();
                                    etOeOrganizationName.getEditText().getText().clear();
                                    etOeJobPosition.getEditText().getText().clear();
                                    etOeJobDescription.getEditText().getText().clear();
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                    Util.showToast(OrganizationExperienceActivity.this, response.message());
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(OrganizationExperienceActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(OrganizationExperienceActivity.this, "onFailure");
                            }
                        });
                    }
                }
                break;
        }
    }

    @OnClick({R.id.et_oe_month_start, R.id.et_oe_year_start, R.id.et_oe_month_finish, R.id.et_oe_year_finish})
    public void onClickDateOE(View view) {
        switch (view.getId()) {
            case R.id.et_oe_month_start:
                String monthStart = "oe_month_start";
                dialogMonth(monthStart);
                break;
            case R.id.et_oe_year_start:
                String yearStart = "oe_year_start";
                dialogYear(yearStart);
                break;
            case R.id.et_oe_month_finish:
                String monthFinish = "oe_month_finish";
                dialogMonth(monthFinish);
                break;
            case R.id.et_oe_year_finish:
                String yearFinish = "oe_year_finish";
                dialogYear(yearFinish);
                break;
        }
    }

    private void dialogMonth(final String valueMonth) {
        final CharSequence[] items = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember",};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pilih Bulan");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (valueMonth.equals("oe_month_start")) {
                    etOeMonthStart.setText(items[item]);
                } else {
                    etOeMonthFinish.setText(items[item]);
                }
            }
        });
        builder.show();
    }

    private void dialogYear(final String strYear) {
        String dateYearStart = dateFormat.format(Calendar.getInstance().getTime());
        if (strYear.equals("oe_year_start")) {
            dialog.setTitle("Tahun Mulai");
        } else {
            dialog.setTitle("Tahun Selesai");
        }
        dialog.setContentView(R.layout.utils_year_picker);
        Button btnCancelNp = (Button) dialog.findViewById(R.id.btn_cancel_np);
        Button btnSaveNp = (Button) dialog.findViewById(R.id.btn_save_np);
        final NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.np_year);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(9999);
        numberPicker.setValue(Integer.parseInt(dateYearStart));
        numberPicker.setWrapSelectorWheel(false);
        btnCancelNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSaveNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strYear.equals("oe_year_start")) {
                    etOeYearStart.setText(String.valueOf(numberPicker.getValue()));
                } else {
                    etOeYearFinish.setText(String.valueOf(numberPicker.getValue()));
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
