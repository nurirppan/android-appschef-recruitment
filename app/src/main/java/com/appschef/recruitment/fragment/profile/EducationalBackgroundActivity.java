package com.appschef.recruitment.fragment.profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.bus.RefreshProfile;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EducationalBackgroundActivity extends AppCompatActivity {

    @BindView(R.id.spn_educational)
    Spinner spnEducational;
    @BindView(R.id.et_eb_year_of_graduation)
    TextInputEditText etEbYearOfGraduation;
    @BindView(R.id.til_eb_year_of_graduation)
    TextInputLayout tilEbYearOfGraduation;
    @BindView(R.id.btn_rmv_eb_year_of_graduation)
    ImageView btnRmvEbYearOfGraduation;
    @BindView(R.id.et_eb_study_program)
    TextInputLayout etEbStudyProgram;
    @BindView(R.id.btn_rmv_eb_study_program)
    ImageView btnRmvEbStudyProgram;
    @BindView(R.id.et_eb_ipk)
    TextInputLayout etEbIpk;
    @BindView(R.id.btn_rmv_eb_ipk)
    ImageView btnRmvEbIpk;
    @BindView(R.id.btn_cancel_eb)
    Button btnCancelEb;
    @BindView(R.id.btn_save_eb)
    Button btnSaveEb;
    private ProgressDialog progressDialog;

    ApiService mApiService;
    SharedPreferences sharedPreferences;
    private String token;

    private String strLevelEducation;
    private String strYearOfGraduation;
    private String strStudyProgram;
    private String strIpk;
    private Intent intent;
    private String page;
    private String initId;
    private String tingkatPendidikan;
    private String programStudi;
    private String tahunLulus;
    private String ipk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_educational_background);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();

        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        intent = getIntent();
        page = intent.getStringExtra("page");
        if (page.equals("update")) {
            initId = intent.getStringExtra("id");
            tingkatPendidikan = intent.getStringExtra("tingkatPendidikan");
            programStudi = intent.getStringExtra("programStudi");
            tahunLulus = intent.getStringExtra("tahunLulus");
            ipk = intent.getStringExtra("ipk");

            if (tingkatPendidikan.equals("SD") || tingkatPendidikan.equals("SLTP / Equivalent") || tingkatPendidikan.equals("SLTA / Equivalent") || tingkatPendidikan.equals("D1") || tingkatPendidikan.equals("D2") || tingkatPendidikan.equals("D3") || tingkatPendidikan.equals("D4") || tingkatPendidikan.equals("S1") || tingkatPendidikan.equals("S2") || tingkatPendidikan.equals("S3")) {
                spnEducational.setSelection(((ArrayAdapter<String>) spnEducational.getAdapter()).getPosition(tingkatPendidikan));
            }
            etEbYearOfGraduation.setText(tahunLulus);
            etEbStudyProgram.getEditText().setText(programStudi);
            etEbIpk.getEditText().setText(ipk);
        }
    }

    private void valueEducationalBakcground() {
        strLevelEducation = spnEducational.getSelectedItem().toString();
        strYearOfGraduation = etEbYearOfGraduation.getText().toString().trim();
        strStudyProgram = etEbStudyProgram.getEditText().getText().toString().trim();
        strIpk = etEbIpk.getEditText().getText().toString().trim();
    }

    @OnClick({R.id.btn_cancel_eb, R.id.btn_save_eb})
    public void onClickEB(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel_eb:
                finish();
                break;
            case R.id.btn_save_eb:
                if (checkEducationalBakcground()) {
                    progressDialog.show();
                    if (page.equals("new")) {
                        mApiService.educationalBackgroundPost(token, strLevelEducation, strYearOfGraduation, strStudyProgram, strIpk).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etEbYearOfGraduation.getText().clear();
                                    etEbStudyProgram.getEditText().getText().clear();
                                    etEbIpk.getEditText().getText().clear();
                                    Util.showToast(EducationalBackgroundActivity.this, response.message());
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(EducationalBackgroundActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(EducationalBackgroundActivity.this, "onFailure");
                            }
                        });
                    } else if (page.equals("update")) {
                        mApiService.educationalBackgroundUpdate(token, Integer.valueOf(initId), strLevelEducation, strYearOfGraduation, strStudyProgram, strIpk).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etEbYearOfGraduation.getText().clear();
                                    etEbStudyProgram.getEditText().getText().clear();
                                    etEbIpk.getEditText().getText().clear();
                                    EventBus.getDefault().post(new RefreshProfile());
                                    Util.showToast(EducationalBackgroundActivity.this, response.message());
                                    progressDialog.dismiss();
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(EducationalBackgroundActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(EducationalBackgroundActivity.this, "onFailure");
                            }
                        });
                    }
                }
                break;
        }
    }

    private boolean checkEducationalBakcground() {
        valueEducationalBakcground();
        if (strYearOfGraduation.isEmpty()) tilEbYearOfGraduation.setError("Wajib Di Isi");
        else tilEbYearOfGraduation.setError(null);
        if (strStudyProgram.isEmpty()) etEbStudyProgram.setError("Wajib Di Isi");
        else etEbStudyProgram.setError(null);
        if (strIpk.isEmpty()) etEbIpk.setError("Wajib Di Isi");
        else etEbIpk.setError(null);

        return !strYearOfGraduation.isEmpty() && !strStudyProgram.isEmpty() && !strIpk.isEmpty();
    }

    @OnClick({R.id.btn_rmv_eb_year_of_graduation, R.id.btn_rmv_eb_study_program, R.id.btn_rmv_eb_ipk})
    public void onClickRemoveEB(View view) {
        switch (view.getId()) {
            case R.id.btn_rmv_eb_year_of_graduation:
                etEbYearOfGraduation.getText().clear();
                break;
            case R.id.btn_rmv_eb_study_program:
                etEbStudyProgram.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_eb_ipk:
                etEbIpk.getEditText().getText().clear();
                break;
        }
    }

    @OnClick(R.id.et_eb_year_of_graduation)
    public void onClickDateEB() {
        final Dialog dialog = new Dialog(EducationalBackgroundActivity.this);
        DateFormat df = new SimpleDateFormat("yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        dialog.setTitle("Year");
        dialog.setContentView(R.layout.utils_year_picker);
        Button btnCancelNp = (Button) dialog.findViewById(R.id.btn_cancel_np);
        Button btnSaveNp = (Button) dialog.findViewById(R.id.btn_save_np);
        final NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.np_year);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(9999);
        numberPicker.setValue(Integer.parseInt(date));
        numberPicker.setWrapSelectorWheel(false);
        btnCancelNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSaveNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etEbYearOfGraduation.setText(String.valueOf(numberPicker.getValue()));
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
