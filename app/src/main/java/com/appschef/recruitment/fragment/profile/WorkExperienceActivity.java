package com.appschef.recruitment.fragment.profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.bus.RefreshProfile;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkExperienceActivity extends AppCompatActivity {

    @BindView(R.id.et_we_month_start)
    TextInputEditText etWeMonthStart;
    @BindView(R.id.til_we_month_start)
    TextInputLayout tilWeMonthStart;
    @BindView(R.id.btn_rmv_we_month_start)
    ImageView btnRmvWeMonthStart;
    @BindView(R.id.et_we_year_start)
    TextInputEditText etWeYearStart;
    @BindView(R.id.til_we_year_start)
    TextInputLayout tilWeYearStart;
    @BindView(R.id.btn_rmv_we_year_start)
    ImageView btnRmvWeYearStart;
    @BindView(R.id.et_we_month_finish)
    TextInputEditText etWeMonthFinish;
    @BindView(R.id.til_we_month_finish)
    TextInputLayout tilWeMonthFinish;
    @BindView(R.id.btn_rmv_we_month_finish)
    ImageView btnRmvWeMonthFinish;
    @BindView(R.id.et_we_year_finish)
    TextInputEditText etWeYearFinish;
    @BindView(R.id.til_we_year_finish)
    TextInputLayout tilWeYearFinish;
    @BindView(R.id.btn_rmv_we_year_finish)
    ImageView btnRmvWeYearFinish;
    @BindView(R.id.et_we_company_name)
    TextInputLayout etWeCompanyName;
    @BindView(R.id.btn_rmv_we_company_name)
    ImageView btnRmvWeCompanyName;
    @BindView(R.id.et_we_job_position)
    TextInputLayout etWeJobPosition;
    @BindView(R.id.btn_rmv_we_job_position)
    ImageView btnRmvWeJobPosition;
    @BindView(R.id.et_we_job_description)
    TextInputLayout etWeJobDescription;
    @BindView(R.id.btn_rmv_we_job_description)
    ImageView btnRmvWeJobDescription;
    @BindView(R.id.btn_cancel_we)
    Button btnCancelWe;
    @BindView(R.id.btn_save_we)
    Button btnSaveWe;
    private ProgressDialog progressDialog;

    ApiService mApiService;
    SharedPreferences sharedPreferences;
    private String token;

    private String strWeMonthStart;
    private String strWeYearStart;
    private String strWeMonthFinish;
    private String strWeYearFinish;
    private String strWeCompanyName;
    private String strWeJobPosition;
    private String strWeJobDescription;

    private Dialog dialog;
    private DateFormat dateFormat;
    private Intent intent;
    private String page;
    private String initId;
    private String we_bulan_mulai;
    private String we_tahun_mulai;
    private String we_bulan_selesai;
    private String we_tahun_selesai;
    private String we_nama_perusahaan;
    private String we_posisi_pekerjaan;
    private String we_uraian_Tugas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_work_experience);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();

        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        dateFormat = new SimpleDateFormat("yyyy");
        dialog = new Dialog(WorkExperienceActivity.this);

        intent = getIntent();
        page = intent.getStringExtra("page");

        if (page.equals("update")) {
            initId = intent.getStringExtra("id");
            we_bulan_mulai = intent.getStringExtra("we_bulan_mulai");
            we_tahun_mulai = intent.getStringExtra("we_tahun_mulai");
            we_bulan_selesai = intent.getStringExtra("we_bulan_selesai");
            we_tahun_selesai = intent.getStringExtra("we_tahun_selesai");
            we_nama_perusahaan = intent.getStringExtra("we_nama_perusahaan");
            we_posisi_pekerjaan = intent.getStringExtra("we_posisi_pekerjaan");
            we_uraian_Tugas = intent.getStringExtra("we_uraian_Tugas");

            etWeMonthStart.setText(we_bulan_mulai);
            etWeYearStart.setText(we_tahun_mulai);
            etWeMonthFinish.setText(we_bulan_selesai);
            etWeYearFinish.setText(we_tahun_selesai);
            etWeCompanyName.getEditText().setText(we_nama_perusahaan);
            etWeJobPosition.getEditText().setText(we_posisi_pekerjaan);
            etWeJobDescription.getEditText().setText(we_uraian_Tugas);
        }
    }

    private boolean checkWorkExperience() {
        valueWorkExperience();
        if (strWeMonthStart.isEmpty()) tilWeMonthStart.setError("Wajib Di Isi");
        else tilWeMonthStart.setError(null);
        if (strWeYearStart.isEmpty()) tilWeYearStart.setError("Wajib Di Isi");
        else tilWeYearStart.setError(null);
        if (strWeMonthFinish.isEmpty()) tilWeMonthFinish.setError("Wajib Di Isi");
        else tilWeMonthFinish.setError(null);
        if (strWeYearFinish.isEmpty()) tilWeYearFinish.setError("Wajib Di Isi");
        else tilWeYearFinish.setError(null);
        if (strWeCompanyName.isEmpty()) etWeCompanyName.setError("Wajib Di Isi");
        else etWeCompanyName.setError(null);
        if (strWeJobPosition.isEmpty()) etWeJobPosition.setError("Wajib Di Isi");
        else etWeJobPosition.setError(null);
        if (strWeJobDescription.isEmpty()) etWeJobDescription.setError("Wajib Di Isi");
        else etWeJobDescription.setError(null);

        return !strWeMonthStart.isEmpty() && !strWeYearStart.isEmpty() && !strWeMonthFinish.isEmpty() && !strWeYearFinish.isEmpty() && !strWeCompanyName.isEmpty() && !strWeJobPosition.isEmpty() && !strWeJobDescription.isEmpty();
    }

    private void valueWorkExperience() {
        strWeMonthStart = etWeMonthStart.getText().toString().trim();
        strWeYearStart = etWeYearStart.getText().toString().trim();
        strWeMonthFinish = etWeMonthFinish.getText().toString().trim();
        strWeYearFinish = etWeYearFinish.getText().toString().trim();
        strWeCompanyName = etWeCompanyName.getEditText().getText().toString().trim();
        strWeJobPosition = etWeJobPosition.getEditText().getText().toString().trim();
        strWeJobDescription = etWeJobDescription.getEditText().getText().toString().trim();
    }

    @OnClick({R.id.btn_rmv_we_month_start, R.id.btn_rmv_we_year_start, R.id.btn_rmv_we_month_finish, R.id.btn_rmv_we_year_finish, R.id.btn_rmv_we_company_name, R.id.btn_rmv_we_job_position, R.id.btn_rmv_we_job_description})
    public void onClickRemoveWE(View view) {
        switch (view.getId()) {
            case R.id.btn_rmv_we_month_start:
                etWeMonthStart.getText().clear();
                break;
            case R.id.btn_rmv_we_year_start:
                etWeYearStart.getText().clear();
                break;
            case R.id.btn_rmv_we_month_finish:
                etWeMonthFinish.getText().clear();
                break;
            case R.id.btn_rmv_we_year_finish:
                etWeYearFinish.getText().clear();
                break;
            case R.id.btn_rmv_we_company_name:
                etWeCompanyName.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_we_job_position:
                etWeJobPosition.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_we_job_description:
                etWeJobDescription.getEditText().getText().clear();
                break;
        }
    }

    @OnClick({R.id.btn_cancel_we, R.id.btn_save_we})
    public void onCLickWE(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel_we:
                finish();
                break;
            case R.id.btn_save_we:
                if (checkWorkExperience()) {
                    progressDialog.show();
                    if (page.equals("new")) {
                        mApiService.workExperiencePost(token, strWeMonthStart, strWeYearStart, strWeMonthFinish, strWeYearFinish, strWeCompanyName, strWeJobPosition, strWeJobDescription).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etWeMonthStart.getText().clear();
                                    etWeYearStart.getText().clear();
                                    etWeMonthFinish.getText().clear();
                                    etWeYearFinish.getText().clear();
                                    etWeCompanyName.getEditText().getText().clear();
                                    etWeJobPosition.getEditText().getText().clear();
                                    etWeJobDescription.getEditText().getText().clear();
                                    Util.showToast(WorkExperienceActivity.this, response.message());
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(WorkExperienceActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(WorkExperienceActivity.this, "onFailure");
                            }
                        });
                    } else {
                        mApiService.workExperienceUpdate(token, Integer.valueOf(initId), strWeMonthStart, strWeYearStart, strWeMonthFinish, strWeYearFinish, strWeCompanyName, strWeJobPosition, strWeJobDescription).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    finish();
                                    Util.showToast(WorkExperienceActivity.this, response.message());
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(WorkExperienceActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(WorkExperienceActivity.this, "onFailure");
                            }
                        });
                    }
                }
                break;
        }
    }

    @OnClick({R.id.et_we_month_start, R.id.et_we_year_start, R.id.et_we_month_finish, R.id.et_we_year_finish})
    public void onClickDateWE(View view) {
        switch (view.getId()) {
            case R.id.et_we_month_start:
                String monthStart = "we_month_start";
                dialogMonth(monthStart);
                break;
            case R.id.et_we_year_start:
                String yearStart = "we_year_start";
                dialogYear(yearStart);
                break;
            case R.id.et_we_month_finish:
                String monthFinish = "we_month_finish";
                dialogMonth(monthFinish);
                break;
            case R.id.et_we_year_finish:
                String yearFinish = "we_year_finish";
                dialogYear(yearFinish);
                break;
        }
    }

    private void dialogYear(final String strYear) {
        String dateYearStart = dateFormat.format(Calendar.getInstance().getTime());
        if (strYear.equals("we_year_start")) {
            dialog.setTitle("Tahun Mulai");
        } else {
            dialog.setTitle("Tahun Selesai");
        }
        dialog.setContentView(R.layout.utils_year_picker);
        Button btnCancelNp = (Button) dialog.findViewById(R.id.btn_cancel_np);
        Button btnSaveNp = (Button) dialog.findViewById(R.id.btn_save_np);
        final NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.np_year);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(9999);
        numberPicker.setValue(Integer.parseInt(dateYearStart));
        numberPicker.setWrapSelectorWheel(false);
        btnCancelNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSaveNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strYear.equals("we_year_start")) {
                    etWeYearStart.setText(String.valueOf(numberPicker.getValue()));
                } else {
                    etWeYearFinish.setText(String.valueOf(numberPicker.getValue()));
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dialogMonth(final String valueMonth) {
        final CharSequence[] items = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember",};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pilih Bulan");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (valueMonth.equals("we_month_start")) {
                    etWeMonthStart.setText(items[item]);
                } else {
                    etWeMonthFinish.setText(items[item]);
                }
            }
        });
        builder.show();
    }
}
