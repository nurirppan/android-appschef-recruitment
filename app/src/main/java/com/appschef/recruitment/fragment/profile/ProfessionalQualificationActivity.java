package com.appschef.recruitment.fragment.profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.bus.RefreshProfile;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfessionalQualificationActivity extends AppCompatActivity {

    @BindView(R.id.et_pq_name_of_qualification)
    TextInputLayout etPqNameOfQualification;
    @BindView(R.id.btn_rmv_pq_name_of_qualification)
    ImageView btnRmvPqNameOfQualification;
    @BindView(R.id.et_pq_held_by)
    TextInputLayout etPqHeldBy;
    @BindView(R.id.btn_rmv_pq_held_by)
    ImageView btnRmvPqHeldBy;
    @BindView(R.id.et_pq_obtained_years)
    TextInputEditText etPqObtainedYears;
    @BindView(R.id.til_pq_obtained_years)
    TextInputLayout tilPqObtainedYears;
    @BindView(R.id.btn_rmv_pq_obtained_years)
    ImageView btnRmvPqObtainedYears;
    @BindView(R.id.btn_cancel_pq)
    Button btnCancelPq;
    @BindView(R.id.btn_save_pq)
    Button btnSavePq;
    private ProgressDialog progressDialog;
    ApiService mApiService;
    SharedPreferences sharedPreferences;
    private String token;
    private String strPqNameOfQualification;
    private String strPqHeldBy;
    private String strPqObtainedYears;

    private Dialog dialog;
    private DateFormat dateFormat;
    private Intent intent;
    private String page;
    private String initId;
    private String pq_nama_kualifikasi_profesional;
    private String pq_diselenggarakan_oleh;
    private String pq_diterima_tahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_professional_qualification);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();

        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        dateFormat = new SimpleDateFormat("yyyy");
        dialog = new Dialog(ProfessionalQualificationActivity.this);

        intent = getIntent();
        page = intent.getStringExtra("page");

        if (page.equals("update")) {
            initId = intent.getStringExtra("id");
            pq_nama_kualifikasi_profesional = intent.getStringExtra("pq_nama_kualifikasi_profesional");
            pq_diselenggarakan_oleh = intent.getStringExtra("pq_diselenggarakan_oleh");
            pq_diterima_tahun = intent.getStringExtra("pq_diterima_tahun");

            etPqNameOfQualification.getEditText().setText(pq_nama_kualifikasi_profesional);
            etPqHeldBy.getEditText().setText(pq_diselenggarakan_oleh);
            etPqObtainedYears.setText(pq_diterima_tahun);
        }
    }

    private boolean checkProfessionalQualification() {
        valueProfessionalQualification();
        if (strPqNameOfQualification.isEmpty()) etPqNameOfQualification.setError("Wajib Di Isi");
        else etPqNameOfQualification.setError(null);
        if (strPqHeldBy.isEmpty()) etPqHeldBy.setError("Wajib Di Isi");
        else etPqHeldBy.setError(null);
        if (strPqObtainedYears.isEmpty()) tilPqObtainedYears.setError("Wajib Di Isi");
        else tilPqObtainedYears.setError(null);

        return !strPqNameOfQualification.isEmpty() && !strPqHeldBy.isEmpty() && !strPqObtainedYears.isEmpty();
    }

    private void valueProfessionalQualification() {
        strPqNameOfQualification = etPqNameOfQualification.getEditText().getText().toString().trim();
        strPqHeldBy = etPqHeldBy.getEditText().getText().toString().trim();
        strPqObtainedYears = etPqObtainedYears.getText().toString().trim();
    }

    @OnClick({R.id.btn_rmv_pq_name_of_qualification, R.id.btn_rmv_pq_held_by, R.id.btn_rmv_pq_obtained_years})
    public void onClickRemovePQ(View view) {
        switch (view.getId()) {
            case R.id.btn_rmv_pq_name_of_qualification:
                etPqNameOfQualification.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pq_held_by:
                etPqHeldBy.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pq_obtained_years:
                etPqObtainedYears.getText().clear();
                break;
        }
    }

    @OnClick({R.id.btn_cancel_pq, R.id.btn_save_pq})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel_pq:
                finish();
                break;
            case R.id.btn_save_pq:
                if (checkProfessionalQualification()) {
                    progressDialog.show();
                    if (page.equals("new")) {
                        mApiService.professionalQualificationPost(token, strPqNameOfQualification, strPqHeldBy, strPqObtainedYears).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etPqNameOfQualification.getEditText().getText().clear();
                                    etPqHeldBy.getEditText().getText().clear();
                                    etPqObtainedYears.getText().clear();
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                    Util.showToast(ProfessionalQualificationActivity.this, response.message());
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(ProfessionalQualificationActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(ProfessionalQualificationActivity.this, "onFailure");
                            }
                        });
                    } else {
                        mApiService.professionalQualificationUpdate(token, Integer.valueOf(initId), strPqNameOfQualification, strPqHeldBy, strPqObtainedYears).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    finish();
                                    etPqNameOfQualification.getEditText().getText().clear();
                                    etPqHeldBy.getEditText().getText().clear();
                                    etPqObtainedYears.getText().clear();
                                    Util.showToast(ProfessionalQualificationActivity.this, response.message());
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(ProfessionalQualificationActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(ProfessionalQualificationActivity.this, "onFailure");
                            }
                        });
                    }
                }
                break;
        }
    }

    @OnClick(R.id.et_pq_obtained_years)
    public void onViewClicked() {
        String dateYearStart = dateFormat.format(Calendar.getInstance().getTime());
        dialog.setTitle("Tahun");
        dialog.setContentView(R.layout.utils_year_picker);
        Button btnCancelNp = (Button) dialog.findViewById(R.id.btn_cancel_np);
        Button btnSaveNp = (Button) dialog.findViewById(R.id.btn_save_np);
        final NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.np_year);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(9999);
        numberPicker.setValue(Integer.parseInt(dateYearStart));
        numberPicker.setWrapSelectorWheel(false);
        btnCancelNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSaveNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPqObtainedYears.setText(String.valueOf(numberPicker.getValue()));
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
