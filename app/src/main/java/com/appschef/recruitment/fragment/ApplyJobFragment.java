package com.appschef.recruitment.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplyJobFragment extends Fragment {

    @BindView(R.id.ll_main_ruby_on_rails)
    LinearLayout llMainRubyOnRails;
    @BindView(R.id.tv_requirements_ruby_on_rails)
    TextView tvRequirementsRubyOnRails;
    @BindView(R.id.cbx_ruby_on_rails)
    CheckBox cbxRubyOnRails;
    @BindView(R.id.ll_wraper_ruby_on_rails)
    LinearLayout llWraperRubyOnRails;
    @BindView(R.id.ll_main_php)
    LinearLayout llMainPhp;
    @BindView(R.id.ll_main_java)
    LinearLayout llMainJava;
    @BindView(R.id.tv_requirements_java)
    TextView tvRequirementsJava;
    @BindView(R.id.cbx_java)
    CheckBox cbxJava;
    @BindView(R.id.ll_wraper_java)
    LinearLayout llWraperJava;
    @BindView(R.id.ll_main_quality_asurance)
    LinearLayout llMainQualityAsurance;
    @BindView(R.id.tv_requirements_quality_asurance)
    TextView tvRequirementsQualityAsurance;
    @BindView(R.id.cbx_quality_asurance)
    CheckBox cbxQualityAsurance;
    @BindView(R.id.ll_wraper_quality_asurance)
    LinearLayout llWraperQualityAsurance;
    @BindView(R.id.tv_requirements_android)
    TextView tvRequirementsAndroid;
    @BindView(R.id.cbx_android)
    CheckBox cbxAndroid;
    @BindView(R.id.ll_wraper_android)
    LinearLayout llWraperAndroid;
    @BindView(R.id.tv_requirements_laravel)
    TextView tvRequirementsLaravel;
    @BindView(R.id.cbx_laravel)
    CheckBox cbxLaravel;
    @BindView(R.id.ll_wraper_laravel)
    LinearLayout llWraperLaravel;
    @BindView(R.id.btn_aj_submit)
    Button btnAjSubmit;
    Unbinder unbinder;
    ApiService mApiService;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    private String strTypeJob;
    private String token;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_apply_job, container, false);
        getActivity().setTitle("Apply Job");
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mApiService = UtilsApi.getApiService();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_aj_submit})
    public void onClickApplyJob(View view) {
        switch (view.getId()) {
            case R.id.btn_aj_submit:
                if (!cbxAndroid.isChecked() && !cbxLaravel.isChecked()) Util.showToast(getContext(), "Silahkan Pilih Pekerjaan");
                else if (cbxAndroid.isChecked() && cbxLaravel.isChecked()) Util.showToast(getContext(), "Maksimal 1 Lamaran Pekerjaan");
                else applyJob();
                break;
        }
    }

    private void applyJob() {
        progressDialog.show();
        strTypeJob = (cbxAndroid.isChecked()) ? "android" : "laravel";
        mApiService.postUploadJob(token, strTypeJob).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    Util.showToast(getContext(), "Berhasil Melamar Pekerjaan");
                } else {
                    progressDialog.dismiss();
                    Util.showToast(getContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDialog.dismiss();
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    @OnClick({R.id.cbx_android, R.id.cbx_laravel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cbx_android:
                if (tvRequirementsAndroid.getVisibility() == View.GONE) tvRequirementsAndroid.setVisibility(View.VISIBLE);
                else tvRequirementsAndroid.setVisibility(View.GONE);
                break;
            case R.id.cbx_laravel:
                if (tvRequirementsLaravel.getVisibility() == View.GONE) tvRequirementsLaravel.setVisibility(View.VISIBLE);
                else tvRequirementsLaravel.setVisibility(View.GONE);
                break;
        }
    }
}
