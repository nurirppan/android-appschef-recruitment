package com.appschef.recruitment.fragment.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.bus.RefreshProfile;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SkillProgramActivity extends AppCompatActivity {

    @BindView(R.id.et_sp_skill_names)
    TextInputLayout etSpSkillNames;
    @BindView(R.id.btn_rmv_sp_skill_names)
    ImageView btnRmvSpSkillNames;
    @BindView(R.id.et_sp_skill_description)
    TextInputLayout etSpSkillDescription;
    @BindView(R.id.btn_rmv_sp_skill_description)
    ImageView btnRmvSpSkillDescription;
    @BindView(R.id.btn_cancel_ach)
    Button btnCancelAch;
    @BindView(R.id.btn_save_ach)
    Button btnSaveAch;

    private ProgressDialog progressDialog;
    ApiService mApiService;
    SharedPreferences sharedPreferences;
    private String token;
    private String strSpSkillNames;
    private String strSpSkillDescription;
    private Intent intent;
    private String page;
    private String initId;
    private String sp_nama_keahlian;
    private String sp_deskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_skill_program);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();

        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        intent = getIntent();
        page = intent.getStringExtra("page");

        if (page.equals("update")) {
            initId = intent.getStringExtra("id");
            sp_nama_keahlian = intent.getStringExtra("sp_nama_keahlian");
            sp_deskripsi = intent.getStringExtra("sp_deskripsi");

            etSpSkillNames.getEditText().setText(sp_nama_keahlian);
            etSpSkillDescription.getEditText().setText(sp_deskripsi);
        }
    }

    private void valueSkillProgram() {
        strSpSkillNames = etSpSkillNames.getEditText().getText().toString().trim();
        strSpSkillDescription = etSpSkillDescription.getEditText().getText().toString().trim();
    }

    private boolean checkSkillProgram() {
        valueSkillProgram();
        if (strSpSkillNames.isEmpty()) etSpSkillNames.setError("Wajib Di Isi");
        else etSpSkillNames.setError(null);
        if (strSpSkillDescription.isEmpty()) etSpSkillDescription.setError("Wajib Di Isi");
        else etSpSkillDescription.setError(null);
        return !strSpSkillNames.isEmpty() && !strSpSkillDescription.isEmpty();
    }

    @OnClick({R.id.btn_rmv_sp_skill_names, R.id.btn_rmv_sp_skill_description})
    public void onClickRemoveSP(View view) {
        switch (view.getId()) {
            case R.id.btn_rmv_sp_skill_names:
                etSpSkillNames.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_sp_skill_description:
                etSpSkillDescription.getEditText().getText().clear();
                break;
        }
    }

    @OnClick({R.id.btn_cancel_ach, R.id.btn_save_ach})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel_ach:
                finish();
                break;
            case R.id.btn_save_ach:
                if (checkSkillProgram()) {
                    progressDialog.show();
                    if (page.equals("new")) {
                        mApiService.skillsProgramPost(token, strSpSkillNames, strSpSkillDescription).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etSpSkillNames.getEditText().getText().clear();
                                    etSpSkillDescription.getEditText().getText().clear();
                                    Util.showToast(SkillProgramActivity.this, response.message());
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(SkillProgramActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(SkillProgramActivity.this, "onFailure");
                            }
                        });
                    } else {
                        mApiService.skillsProgramUpdate(token, Integer.valueOf(initId), strSpSkillNames, strSpSkillDescription).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etSpSkillNames.getEditText().getText().clear();
                                    etSpSkillDescription.getEditText().getText().clear();
                                    Util.showToast(SkillProgramActivity.this, response.message());
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(SkillProgramActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(SkillProgramActivity.this, "onFailure");
                                Log.i("PostSP", "onFailure");
                            }
                        });
                    }
                }
                break;
        }
    }
}
