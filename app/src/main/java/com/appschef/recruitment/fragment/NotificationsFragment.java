package com.appschef.recruitment.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appschef.recruitment.R;
import com.appschef.recruitment.adapter.NotificationAdapter;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.model.response.childresponse.NotificationResponse;
import com.appschef.recruitment.model.response.parentresponse.NotificationParent;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsFragment extends Fragment {

    @BindView(R.id.rv_list_notification)
    RecyclerView rvListNotification;
    Unbinder unbinder;
    private ApiService mApiService;
    private SharedPreferences sharedPreferences;
    private String token;
    private ProgressDialog progressDialog;
    private List<NotificationResponse> notificationResponseList = new ArrayList<>();
    private NotificationAdapter notificationAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        getActivity().setTitle("Pemberitahuan");
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();
        mApiService.getListNotification(token).enqueue(new Callback<BaseResponse<NotificationParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<NotificationParent>> call, Response<BaseResponse<NotificationParent>> response) {
                if (response.isSuccessful()) {
                    RecyclerView.LayoutManager llRv = new LinearLayoutManager(getContext());
                    rvListNotification.setLayoutManager(llRv);
                    rvListNotification.setItemAnimator(new DefaultItemAnimator());
                    notificationResponseList = response.body().getData().getNotifications();
                    notificationAdapter = new NotificationAdapter(getContext(), notificationResponseList, mApiService, token, NotificationsFragment.this);
                    rvListNotification.setAdapter(notificationAdapter);
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Util.showToast(getContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<NotificationParent>> call, Throwable t) {
                progressDialog.dismiss();
                Util.showToast(getContext(), "onFailure");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
