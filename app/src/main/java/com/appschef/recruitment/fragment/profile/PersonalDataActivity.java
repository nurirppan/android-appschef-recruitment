package com.appschef.recruitment.fragment.profile;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.appschef.recruitment.R;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.model.bus.RefreshProfile;
import com.appschef.recruitment.model.response.parentresponse.profile.PersonalDataParent;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PersonalDataActivity extends AppCompatActivity {

    ApiService mApiService;
    @BindView(R.id.et_pd_ktp_number)
    TextInputLayout etPdKtpNumber;
    @BindView(R.id.btn_rmv_pd_ktp_number)
    ImageView btnRmvPdKtpNumber;
    @BindView(R.id.et_pd_username)
    TextInputLayout etPdUsername;
    @BindView(R.id.btn_rmv_pd_username)
    ImageView btnRmvPdUsername;
    @BindView(R.id.et_pd_fullname)
    TextInputLayout etPdFullname;
    @BindView(R.id.btn_rmv_pd_fullname)
    ImageView btnRmvPdFullname;
    @BindView(R.id.tv_gender)
    TextView tvGender;
    @BindView(R.id.rb_gender_man)
    RadioButton rbGenderMan;
    @BindView(R.id.rb_gender_woman)
    RadioButton rbGenderWoman;
    @BindView(R.id.rg_gender)
    RadioGroup rgGender;
    @BindView(R.id.et_pd_birth_place)
    TextInputLayout etPdBirthPlace;
    @BindView(R.id.btn_rmv_pd_birth_place)
    ImageView btnRmvPdBirthPlace;
    @BindView(R.id.et_pd_birth_date)
    TextInputEditText etPdBirthDate;
    @BindView(R.id.btn_rmv_pd_birth_date)
    ImageView btnRmvPdBirthDate;
    @BindView(R.id.et_pd_age)
    TextInputLayout etPdAge;
    @BindView(R.id.spn_citizen)
    Spinner spnCitizen;
    @BindView(R.id.tv_marital_status)
    TextView tvMaritalStatus;
    @BindView(R.id.rb_single)
    RadioButton rbSingle;
    @BindView(R.id.rb_married)
    RadioButton rbMarried;
    @BindView(R.id.rg_marital_status)
    RadioGroup rgMaritalStatus;
    @BindView(R.id.et_pd_address)
    TextInputLayout etPdAddress;
    @BindView(R.id.btn_rmv_pd_address)
    ImageView btnRmvPdAddress;
    @BindView(R.id.et_pd_rt)
    TextInputLayout etPdRt;
    @BindView(R.id.btn_rmv_pd_rt)
    ImageView btnRmvPdRt;
    @BindView(R.id.et_pd_rw)
    TextInputLayout etPdRw;
    @BindView(R.id.btn_rmv_pd_rw)
    ImageView btnRmvPdRw;
    @BindView(R.id.et_pd_urban_village)
    TextInputLayout etPdUrbanVillage;
    @BindView(R.id.btn_rmv_pd_urban_village)
    ImageView btnRmvPdUrbanVillage;
    @BindView(R.id.et_pd_sub_district)
    TextInputLayout etPdSubDistrict;
    @BindView(R.id.btn_rmv_pd_sub_district)
    ImageView btnRmvPdSubDistrict;
    @BindView(R.id.et_pd_city)
    TextInputLayout etPdCity;
    @BindView(R.id.btn_rmv_pd_city)
    ImageView btnRmvPdCity;
    @BindView(R.id.et_pd_home_phone)
    TextInputLayout etPdHomePhone;
    @BindView(R.id.btn_rmv_pd_home_phone)
    ImageView btnRmvPdHomePhone;
    @BindView(R.id.et_pd_handphone_one)
    TextInputLayout etPdHandphoneOne;
    @BindView(R.id.btn_rmv_pd_handphone_one)
    ImageView btnRmvPdHandphoneOne;
    @BindView(R.id.et_pd_handphone_two)
    TextInputLayout etPdHandphoneTwo;
    @BindView(R.id.btn_rmv_pd_handphone_two)
    ImageView btnRmvPdHandphoneTwo;
    @BindView(R.id.et_pd_postal_code)
    TextInputLayout etPdPostalCode;
    @BindView(R.id.btn_rmv_pd_postal_code)
    ImageView btnRmvPdPostalCode;
    @BindView(R.id.et_pd_email)
    TextInputLayout etPdEmail;
    @BindView(R.id.btn_rmv_pd_email)
    ImageView btnRmvPdEmail;
    @BindView(R.id.btn_pd_no)
    Button btnPdNo;
    @BindView(R.id.btn_pd_yes)
    Button btnPdYes;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Calendar birthCalendar, nowCalender;

    private String pdKtpNumber;
    private String pdUsername;
    private String pdFullname;
    private String pdBirthPlace;
    private String pdBirthDate;
    private String pdPdAge;
    private String pdAddress;
    private String pdRt;
    private String pdRw;
    private String pdUrbanVillage;
    private String pdSubDistrict;
    private String pdCity;
    private String pdHomePhone;
    private String pdHandphoneOne;
    private String pdHandphoneTwo;
    private String pdPostalCode;
    private String pdEmail;
    private String strGender;
    private String strMaritalStatus;
    private String strCitizen;
    private ProgressDialog progressDialog;

    private Map<String, RequestBody> map;

    SharedPreferences sharedPreferences;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_personal_data);
        ButterKnife.bind(this);

        mApiService = UtilsApi.getApiService();

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        birthCalendar = Calendar.getInstance();
        nowCalender = Calendar.getInstance();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        valuePersonalDataGet();
    }

    @OnClick({R.id.et_pd_birth_date})
    public void onClickDatePD(View view) {
        switch (view.getId()) {
            case R.id.et_pd_birth_date:
                datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        birthCalendar.set(year, month, dayOfMonth);
                        int ageYears = nowCalender.get(Calendar.YEAR) - year;
                        int ageMonth = nowCalender.get(Calendar.MONTH) - month;
                        int ageDayOfMonth = nowCalender.get(Calendar.DAY_OF_MONTH) - dayOfMonth;
                        if (ageDayOfMonth < 0) {
                            ageMonth--;
                            ageDayOfMonth += nowCalender.getActualMaximum(Calendar.DAY_OF_MONTH);
                        }
                        if (ageMonth < 0) {
                            ageYears--;
                            ageMonth += 12;
                        }
                        etPdAge.getEditText().setText(ageYears + " Tahun " + ageMonth + " Bulan " + ageDayOfMonth + " Hari");
                        etPdBirthDate.setText(dateFormatter.format(birthCalendar.getTime()));
                    }
                }, birthCalendar.get(Calendar.YEAR), birthCalendar.get(Calendar.MONTH), birthCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
        }
    }

    @OnClick({R.id.btn_rmv_pd_ktp_number, R.id.btn_rmv_pd_username, R.id.btn_rmv_pd_fullname, R.id.btn_rmv_pd_birth_place, R.id.btn_rmv_pd_birth_date, R.id.btn_rmv_pd_address, R.id.btn_rmv_pd_rt, R.id.btn_rmv_pd_rw, R.id.btn_rmv_pd_urban_village, R.id.btn_rmv_pd_sub_district, R.id.btn_rmv_pd_city, R.id.btn_rmv_pd_home_phone, R.id.btn_rmv_pd_handphone_one, R.id.btn_rmv_pd_handphone_two, R.id.btn_rmv_pd_postal_code, R.id.btn_rmv_pd_email})
    public void onClickRemovePersonalData(View view) {
        switch (view.getId()) {
            case R.id.btn_rmv_pd_ktp_number:
                etPdKtpNumber.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_username:
                etPdUsername.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_fullname:
                etPdFullname.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_birth_place:
                etPdBirthPlace.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_birth_date:
                etPdBirthDate.getText().clear();
                break;
            case R.id.btn_rmv_pd_address:
                etPdAddress.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_rt:
                etPdRt.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_rw:
                etPdRw.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_urban_village:
                etPdUrbanVillage.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_sub_district:
                etPdSubDistrict.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_city:
                etPdCity.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_home_phone:
                etPdHomePhone.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_handphone_one:
                etPdHandphoneOne.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_handphone_two:
                etPdHandphoneTwo.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_postal_code:
                etPdPostalCode.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_pd_email:
                etPdEmail.getEditText().getText().clear();
                break;
        }
    }

    private boolean checkDataPersonalData() {
        valuePersonalData();
        if (pdKtpNumber.isEmpty()) etPdKtpNumber.setError("Nomor KTP Wajib Di Isi");
        else if (pdKtpNumber.length() < 16) etPdKtpNumber.setError("Panjang Nomor KTP Tidak Sesuai");
        else etPdKtpNumber.setError(null);

        if (pdUsername.isEmpty()) etPdUsername.setError("Username Wajib Di Isi");
        else etPdUsername.setError(null);

        if (pdFullname.isEmpty()) etPdFullname.setError("Nama Lengkap Wajib Di Isi");
        else etPdFullname.setError(null);

        if (pdBirthPlace.isEmpty()) etPdBirthPlace.setError("Tempat Lahir Wajib Di Isi");
        else etPdBirthPlace.setError(null);

        if (pdBirthDate.isEmpty()) etPdBirthDate.setError("Tanggal Lahir Wajib Di Isi");
        else etPdBirthDate.setError(null);

        if (pdPdAge.isEmpty()) etPdAge.setError("Umur Wajib Di Isi");
        else etPdAge.setError(null);

        if (pdAddress.isEmpty()) etPdAddress.setError("Alamat Wajib Di Isi");
        else etPdAddress.setError(null);

        if (pdRt.isEmpty()) etPdRt.setError("RT Wajib Di Isi");
        else etPdRt.setError(null);

        if (pdRw.isEmpty()) etPdRw.setError("RW Wajib Di Isi");
        else etPdRw.setError(null);

        if (pdUrbanVillage.isEmpty()) etPdUrbanVillage.setError("Kecamatan Wajib Di Isi");
        else etPdUrbanVillage.setError(null);

        if (pdSubDistrict.isEmpty()) etPdSubDistrict.setError("Kelurahan Wajib Di Isi");
        else etPdSubDistrict.setError(null);

        if (pdCity.isEmpty()) etPdCity.setError("Kota Wajib Di Isi");
        else etPdCity.setError(null);

        if (pdHomePhone.isEmpty()) etPdHomePhone.setError("Telepon Wajib Di Isi");
        else etPdHomePhone.setError(null);

        if (pdHandphoneOne.isEmpty()) etPdHandphoneOne.setError("Nomor Handphone 1 Wajib Di Isi");
        else etPdHandphoneOne.setError(null);

        if (pdHandphoneTwo.isEmpty()) etPdHandphoneTwo.setError("Nomor Handphone 2 Wajib Di Isi");
        else etPdHandphoneTwo.setError(null);

        if (pdPostalCode.isEmpty()) etPdPostalCode.setError("Kode Pos Wajib Di Isi");
        else etPdPostalCode.setError(null);

        if (pdEmail.isEmpty()) etPdEmail.setError("Email Wajib Di Isi");
        else etPdEmail.setError(null);

        strGender = (!rbGenderMan.isChecked() && !rbGenderWoman.isChecked()) ? "0" : "1";
        strMaritalStatus = (!rbSingle.isChecked() && !rbMarried.isChecked()) ? "0" : "1";

        if (strGender.equals("0")) tvGender.setTextColor(Color.RED);
        else tvGender.setTextColor(getResources().getColor(R.color.text_colour_default));

        if (strMaritalStatus.equals("0")) tvMaritalStatus.setTextColor(Color.RED);
        else tvMaritalStatus.setTextColor(getResources().getColor(R.color.text_colour_default));

        return !pdKtpNumber.isEmpty() && pdKtpNumber.length() <= 16 && !pdFullname.isEmpty() && !strGender.equals("0") && !pdBirthPlace.isEmpty() && !pdBirthDate.isEmpty() && !pdPdAge.isEmpty() && !strMaritalStatus.equals("0") && !pdAddress.isEmpty() && !pdRt.isEmpty() && !pdRw.isEmpty() && !pdUrbanVillage.isEmpty() && !pdSubDistrict.isEmpty() && !pdCity.isEmpty() && !pdHomePhone.isEmpty() && !pdHandphoneOne.isEmpty() && !pdHandphoneTwo.isEmpty() && !pdPostalCode.isEmpty() && !pdEmail.isEmpty();
    }

    private void valuePersonalData() {
        pdKtpNumber = etPdKtpNumber.getEditText().getText().toString().trim();
        pdUsername = etPdUsername.getEditText().getText().toString().trim();
        pdFullname = etPdFullname.getEditText().getText().toString().trim();
        strGender = (rbGenderMan.isChecked()) ? "Laki-laki" : "Perempuan";
        pdBirthPlace = etPdBirthPlace.getEditText().getText().toString().trim();
        pdBirthDate = etPdBirthDate.getText().toString().trim();
        pdPdAge = etPdAge.getEditText().getText().toString().trim();
        strCitizen = spnCitizen.getSelectedItem().toString();
        strMaritalStatus = (rbSingle.isChecked()) ? "Lajang" : "Menikah";
        pdAddress = etPdAddress.getEditText().getText().toString().trim();
        pdRt = etPdRt.getEditText().getText().toString().trim();
        pdRw = etPdRw.getEditText().getText().toString().trim();
        pdUrbanVillage = etPdUrbanVillage.getEditText().getText().toString().trim();
        pdSubDistrict = etPdSubDistrict.getEditText().getText().toString().trim();
        pdCity = etPdCity.getEditText().getText().toString().trim();
        pdHomePhone = etPdHomePhone.getEditText().getText().toString().trim();
        pdHandphoneOne = etPdHandphoneOne.getEditText().getText().toString().trim();
        pdHandphoneTwo = etPdHandphoneTwo.getEditText().getText().toString().trim();
        pdPostalCode = etPdPostalCode.getEditText().getText().toString().trim();
        pdEmail = etPdEmail.getEditText().getText().toString().trim();
    }

    private void valuePersonalDataGet() {
        progressDialog.show();
        mApiService.personalDataGet(token).enqueue(new Callback<BaseResponse<PersonalDataParent>>() {
            @Override
            public void onResponse(Call<BaseResponse<PersonalDataParent>> call, Response<BaseResponse<PersonalDataParent>> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) setPersonalData(response);
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    Util.showToast(PersonalDataActivity.this, response.message());
                }
            }

            private void setPersonalData(Response<BaseResponse<PersonalDataParent>> response) {
                String strTempatLahir = (response.body().getData().getPersonalDataGets().getPdTempatLahir() == null) ? "" : response.body().getData().getPersonalDataGets().getPdTempatLahir();
                String strTanggalLahir = (response.body().getData().getPersonalDataGets().getPdTanggalLahir() == null) ? "" : response.body().getData().getPersonalDataGets().getPdTanggalLahir();
                String strRt = (response.body().getData().getPersonalDataGets().getPdRt() == null) ? "" : response.body().getData().getPersonalDataGets().getPdRt();
                String strRw = (response.body().getData().getPersonalDataGets().getPdRw() == null) ? "" : response.body().getData().getPersonalDataGets().getPdRw();
                String strKecamatan = (response.body().getData().getPersonalDataGets().getPdKecamatan() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKecamatan();
                String strKelurahan = (response.body().getData().getPersonalDataGets().getPdKelurahan() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKelurahan();

                etPdKtpNumber.getEditText().setText((response.body().getData().getPersonalDataGets().getPdKtp() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKtp());
                etPdUsername.getEditText().setText((response.body().getData().getPersonalDataGets().getPdUsername() == null) ? "" : response.body().getData().getPersonalDataGets().getPdUsername());
                etPdFullname.getEditText().setText((response.body().getData().getPersonalDataGets().getPdNamaLengkap() == null) ? "" : response.body().getData().getPersonalDataGets().getPdNamaLengkap());

                if (response.body().getData().getPersonalDataGets().getPdJenisKelamin().equals(getString(R.string.txt_laki_laki))) rbGenderMan.setChecked(true);
                else if (response.body().getData().getPersonalDataGets().getPdJenisKelamin().equals(getString(R.string.txt_perempuan))) rbGenderWoman.setChecked(true);

                etPdBirthPlace.getEditText().setText(strTempatLahir);
                etPdBirthDate.setText(strTanggalLahir);
                etPdAge.getEditText().setText((response.body().getData().getPersonalDataGets().getPdUmur() == null) ? "" : response.body().getData().getPersonalDataGets().getPdUmur());

                if (response.body().getData().getPersonalDataGets().getPdStatusPernikahan().equals(getString(R.string.lajang))) rbSingle.setChecked(true);
                else if (response.body().getData().getPersonalDataGets().getPdStatusPernikahan().equals(getString(R.string.menikah))) rbMarried.setChecked(true);

                etPdAddress.getEditText().setText((response.body().getData().getPersonalDataGets().getPdAlamat() == null) ? "" : response.body().getData().getPersonalDataGets().getPdAlamat());
                etPdRt.getEditText().setText(strRt);
                etPdRw.getEditText().setText(strRw);
                etPdUrbanVillage.getEditText().setText(strKecamatan);
                etPdSubDistrict.getEditText().setText(strKelurahan);
                etPdCity.getEditText().setText((response.body().getData().getPersonalDataGets().getPdKota() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKota());
                etPdHomePhone.getEditText().setText((response.body().getData().getPersonalDataGets().getPdTeleponRumah() == null) ? "" : response.body().getData().getPersonalDataGets().getPdTeleponRumah());
                etPdHandphoneOne.getEditText().setText((response.body().getData().getPersonalDataGets().getPdNomorHandphoneSatu() == null) ? "" : response.body().getData().getPersonalDataGets().getPdNomorHandphoneSatu());
                etPdHandphoneTwo.getEditText().setText((response.body().getData().getPersonalDataGets().getPdNomorHandphoneDua() == null) ? "" : response.body().getData().getPersonalDataGets().getPdNomorHandphoneDua());
                etPdPostalCode.getEditText().setText((response.body().getData().getPersonalDataGets().getPdKodePos() == null) ? "" : response.body().getData().getPersonalDataGets().getPdKodePos());
                etPdEmail.getEditText().setText((response.body().getData().getPersonalDataGets().getPdEmail() == null) ? "" : response.body().getData().getPersonalDataGets().getPdEmail());
            }

            @Override
            public void onFailure(Call<BaseResponse<PersonalDataParent>> call, Throwable t) {
                progressDialog.dismiss();
                Util.showToast(PersonalDataActivity.this, "onFailure");
            }
        });
    }

    @OnClick({R.id.btn_pd_no, R.id.btn_pd_yes})
    public void onClickPersonalData(View view) {
        switch (view.getId()) {
            case R.id.btn_pd_no:
                finish();
                break;
            case R.id.btn_pd_yes:
                if (checkDataPersonalData()) {
                    progressDialog.show();
                    postPersonalData();
                }
                break;
        }
    }

    private void postPersonalData() {
        valuePersonalData();
        mapPersonalDataPost();
        mApiService.personalDataPost(token, map).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    EventBus.getDefault().post(new RefreshProfile());
                    Util.showToast(PersonalDataActivity.this, response.message());
                    progressDialog.dismiss();
                    finish();
                } else {
                    progressDialog.dismiss();
                    Util.showToast(PersonalDataActivity.this, response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Util.showToast(PersonalDataActivity.this,  "onFailure");
            }
        });
    }

    private void mapPersonalDataPost() {
        map = new HashMap<>();
        map.put("pd_ktp", Util.toTextRequestBody(pdKtpNumber));
        map.put("pd_username", Util.toTextRequestBody(pdUsername));
        map.put("pd_nama_lengkap", Util.toTextRequestBody(pdFullname));
        map.put("pd_jenis_kelamin", Util.toTextRequestBody(strGender));
        map.put("pd_tempat_lahir", Util.toTextRequestBody(pdBirthPlace));
        map.put("pd_tanggal_lahir", Util.toTextRequestBody(pdBirthDate));
        map.put("pd_umur", Util.toTextRequestBody(pdPdAge));
        map.put("pd_kewarganegaraan", Util.toTextRequestBody(strCitizen));
        map.put("pd_status_pernikahan", Util.toTextRequestBody(strMaritalStatus));
        map.put("pd_alamat", Util.toTextRequestBody(pdAddress));
        map.put("pd_rt", Util.toTextRequestBody(pdRt));
        map.put("pd_rw", Util.toTextRequestBody(pdRw));
        map.put("pd_kelurahan", Util.toTextRequestBody(pdUrbanVillage));
        map.put("pd_kecamatan", Util.toTextRequestBody(pdSubDistrict));
        map.put("pd_kota", Util.toTextRequestBody(pdCity));
        map.put("pd_telepon_rumah", Util.toTextRequestBody(pdHomePhone));
        map.put("pd_nomor_handphone_satu", Util.toTextRequestBody(pdHandphoneOne));
        map.put("pd_nomor_handphone_dua", Util.toTextRequestBody(pdHandphoneTwo));
        map.put("pd_kode_pos", Util.toTextRequestBody(pdPostalCode));
        map.put("pd_email", Util.toTextRequestBody(pdEmail));
    }
}
