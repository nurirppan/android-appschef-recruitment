package com.appschef.recruitment.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends Fragment {

    @BindView(R.id.et_change_password)
    EditText etChangePassword;
    @BindView(R.id.et_new_password)
    EditText etNewPassword;
    @BindView(R.id.et_re_password)
    EditText etRePassword;
    @BindView(R.id.btn_save_password)
    Button btnSavePassword;
    Unbinder unbinder;
    private SharedPreferences sharedPreferences;
    private ApiService mApiService;
    private String token;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        getActivity().setTitle("Change Password");
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();
        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_save_password})
    public void onClickPassword(View view) {
        switch (view.getId()) {
            case R.id.btn_save_password:
                String curentPassword = etChangePassword.getText().toString().trim();
                String newPassword = etNewPassword.getText().toString().trim();
                String rePassword = etRePassword.getText().toString().trim();
                if (curentPassword.isEmpty())
                    Toast.makeText(getContext(), "Current Password is Empty", Toast.LENGTH_SHORT).show();
                else if (newPassword.isEmpty())
                    Toast.makeText(getContext(), "New Password is Empty", Toast.LENGTH_SHORT).show();
                else if (rePassword.isEmpty())
                    Toast.makeText(getContext(), "Retype Password is Empty", Toast.LENGTH_SHORT).show();
                else if (!newPassword.equals(rePassword))
                    Toast.makeText(getContext(), "Check Your Password and Retype Password", Toast.LENGTH_SHORT).show();
                else {
                    progressDialog.show();
                    mApiService.changePasswordPost(token, curentPassword, newPassword).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            if (response.isSuccessful()) {
                                progressDialog.dismiss();
                                if (response.body().getMeta().getMessage().equals("Change Password Successfuly")) {
                                    etChangePassword.getText().clear();
                                    etNewPassword.getText().clear();
                                    etRePassword.getText().clear();
                                }
                                Util.showToast(getContext(), response.message());
                            } else {
                                progressDialog.dismiss();
                                Util.showToast(getContext(), response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Util.showToast(getContext(), "onFailure");
                        }
                    });
                }
                break;
        }
    }
}
