package com.appschef.recruitment.fragment.profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;

import com.appschef.recruitment.R;
import com.appschef.recruitment.helper.Util;
import com.appschef.recruitment.model.bus.RefreshProfile;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.service.UtilsApi;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AchievementActivity extends AppCompatActivity {

    @BindView(R.id.et_ach_name_of_achievement)
    TextInputLayout etAchNameOfAchievement;
    @BindView(R.id.btn_rmv_ach_name_of_achievement)
    ImageView btnRmvAchNameOfAchievement;
    @BindView(R.id.et_ach_held_by)
    TextInputLayout etAchHeldBy;
    @BindView(R.id.btn_rmv_ach_held_by)
    ImageView btnRmvAchHeldBy;
    @BindView(R.id.et_ach_obtained_years)
    TextInputEditText etAchObtainedYears;
    @BindView(R.id.til_ach_obtained_years)
    TextInputLayout tilAchObtainedYears;
    @BindView(R.id.btn_rmv_ach_obtained_years)
    ImageView btnRmvAchObtainedYears;
    @BindView(R.id.btn_cancel_ach)
    Button btnCancelAch;
    @BindView(R.id.btn_save_ach)
    Button btnSaveAch;
    private ProgressDialog progressDialog;
    ApiService mApiService;
    SharedPreferences sharedPreferences;
    private String token;
    private String strAchNameOfAchievement;
    private String strAchHeldBy;
    private String strAchObtainedYears;
    private Intent intent;
    private String page;

    private Dialog dialog;
    private DateFormat dateFormat;
    private String initId;
    private String ach_nama_prestasi;
    private String ach_diselenggarakan_oleh;
    private String ach_diterima_tahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_achievement);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        mApiService = UtilsApi.getApiService();

        token = sharedPreferences.getString(getResources().getString(R.string.sharedpref_token), "");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        dateFormat = new SimpleDateFormat("yyyy");
        dialog = new Dialog(AchievementActivity.this);

        intent = getIntent();
        page = intent.getStringExtra("page");

        if (page.equals("update")) {
            initId = intent.getStringExtra("id");
            ach_nama_prestasi = intent.getStringExtra("ach_nama_prestasi");
            ach_diselenggarakan_oleh = intent.getStringExtra("ach_diselenggarakan_oleh");
            ach_diterima_tahun = intent.getStringExtra("ach_diterima_tahun");

            etAchNameOfAchievement.getEditText().setText(ach_nama_prestasi);
            etAchHeldBy.getEditText().setText(ach_diselenggarakan_oleh);
            etAchObtainedYears.setText(ach_diterima_tahun);
        }
    }

    private void valueAchievement() {
        strAchNameOfAchievement = etAchNameOfAchievement.getEditText().getText().toString().trim();
        strAchHeldBy = etAchHeldBy.getEditText().getText().toString().trim();
        strAchObtainedYears = etAchObtainedYears.getText().toString().trim();
    }

    private boolean checkAchievement() {
        valueAchievement();
        if (strAchNameOfAchievement.isEmpty()) etAchNameOfAchievement.setError("Wajib Di Isi");
        else etAchNameOfAchievement.setError(null);
        if (strAchHeldBy.isEmpty()) etAchHeldBy.setError("Wajib Di Isi");
        else etAchHeldBy.setError(null);
        if (strAchObtainedYears.isEmpty()) tilAchObtainedYears.setError("Wajib Di Isi");
        else tilAchObtainedYears.setError(null);
        return !strAchNameOfAchievement.isEmpty() && !strAchHeldBy.isEmpty() && !strAchObtainedYears.isEmpty();
    }

    @OnClick({R.id.btn_rmv_ach_name_of_achievement, R.id.btn_rmv_ach_held_by, R.id.btn_rmv_ach_obtained_years})
    public void onClickRemoveACH(View view) {
        switch (view.getId()) {
            case R.id.btn_rmv_ach_name_of_achievement:
                etAchNameOfAchievement.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_ach_held_by:
                etAchHeldBy.getEditText().getText().clear();
                break;
            case R.id.btn_rmv_ach_obtained_years:
                etAchObtainedYears.getText().clear();
                break;
        }
    }

    @OnClick({R.id.btn_cancel_ach, R.id.btn_save_ach})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel_ach:
                finish();
                break;
            case R.id.btn_save_ach:
                if (checkAchievement()) {
                    progressDialog.show();
                    if (page.equals("new")) {
                        mApiService.achievementPost(token, strAchNameOfAchievement, strAchHeldBy, strAchObtainedYears).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etAchNameOfAchievement.getEditText().getText().clear();
                                    etAchHeldBy.getEditText().getText().clear();
                                    etAchObtainedYears.getText().clear();
                                    Util.showToast(AchievementActivity.this, response.message());
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(AchievementActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(AchievementActivity.this, "onFailure");
                            }
                        });
                    } else {
                        mApiService.achievementUpdate(token, Integer.valueOf(initId), strAchNameOfAchievement, strAchHeldBy, strAchObtainedYears).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    etAchNameOfAchievement.getEditText().getText().clear();
                                    etAchHeldBy.getEditText().getText().clear();
                                    etAchObtainedYears.getText().clear();
                                    Util.showToast(AchievementActivity.this, response.message());
                                    EventBus.getDefault().post(new RefreshProfile());
                                    progressDialog.dismiss();
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    Util.showToast(AchievementActivity.this, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Util.showToast(AchievementActivity.this, "onFailure");
                            }
                        });
                    }
                }
                break;
        }
    }

    @OnClick(R.id.et_ach_obtained_years)
    public void onViewClicked() {
        String dateYearStart = dateFormat.format(Calendar.getInstance().getTime());
        dialog.setTitle("Tahun");
        dialog.setContentView(R.layout.utils_year_picker);
        Button btnCancelNp = (Button) dialog.findViewById(R.id.btn_cancel_np);
        Button btnSaveNp = (Button) dialog.findViewById(R.id.btn_save_np);
        final NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.np_year);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(9999);
        numberPicker.setValue(Integer.parseInt(dateYearStart));
        numberPicker.setWrapSelectorWheel(false);
        btnCancelNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSaveNp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAchObtainedYears.setText(String.valueOf(numberPicker.getValue()));
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
