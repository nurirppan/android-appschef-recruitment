package com.appschef.recruitment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.fragment.profile.AchievementActivity;
import com.appschef.recruitment.fragment.profile.DetailProfileFragment;
import com.appschef.recruitment.model.response.childresponse.profile.AchievementGet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.ViewHolder> {

    private Context context;
    private List<AchievementGet> achievementGetList;
    private AchievementGet achievementGet;
    private ApiService mApiService;
    private String token;
    private DetailProfileFragment detailProfileFragment;

    public AchievementAdapter(Context context, List<AchievementGet> achievementGetList, ApiService mApiService, String token, DetailProfileFragment detailProfileFragment) {
        this.context = context;
        this.achievementGetList = achievementGetList;
        this.mApiService = mApiService;
        this.token = token;
        this.detailProfileFragment = detailProfileFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.crud_achievement, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        achievementGet = achievementGetList.get(position);
        holder.tvAchNamaPrestasi.setText(achievementGet.getAchNamaPrestasi());
        holder.tvAchDiadakanOleh.setText(achievementGet.getAchDiselenggarakanOleh());
        holder.tvAchDiperolehTahun.setText(achievementGet.getAchDiterimaTahun());

        holder.btnAchUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AchievementActivity.class);
                i.putExtra("page", "update");
                i.putExtra("id", String.valueOf(achievementGet.getId()));
                i.putExtra("ach_nama_prestasi", achievementGet.getAchNamaPrestasi());
                i.putExtra("ach_diselenggarakan_oleh", achievementGet.getAchDiselenggarakanOleh());
                i.putExtra("ach_diterima_tahun", achievementGet.getAchDiterimaTahun());
                context.startActivity(i);
            }
        });
        holder.btnAchDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailProfileFragment.delDataProfile("ACH", achievementGet.getId(), token);
            }
        });
    }

    @Override
    public int getItemCount() {
        return achievementGetList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_ach_nama_prestasi)
        TextView tvAchNamaPrestasi;
        @BindView(R.id.tv_ach_diadakan_oleh)
        TextView tvAchDiadakanOleh;
        @BindView(R.id.tv_ach_diperoleh_tahun)
        TextView tvAchDiperolehTahun;
        @BindView(R.id.btn_ach_update)
        ImageView btnAchUpdate;
        @BindView(R.id.btn_ach_delete)
        ImageView btnAchDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
