package com.appschef.recruitment.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.fragment.profile.DetailProfileFragment;
import com.appschef.recruitment.fragment.profile.OrganizationExperienceActivity;
import com.appschef.recruitment.model.response.childresponse.profile.OrganizationalExperienceGet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrganizationExperienceAdapter extends RecyclerView.Adapter<OrganizationExperienceAdapter.ViewHolder> {

    private Context context;
    private List<OrganizationalExperienceGet> organizationalExperienceGetList;
    private OrganizationalExperienceGet organizationalExperienceGet;
    private ApiService mApiService;
    private String token;
    private DetailProfileFragment detailProfileFragment;

    public OrganizationExperienceAdapter(Context context, List<OrganizationalExperienceGet> organizationalExperienceGetList, ApiService mApiService, String token, DetailProfileFragment detailProfileFragment) {
        this.context = context;
        this.organizationalExperienceGetList = organizationalExperienceGetList;
        this.mApiService = mApiService;
        this.token = token;
        this.detailProfileFragment = detailProfileFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.crud_organizational_experience, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        organizationalExperienceGet = organizationalExperienceGetList.get(position);
        holder.tvOeNamaOrganisasi.setText(organizationalExperienceGet.getOeNamaOrganisasi());
        holder.tvOePeriode.setText(organizationalExperienceGet.getOeBulanMulai() + " " + organizationalExperienceGet.getOeTahunMulai() + " - " +
                organizationalExperienceGet.getOeBulanSelesai() + " " + organizationalExperienceGet.getOeTahunSelesai());
        holder.tvOePosisiOrganisasi.setText(organizationalExperienceGet.getOePosisiPekerjaan());

        holder.btnOeUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, OrganizationExperienceActivity.class);
                i.putExtra("page", "update");
                i.putExtra("id", String.valueOf(organizationalExperienceGet.getId()));
                i.putExtra("oe_bulan_mulai", organizationalExperienceGet.getOeBulanMulai());
                i.putExtra("oe_tahun_mulai", organizationalExperienceGet.getOeTahunMulai());
                i.putExtra("oe_bulan_selesai", organizationalExperienceGet.getOeBulanSelesai());
                i.putExtra("oe_tahun_selesai", organizationalExperienceGet.getOeTahunSelesai());
                i.putExtra("oe_nama_organisasi", organizationalExperienceGet.getOeNamaOrganisasi());
                i.putExtra("oe_posisi_pekerjaan", organizationalExperienceGet.getOePosisiPekerjaan());
                i.putExtra("oe_uraian_Tugas", organizationalExperienceGet.getOeUraianTugas());
                context.startActivity(i);
            }
        });
        holder.btnOeDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailProfileFragment.delDataProfile("OE", organizationalExperienceGet.getId(), token);
            }
        });
    }

    @Override
    public int getItemCount() {
        return organizationalExperienceGetList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_oe_periode)
        TextView tvOePeriode;
        @BindView(R.id.tv_oe_nama_organisasi)
        TextView tvOeNamaOrganisasi;
        @BindView(R.id.tv_oe_posisi_organisasi)
        TextView tvOePosisiOrganisasi;
        @BindView(R.id.btn_oe_update)
        ImageView btnOeUpdate;
        @BindView(R.id.btn_oe_delete)
        ImageView btnOeDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
