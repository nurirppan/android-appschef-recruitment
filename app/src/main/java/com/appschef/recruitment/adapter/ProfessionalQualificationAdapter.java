package com.appschef.recruitment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.fragment.profile.DetailProfileFragment;
import com.appschef.recruitment.fragment.profile.ProfessionalQualificationActivity;
import com.appschef.recruitment.model.response.childresponse.profile.ProfessionalQualificationGet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfessionalQualificationAdapter extends RecyclerView.Adapter<ProfessionalQualificationAdapter.ViewHolder> {

    private Context context;
    private List<ProfessionalQualificationGet> professionalQualificationGetList;
    private ProfessionalQualificationGet professionalQualificationGet;
    private ApiService mApiService;
    private String token;
    private DetailProfileFragment detailProfileFragment;

    public ProfessionalQualificationAdapter(Context context, List<ProfessionalQualificationGet> professionalQualificationGetList, ApiService mApiService, String token, DetailProfileFragment detailProfileFragment) {
        this.context = context;
        this.professionalQualificationGetList = professionalQualificationGetList;
        this.mApiService = mApiService;
        this.token = token;
        this.detailProfileFragment = detailProfileFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.crud_professional_qualification, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        professionalQualificationGet = professionalQualificationGetList.get(position);
        holder.tvPqNamaKualifikasi.setText(professionalQualificationGet.getPqNamaKualifikasiProfesional());
        holder.tvPqDiadakanOleh.setText(professionalQualificationGet.getPqDiselenggarakanOleh());
        holder.tvPqDiperolehTahun.setText(professionalQualificationGet.getPqDiterimaTahun());

        holder.btnPqUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ProfessionalQualificationActivity.class);
                i.putExtra("page", "update");
                i.putExtra("id", String.valueOf(professionalQualificationGet.getId()));
                i.putExtra("pq_nama_kualifikasi_profesional", professionalQualificationGet.getPqNamaKualifikasiProfesional());
                i.putExtra("pq_diselenggarakan_oleh", professionalQualificationGet.getPqDiselenggarakanOleh());
                i.putExtra("pq_diterima_tahun", professionalQualificationGet.getPqDiterimaTahun());
                context.startActivity(i);
            }
        });
        holder.btnPqDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailProfileFragment.delDataProfile("PQ", professionalQualificationGet.getId(), token);
            }
        });
    }

    @Override
    public int getItemCount() {
        return professionalQualificationGetList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_pq_nama_kualifikasi)
        TextView tvPqNamaKualifikasi;
        @BindView(R.id.tv_pq_diadakan_oleh)
        TextView tvPqDiadakanOleh;
        @BindView(R.id.tv_pq_diperoleh_tahun)
        TextView tvPqDiperolehTahun;
        @BindView(R.id.btn_pq_update)
        ImageView btnPqUpdate;
        @BindView(R.id.btn_pq_delete)
        ImageView btnPqDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
