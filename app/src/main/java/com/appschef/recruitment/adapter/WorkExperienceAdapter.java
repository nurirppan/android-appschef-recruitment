package com.appschef.recruitment.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.fragment.profile.DetailProfileFragment;
import com.appschef.recruitment.fragment.profile.WorkExperienceActivity;
import com.appschef.recruitment.model.response.childresponse.profile.WorkExperienceGet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WorkExperienceAdapter extends RecyclerView.Adapter<WorkExperienceAdapter.ViewHolder> {

    private Context context;
    private List<WorkExperienceGet> workExperienceGetList;
    private WorkExperienceGet workExperienceGet;
    private ApiService mApiService;
    private String token;
    private DetailProfileFragment detailProfileFragment;

    public WorkExperienceAdapter(Context context, List<WorkExperienceGet> workExperienceGetList, ApiService mApiService, String token, DetailProfileFragment detailProfileFragment) {
        this.context = context;
        this.workExperienceGetList = workExperienceGetList;
        this.mApiService = mApiService;
        this.token = token;
        this.detailProfileFragment = detailProfileFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.crud_work_experience, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        workExperienceGet = workExperienceGetList.get(position);
        holder.tvWeNamaPerusahaan.setText(workExperienceGet.getWeNamaPerusahaan());
        holder.tvWePeriode.setText(workExperienceGet.getWeBulanMulai() + " " + workExperienceGet.getWeTahunMulai() + " - " +
                workExperienceGet.getWeBulanSelesai() + " " + workExperienceGet.getWeTahunSelesai());
        holder.tvWePosisiKerja.setText(workExperienceGet.getWePosisiPekerjaan());

        holder.btnWeUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, WorkExperienceActivity.class);
                i.putExtra("page", "update");
                i.putExtra("id", String.valueOf(workExperienceGet.getId()));
                i.putExtra("we_bulan_mulai", workExperienceGet.getWeBulanMulai());
                i.putExtra("we_tahun_mulai", workExperienceGet.getWeTahunMulai());
                i.putExtra("we_bulan_selesai", workExperienceGet.getWeBulanSelesai());
                i.putExtra("we_tahun_selesai", workExperienceGet.getWeTahunSelesai());
                i.putExtra("we_nama_perusahaan", workExperienceGet.getWeNamaPerusahaan());
                i.putExtra("we_posisi_pekerjaan", workExperienceGet.getWePosisiPekerjaan());
                i.putExtra("we_uraian_Tugas", workExperienceGet.getWeUraianTugas());
                context.startActivity(i);
            }
        });
        holder.btnWeDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailProfileFragment.delDataProfile("WE", workExperienceGet.getId(), token);
            }
        });
    }

    @Override
    public int getItemCount() {
        return workExperienceGetList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_we_periode)
        TextView tvWePeriode;
        @BindView(R.id.tv_we_nama_perusahaan)
        TextView tvWeNamaPerusahaan;
        @BindView(R.id.tv_we_posisi_kerja)
        TextView tvWePosisiKerja;
        @BindView(R.id.btn_we_update)
        ImageView btnWeUpdate;
        @BindView(R.id.btn_we_delete)
        ImageView btnWeDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
