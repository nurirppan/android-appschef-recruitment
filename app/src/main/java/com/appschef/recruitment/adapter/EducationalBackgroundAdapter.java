package com.appschef.recruitment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.fragment.profile.DetailProfileFragment;
import com.appschef.recruitment.fragment.profile.EducationalBackgroundActivity;
import com.appschef.recruitment.model.response.childresponse.profile.EducationalBackgroundGet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EducationalBackgroundAdapter extends RecyclerView.Adapter<EducationalBackgroundAdapter.ViewHolder> {

    private Context context;
    private List<EducationalBackgroundGet> backgroundGetList;
    private EducationalBackgroundGet backgroundGet;
    private ApiService mApiService;
    private String token;
    private DetailProfileFragment detailProfileFragment;

    public EducationalBackgroundAdapter(Context context, List<EducationalBackgroundGet> backgroundGetList, ApiService mApiService, String token, DetailProfileFragment detailProfileFragment) {
        this.context = context;
        this.backgroundGetList = backgroundGetList;
        this.mApiService = mApiService;
        this.token = token;
        this.detailProfileFragment = detailProfileFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.crud_educational_background, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        backgroundGet = backgroundGetList.get(position);
        holder.tvebid.setText(String.valueOf(backgroundGet.getId()));
        holder.tvEbTingkatPendidikan.setText(backgroundGet.getEbTingkatPendidikan());
        holder.tvEbProgramStudi.setText(backgroundGet.getEbProgramStudi());
        holder.tvEbTahunLulus.setText(backgroundGet.getEbTahunKelulusan());
        holder.tvEbIpk.setText(backgroundGet.getEbIpk());

        holder.btnEbUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, EducationalBackgroundActivity.class);
                i.putExtra("page", "update");
                i.putExtra("id", String.valueOf(backgroundGet.getId()));
                i.putExtra("tingkatPendidikan", backgroundGet.getEbTingkatPendidikan());
                i.putExtra("programStudi", backgroundGet.getEbProgramStudi());
                i.putExtra("tahunLulus", backgroundGet.getEbTahunKelulusan());
                i.putExtra("ipk", backgroundGet.getEbIpk());
                context.startActivity(i);
            }
        });
        holder.btnEbDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailProfileFragment.delDataProfile("EB", backgroundGet.getId(), token);
            }
        });
    }

    @Override
    public int getItemCount() {
        return backgroundGetList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_eb_id)
        TextView tvebid;
        @BindView(R.id.tv_eb_tahun_lulus)
        TextView tvEbTahunLulus;
        @BindView(R.id.tv_eb_tingkat_pendidikan)
        TextView tvEbTingkatPendidikan;
        @BindView(R.id.tv_eb_program_studi)
        TextView tvEbProgramStudi;
        @BindView(R.id.tv_eb_ipk)
        TextView tvEbIpk;
        @BindView(R.id.btn_eb_update)
        ImageView btnEbUpdate;
        @BindView(R.id.btn_eb_delete)
        ImageView btnEbDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
