package com.appschef.recruitment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.activity.InterviewScheduleActivity;
import com.appschef.recruitment.activity.TestOnlineScheduleActivity;
import com.appschef.recruitment.fragment.NotificationsFragment;
import com.appschef.recruitment.model.response.childresponse.NotificationResponse;
import com.appschef.recruitment.service.ApiService;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private Context context;
    private List<NotificationResponse> notificationResponseList;
    private NotificationResponse notificationResponse;
    private ApiService mApiService;
    private String token;
    private NotificationsFragment notificationsFragment;
    private Intent intent;

    public NotificationAdapter(Context context, List<NotificationResponse> notificationResponseList, ApiService mApiService, String token, NotificationsFragment notificationsFragment) {
        this.context = context;
        this.notificationResponseList = notificationResponseList;
        this.mApiService = mApiService;
        this.token = token;
        this.notificationsFragment = notificationsFragment;
    }

    @NonNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_notification, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.ViewHolder holder, int position) {
        notificationResponse = notificationResponseList.get(position);
        holder.tvTitle.setText(notificationResponse.getTitle());

        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notificationResponse.getType().equals("online-test")) {
                    intent = new Intent(context, TestOnlineScheduleActivity.class);
                    intent.putExtra("recruitment_id", String.valueOf(notificationResponse.getRecruitmentId()));
                    intent.putExtra("type", notificationResponse.getType());
                    intent.putExtra("title", notificationResponse.getTitle());
                    context.startActivity(intent);
                } else {
                    intent = new Intent(context, InterviewScheduleActivity.class);
                    intent.putExtra("recruitment_id", String.valueOf(notificationResponse.getRecruitmentId()));
                    intent.putExtra("type", notificationResponse.getType());
                    intent.putExtra("title", notificationResponse.getTitle());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationResponseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
