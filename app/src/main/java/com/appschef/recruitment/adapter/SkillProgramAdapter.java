package com.appschef.recruitment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appschef.recruitment.R;
import com.appschef.recruitment.service.ApiService;
import com.appschef.recruitment.fragment.profile.DetailProfileFragment;
import com.appschef.recruitment.fragment.profile.SkillProgramActivity;
import com.appschef.recruitment.model.response.childresponse.profile.SkillProgramGet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SkillProgramAdapter extends RecyclerView.Adapter<SkillProgramAdapter.ViewHolder> {

    private Context context;
    private List<SkillProgramGet> skillProgramGetList;
    private SkillProgramGet skillProgramGet;
    private ApiService mApiService;
    private String token;
    private DetailProfileFragment detailProfileFragment;

    public SkillProgramAdapter(Context context, List<SkillProgramGet> skillProgramGetList, ApiService mApiService, String token, DetailProfileFragment detailProfileFragment) {
        this.context = context;
        this.skillProgramGetList = skillProgramGetList;
        this.mApiService = mApiService;
        this.token = token;
        this.detailProfileFragment = detailProfileFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.crud_skills_or_programs_ever_made, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        skillProgramGet = skillProgramGetList.get(position);
        holder.tvSpNamaKualifikasi.setText(skillProgramGet.getSpNamaKeahlian());
        holder.tvSpDeskripsi.setText(skillProgramGet.getSpDeskripsi());

        holder.btnSpUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SkillProgramActivity.class);
                i.putExtra("page", "update");
                i.putExtra("id", String.valueOf(skillProgramGet.getId()));
                i.putExtra("sp_nama_keahlian", skillProgramGet.getSpNamaKeahlian());
                i.putExtra("sp_deskripsi", skillProgramGet.getSpDeskripsi());
                context.startActivity(i);
            }
        });
        holder.btnSpDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailProfileFragment.delDataProfile("SP", skillProgramGet.getId(), token);
            }
        });
    }

    @Override
    public int getItemCount() {
        return skillProgramGetList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_sp_nama_kualifikasi)
        TextView tvSpNamaKualifikasi;
        @BindView(R.id.tv_sp_deskripsi)
        TextView tvSpDeskripsi;
        @BindView(R.id.btn_sp_update)
        ImageView btnSpUpdate;
        @BindView(R.id.btn_sp_delete)
        ImageView btnSpDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
