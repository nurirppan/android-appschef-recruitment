package com.appschef.recruitment.service;

import android.content.Intent;
import android.util.Log;

import com.appschef.recruitment.activity.HomeActivity;
import com.appschef.recruitment.helper.Config;
import com.appschef.recruitment.model.NotificationVO;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private NotificationVO notificationVO;
    private String strTitle;
    private String strMessage;
    private String strIconUrl;
    private String strAction;
    private String strActionDestination;
    private String strName;
    private String strTypeJob;
    private String strStartDate;
    private String strEndDate;
    private String strTimeDate;
    private String strRecruitmentId;
    private String strType;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(Config.TAG, "From: " + remoteMessage.getFrom());
        notificationVO = new NotificationVO();

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(Config.TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            handleData(data);

        } else if (remoteMessage.getNotification() != null) {
            Log.d(Config.TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification());
        }// Check if message contains a notification payload.

        Intent resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.displayNotification(notificationVO, resultIntent);
        notificationUtils.playNotificationSound();

    }

    private void handleNotification(RemoteMessage.Notification RemoteMsgNotification) {
        strMessage = RemoteMsgNotification.getBody();
        strTitle = RemoteMsgNotification.getTitle();
        notificationVO.setTitle(strTitle);
        notificationVO.setMessage(strMessage);
    }

    private void handleData(Map<String, String> data) {
        strTitle = data.get(Config.TITLE);
        strMessage = data.get(Config.MESSAGE);
        strIconUrl = data.get(Config.IMAGE);
        strAction = data.get(Config.ACTION);
        strActionDestination = data.get(Config.ACTION_DESTINATION);
        strName = data.get(Config.NAME);
        strTypeJob = data.get(Config.TYPE_JOB);
        strStartDate = data.get(Config.START_DATE);
        strEndDate = data.get(Config.END_DATE);
        strTimeDate = data.get(Config.TIME_DATE);
        strRecruitmentId = data.get(Config.RECRUITMEN_ID);
        strType = data.get(Config.TYPE_JOB);

        notificationVO.setTitle(strTitle);
        notificationVO.setMessage(strMessage);
        notificationVO.setIconUrl(strIconUrl);
        notificationVO.setAction(strAction);
        notificationVO.setActionDestination(strActionDestination);
        notificationVO.setName(strName);
        notificationVO.setTypeJob(strTypeJob);
        notificationVO.setStartDate(strStartDate);
        notificationVO.setEndDate(strEndDate);
        notificationVO.setTimeDate(strTimeDate);
        notificationVO.setRecruitment_id(strRecruitmentId);
        notificationVO.setType(strType);
    }
}
