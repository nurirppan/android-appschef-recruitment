package com.appschef.recruitment.service;

public class UtilsApi {

    public static final String BASE_URL_API = "http://recruitment-appschef.com/api/";

    public static ApiService getApiService(){
        return RetrofitClient.getClient(BASE_URL_API).create(ApiService.class);
    }

}
