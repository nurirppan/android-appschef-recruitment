package com.appschef.recruitment.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.appschef.recruitment.R;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

public class DeleteTokenService extends IntentService {
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    public static final String TAG = DeleteTokenService.class.getSimpleName();

    public DeleteTokenService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPref = getSharedPreferences(getString(R.string.sharedpref_MyPrefs), Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        // Resets Instance ID and revokes all tokens.
        try {
            FirebaseInstanceId.getInstance().deleteInstanceId();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Now manually call onTokenRefresh()
        Log.d(TAG, "Getting new token");
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (!refreshedToken.isEmpty() || refreshedToken != null){
            editor.putString(getString(R.string.sharedpref_token_firebase), FirebaseInstanceId.getInstance().getToken());
            editor.apply();
            Log.i("token_firebase_new", FirebaseInstanceId.getInstance().getToken());
        }
    }
}
