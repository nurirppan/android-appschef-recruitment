package com.appschef.recruitment.service;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.InvalidParameterException;

public class ErrorRetrofitUtil {

    public String responseFailedError(Throwable e) {
        if (e != null) {
            if (e instanceof UnknownHostException) {
                return "Tidak Ada Koneksi Internet";
            }else if (e instanceof SocketTimeoutException){
                return "Server sedang sibuk, Mohon coba beberapa saat lagi";
            }else if (e instanceof NumberFormatException){
                return "Terjadi kesalahan pada server";
            }else if (e instanceof InvalidParameterException){
                return "Terjadi kesalahan pada server";
            }
        }
        return "Terjadi kesalahan koneksi";
    }
}
