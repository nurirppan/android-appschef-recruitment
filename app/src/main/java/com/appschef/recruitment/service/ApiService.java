package com.appschef.recruitment.service;

import com.appschef.recruitment.model.BaseResponse;
import com.appschef.recruitment.model.response.LoginResponse;
import com.appschef.recruitment.model.response.ScheduleResponse;
import com.appschef.recruitment.model.response.parentresponse.NotificationParent;
import com.appschef.recruitment.model.response.parentresponse.onlinetes.OnlineTestParent;
import com.appschef.recruitment.model.response.parentresponse.profile.AchievementParent;
import com.appschef.recruitment.model.response.parentresponse.profile.EducationalBackgroundParent;
import com.appschef.recruitment.model.response.parentresponse.profile.OrganizationalExperienceParent;
import com.appschef.recruitment.model.response.parentresponse.profile.PersonalDataParent;
import com.appschef.recruitment.model.response.parentresponse.profile.ProfessionalQualificationParent;
import com.appschef.recruitment.model.response.parentresponse.profile.SkillProgramParent;
import com.appschef.recruitment.model.response.parentresponse.profile.WorkExperienceParent;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    //    public static final String BASE_URL_API = "http://recruitment-appschef.com/api/";

    @FormUrlEncoded
    @POST("v1/register")
    Call<ResponseBody> registerRequest(@Field("fullname") String fullname, @Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("v1/login")
    Call<BaseResponse<LoginResponse>> loginRequest(@Field("email") String email, @Field("password") String password);

    @Multipart
    @POST("v1/user/personal_data/update")
    Call<ResponseBody> personalDataPost(@Query("token") String token,
                                        @PartMap Map<String, RequestBody> partMap);


    @Headers("Content-Type: application/json")
    @GET("v1/user/personal_data/show")
    Call<BaseResponse<PersonalDataParent>> personalDataGet(@Query("token") String token);

    @Headers("Content-Type: application/json")
    @GET("v1/user/educational_background/show")
    Call<BaseResponse<EducationalBackgroundParent>> educationalBackgroundGet(@Query("token") String token);

    @Headers("Content-Type: application/json")
    @GET("v1/user/work_experience/show")
    Call<BaseResponse<WorkExperienceParent>> workExperienceGet(@Query("token") String token);

    @Headers("Content-Type: application/json")
    @GET("v1/user/organization_experience/show")
    Call<BaseResponse<OrganizationalExperienceParent>> organizationalExperienceGet(@Query("token") String token);

    @Headers("Content-Type: application/json")
    @GET("v1/user/achievement/show")
    Call<BaseResponse<AchievementParent>> achievementGet(@Query("token") String token);

    @Headers("Content-Type: application/json")
    @GET("v1/user/professional_qualification/show")
    Call<BaseResponse<ProfessionalQualificationParent>> professionalQualificationGet(@Query("token") String token);

    @Headers("Content-Type: application/json")
    @GET("v1/user/expertise_program/show")
    Call<BaseResponse<SkillProgramParent>> skillProgramGet(@Query("token") String token);

    @FormUrlEncoded
    @POST("v1/user/educational_background/store")
    Call<ResponseBody> educationalBackgroundPost(@Query("token") String token,
                                                 @Field("eb_tingkat_pendidikan") String tingkatPendidikan,
                                                 @Field("eb_tahun_kelulusan") String tahunKelulusan,
                                                 @Field("eb_program_studi") String programStudi,
                                                 @Field("eb_ipk") String ipk);

    @FormUrlEncoded
    @POST("v1/user/educational_background/update")
    Call<ResponseBody> educationalBackgroundUpdate(@Query("token") String token,
                                                   @Field("id") Integer id, @Field("eb_tingkat_pendidikan") String tingkatPendidikan,
                                                   @Field("eb_tahun_kelulusan") String tahunKelulusan,
                                                   @Field("eb_program_studi") String programStudi,
                                                   @Field("eb_ipk") String ipk);

    @Headers("Content-Type: application/json")
    @GET("v1/user/educational_background/delete")
    Call<BaseResponse> educationalBackgroundDelete(@Query("id") Integer id, @Query("token") String token);

    //    =====================================================================================

    @FormUrlEncoded
    @POST("v1/user/work_experience/store")
    Call<ResponseBody> workExperiencePost(@Query("token") String token,
                                          @Field("we_bulan_mulai") String weBulanMulai,
                                          @Field("we_tahun_mulai") String weTahunMulai,
                                          @Field("we_bulan_selesai") String weBulanSelesai,
                                          @Field("we_tahun_selesai") String weTahunSelesai,
                                          @Field("we_nama_perusahaan") String weNamaPerusahaan,
                                          @Field("we_posisi_pekerjaan") String wePosisiPekerjaan,
                                          @Field("we_uraian_Tugas") String weUraianTugas);

    @FormUrlEncoded
    @POST("v1/user/work_experience/update")
    Call<ResponseBody> workExperienceUpdate(@Query("token") String token, @Field("id") Integer id,
                                            @Field("we_bulan_mulai") String weBulanMulai,
                                            @Field("we_tahun_mulai") String weTahunMulai,
                                            @Field("we_bulan_selesai") String weBulanSelesai,
                                            @Field("we_tahun_selesai") String weTahunSelesai,
                                            @Field("we_nama_perusahaan") String weNamaPerusahaan,
                                            @Field("we_posisi_pekerjaan") String wePosisiPekerjaan,
                                            @Field("we_uraian_Tugas") String weUraianTugas);

    @Headers("Content-Type: application/json")
    @GET("v1/user/work_experience/delete")
    Call<BaseResponse> workExperienceDelete(@Query("id") Integer id, @Query("token") String token);

    //    =====================================================================================

    @FormUrlEncoded
    @POST("v1/user/organization_experience/store")
    Call<ResponseBody> organizationExperiencePost(@Query("token") String token,
                                                  @Field("oe_bulan_mulai") String oeBulanMulai,
                                                  @Field("oe_tahun_mulai") String oeTahunMulai,
                                                  @Field("oe_bulan_selesai") String oeBulanSelesai,
                                                  @Field("oe_tahun_selesai") String oeTahunSelesai,
                                                  @Field("oe_nama_organisasi") String oeNamaOrganisasi,
                                                  @Field("oe_posisi_pekerjaan") String oePosisiPekerjaan,
                                                  @Field("oe_uraian_Tugas") String oeUraianTugas);

    @FormUrlEncoded
    @POST("v1/user/organization_experience/update")
    Call<ResponseBody> organizationExperienceUpdate(@Query("token") String token,
                                                    @Field("id") Integer id,
                                                    @Field("oe_bulan_mulai") String oeBulanMulai,
                                                    @Field("oe_tahun_mulai") String oeTahunMulai,
                                                    @Field("oe_bulan_selesai") String oeBulanSelesai,
                                                    @Field("oe_tahun_selesai") String oeTahunSelesai,
                                                    @Field("oe_nama_organisasi") String oeNamaOrganisasi,
                                                    @Field("oe_posisi_pekerjaan") String oePosisiPekerjaan,
                                                    @Field("oe_uraian_Tugas") String oeUraianTugas);

    @Headers("Content-Type: application/json")
    @GET("v1/user/organization_experience/delete")
    Call<BaseResponse> organizationExperienceDelete(@Query("id") Integer id, @Query("token") String token);

    //    =====================================================================================

    @FormUrlEncoded
    @POST("v1/user/achievement/store")
    Call<ResponseBody> achievementPost(@Query("token") String token, @Field("ach_nama_prestasi") String achNamaPrestasi, @Field("ach_diselenggarakan_oleh") String achDiselenggarakanOleh, @Field("ach_diterima_tahun") String achDiterimaTahun);

    @FormUrlEncoded
    @POST("v1/user/achievement/update")
    Call<ResponseBody> achievementUpdate(@Query("token") String token, @Field("id") Integer id, @Field("ach_nama_prestasi") String achNamaPrestasi, @Field("ach_diselenggarakan_oleh") String achDiselenggarakanOleh, @Field("ach_diterima_tahun") String achDiterimaTahun);

    @Headers("Content-Type: application/json")
    @GET("v1/user/achievement/delete")
    Call<BaseResponse> achievementDelete(@Query("id") Integer id, @Query("token") String token);

    //    =====================================================================================

    @FormUrlEncoded
    @POST("v1/user/professional_qualification/store")
    Call<ResponseBody> professionalQualificationPost(@Query("token") String token, @Field("pq_nama_kualifikasi_profesional") String pqNamaKualifikasiProfesional, @Field("pq_diselenggarakan_oleh") String pqDiselenggarakanOleh, @Field("pq_diterima_tahun") String pqDiterimaTahun);

    @FormUrlEncoded
    @POST("v1/user/professional_qualification/update")
    Call<ResponseBody> professionalQualificationUpdate(@Query("token") String token, @Field("id") Integer id, @Field("pq_nama_kualifikasi_profesional") String pqNamaKualifikasiProfesional, @Field("pq_diselenggarakan_oleh") String pqDiselenggarakanOleh, @Field("pq_diterima_tahun") String pqDiterimaTahun);

    @Headers("Content-Type: application/json")
    @GET("v1/user/professional_qualification/delete")
    Call<BaseResponse> professionalQualificationDelete(@Query("id") Integer id, @Query("token") String token);

    //    =====================================================================================

    @FormUrlEncoded
    @POST("v1/user/expertise_program/store")
    Call<ResponseBody> skillsProgramPost(@Query("token") String token, @Field("sp_nama_keahlian") String sp_nama_skill_program, @Field("sp_deskripsi") String sp_uraian);

    @FormUrlEncoded
    @POST("v1/user/expertise_program/update")
    Call<ResponseBody> skillsProgramUpdate(@Query("token") String token, @Field("id") Integer id, @Field("sp_nama_keahlian") String sp_nama_skill_program, @Field("sp_deskripsi") String sp_uraian);

    @Headers("Content-Type: application/json")
    @GET("v1/user/expertise_program/delete")
    Call<BaseResponse> skillsProgramDelete(@Query("id") Integer id, @Query("token") String token);

    //    =====================================================================================
    @Headers("Content-Type: application/json")
    @GET("v1/user/online_test/show")
    Call<BaseResponse<OnlineTestParent>> multipleChoiceGet(@Query("token") String token,
                                                           @Query("slug") String slug);

    @Headers("Content-Type: application/json")
    @GET("v1/user/online_test/essay")
    Call<BaseResponse<OnlineTestParent>> essayGet(@Query("token") String token,
                                                  @Query("slug") String slug);

    @Multipart
    @POST("v1/user/online_test/store")
    Call<ResponseBody> onlineTestPost(@Query("token") String token,
                                      @PartMap Map<String, RequestBody> partMap);

    //    =====================================================================================

    @Multipart
    @POST("v1/user/profile_picture/upload")
    Call<ResponseBody> imageProfilePost(@Query("token") String token,
                                        @Part MultipartBody.Part image);

    //    =====================================================================================

    @FormUrlEncoded
    @POST("v1/user/change_password")
    Call<BaseResponse> changePasswordPost(@Query("token") String token,
                                          @Field("last_password") String lastPassword,
                                          @Field("new_password") String newPassword);

    @FormUrlEncoded
    @POST("v1/set-fcm")
    Call<BaseResponse> postFCM(@Query("token") String token,
                               @Field("fcm_token") String fcm_token);

    @Headers("Content-Type: application/json")
    @GET("v1/user/notification")
    Call<BaseResponse<NotificationParent>> getListNotification(@Query("token") String token);

    @Headers("Content-Type: application/json")
    @GET("v1/user/schedule/{id}")
    Call<BaseResponse<ScheduleResponse>> getSchedule(@Path("id") String id,
                                                     @Query("token") String token,
                                                     @Query("type") String type);

    @FormUrlEncoded
    @POST("v1/user/upload_job")
    Call<BaseResponse> postUploadJob(@Query("token") String token,
                                     @Field("position") String position);
}
